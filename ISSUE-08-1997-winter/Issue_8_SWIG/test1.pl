#!/usr/local/bin/perl -w

use strict;
use top;

my $clear = "\n";
if (@ARGV && $ARGV[0] =~ /-c/) {
	$clear = `clear`;
}

my($statics) = new statics();
my($si)	     = new system_info();
my($ps)	     = new process_select();
$ps->{idle}     =  1;
$ps->{"system"} =  0;
$ps->{uid}      = -1;

my $r = top::machine_init($statics);
print("machine_init($statics) => $r.\n");

hash_dump("statics", $statics);
my(@procstates) = names($statics->{procstate_names});
my(@cpustates)  = names($statics->{cpustate_names});
my(@swap)       = names($statics->{swap_names});
my(@memory)     = names($statics->{memory_names});

top::get_system_info($si);
for (0 .. 200000) {
	sleep(1);
	top::get_system_info($si);
	my $handle = top::get_process_info($si, $ps);
	if (0) {
		hash_dump("system info", $si);
	} else {
		print $clear;
	}

	#
	# Here is the load average
	#
	print("load averages");
	for my $i (0 .. 2) {
                my $value = top::ptrvalue($si->{load_avg},$i);
                printf("%s %5.2f", $i == 0 ? ":" : ",", $value);
	}
	printf("\t\t\t\t       %2d:%02d:%02d\n", reverse((localtime())[0..2]));

	#
	# Proc states.
	#
	printf("%d processes: ", $si->{p_total});
	for my $i (0 .. $#procstates) {
                my $value = top::ptrvalue($si->{procstates},$i);
		next	unless $value;
                printf("%d%s", $value, $procstates[$i]);
	}
	print("\n");

	#
	# Take care of cpu states.
	#
	my $sum = 0;
	for my $i (0 .. $#cpustates) {
		$sum += top::ptrvalue($si->{cpustates}, $i);
	}
	$sum /= 100.0;
	for my $i (0 .. $#cpustates) {
                my $percent = top::ptrvalue($si->{cpustates},$i)/$sum;
                my $value   = $percent == 100.0
                                ? "100"
                                : sprintf("%4.1f", $percent);
                printf("%s %4s%% %s",
                        $i == 0 ? "CPU states:" : ",", $value,
                        $cpustates[$i]);
	}
	print("\n");

	#
	# Here comes memory information.
	#
	print("Mem: ");
	for my $i (0 .. $#memory) {
                my $value = top::ptrvalue($si->{memory},$i);
		next	if $value == 0;
                print(memfix("$value$memory[$i]"));
	}
	print("\n");

	#
	# Now for swap information.
	#
	print("Swap: ");
	for my $i (0 .. $#swap) {
                my $value = top::ptrvalue($si->{swap},$i);
		next	if $value == 0;
                print(memfix("$value$swap[$i]"));
	}
	print("\n");

	print("    ", top::full_format_header("USERNAME"), "\n");
	for my $p (1 .. $si->{p_total}) {
		printf("%2d: %s\n",
			$p, top::full_format_next_process($handle));
	}
}

sub memfix ($) {
	my $label = shift;
	if ( $label =~ m/(\d+)K/ && $1 > 8192 ) {
		my $M = int($1 / 1024);
		$label =~ s#$1K#${M}M#;
	}
	return $label;
}

sub names ($) {
	my $ref   = shift;
	my @names = ();
	for my $i (0..100) {
		my $val = top::ptrvalue($ref, $i);
		last if $val eq "NULL";
		push(@names, $val);
	}
	return(@names);
}

sub hash_dump ($$) {
	my $name = shift;
	my $href = shift;
	printf("$name is $href.\n");
	my $key;
	for $key (sort(keys(%$href))) {

		my $ref = ref($href->{$key});
		if ($ref eq "ARRAY") {
			printf("   %-16s (%s)\n", $key,
				join(", ", @{$href->{$key}}));
		} elsif ($ref eq "charPtrPtr") {
			printf("   %-16s %s:\n", $key, $href->{$key});
			for my $i (0..20) {
				my $val = top::ptrvalue($href->{$key},$i);
				last if $val eq "NULL";
				printf("    %2d: \"%s\"\n", $i, $val);
			}
		} else {
			printf("   %-16s %s\n", $key, $href->{$key});
		}
	}
}
