#!/usr/bin/perl -w

#
# Use the first N elements of the Fibonacci sequence to approximate
# the Golden Mean.
#

use strict;
use elapsed;

my $before = elapsed::elapsed_seconds();

my $N = @ARGV ? shift : 20;	# Take either the first argument or 20.
my ($n1, $n2) = (1, 1);
printf("%2d:  %10d\n", 1, $n1);
printf("%2d:  %10d  %.10g\n", 2, $n2, $n2/$n1);
for (3..$N) {
	($n1, $n2) = ($n2, $n1 + $n2);
	printf("%2d:  %10d  %.10g\n", $_, $n2, $n2/$n1);
}
my $after = elapsed::elapsed_seconds();

printf("Elapsed time is %g seconds.\n", $after - $before);
