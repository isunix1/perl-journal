#include <stdio.h>

#define	DEFAULT	"world"	/* The default subject */
char *subject = 0;	/* A user defined subject */

void
greeting(unsigned number)
{
	char	*whom = subject ? subject : DEFAULT;

	switch(number) {
	case 0:
		printf("Hello\n");
		break;
	case 1:
		printf("Hello %s\n", whom);
		break;
	default:
		printf("Hello %ss\n", whom);
		break;
	}
}
