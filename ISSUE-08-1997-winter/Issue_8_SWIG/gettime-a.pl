#!/usr/bin/perl -w

use gettime;

my $tv  = new timeval();                # Allocate a timeval structure
gettime::gettimeofday($tv, undef)       # Note undef maps to a null pointer
                && warn("gettimeofday() failed, errno = $gettime::errno.\n");
printf("Time is %d.%06d\n",
        $tv->{tv_sec},          # It is the shadow option that allows these
        $tv->{tv_usec});        # symbolic references to structure fields.
