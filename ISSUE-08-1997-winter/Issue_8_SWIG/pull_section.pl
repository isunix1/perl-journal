#!/usr/bin/perl -w

use strict;

my $sect = shift;
die("Must provide a section name on the command line.\n")	unless $sect;

my $echo = 0;
while(<>) {
	$echo = 0	if /^#\s*SECTION/;
	print if $echo;
	$echo = 1	if /^#\s*SECTION\s*=\s*$sect\b/;
	
}
