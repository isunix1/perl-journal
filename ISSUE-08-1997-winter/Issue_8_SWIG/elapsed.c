#include <sys/time.h>

/*
   Return the number of seconds since the first time elapsed_seconds()
   was called.
 */
float
elapsed_seconds(void)
{
	static struct timeval then = {0, 0};
	struct timeval  now;

	/*
	 * The first time we're called note the time and then return 0.
	 */
	if (then.tv_sec == 0) {
		(void) gettimeofday(&then, 0);
		return (0);
	}

	/*
	 * On subsequent calls return the elapsed time as a floating point
	 * number.
	 */
	(void) gettimeofday(&now, 0);
	return (now.tv_sec - then.tv_sec
		+ (now.tv_usec - then.tv_usec) / 1000000.0);
}
