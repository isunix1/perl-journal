# This file was automatically generated by SWIG
package elapsed;
require Exporter;
require DynaLoader;
@ISA = qw(Exporter DynaLoader);
package elapsedc;
bootstrap elapsed;
var_elapsed_init();
@EXPORT = qw( );

# ---------- BASE METHODS -------------

package elapsed;

sub TIEHASH {
    my ($classname,$obj) = @_;
    return bless $obj, $classname;
}

sub CLEAR { }


# ------- FUNCTION WRAPPERS --------

package elapsed;

*elapsed_seconds = *elapsedc::elapsed_seconds;

# ------- VARIABLE STUBS --------

package elapsed;

1;
