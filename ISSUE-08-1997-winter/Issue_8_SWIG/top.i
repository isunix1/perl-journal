%{
#include "top.h"
#include "machine.h"

char           *
printable(str)
	char           *str;
{
	int             c;

	for (c = 0; str[c] != '\0'; c++) {
		if (!isprint(str[c]))
			str[c] = '?';
	}
	return (str);
}

char           *
full_format_header(char *uname_field)
{
	return (format_header(uname_field));
}

char           *
full_format_next_process(caddr_t handle)
{
	extern char    *username(int uid);

	return (format_next_process(handle, username));
}

%}

%include pointer.i
%include typemaps.i
%include "top.h"
%pragma make_default
%include "machine.h"

extern char    *full_format_header(char *uname_field);
extern char    *full_format_next_process(caddr_t handle);

extern int      machine_init(struct statics * statics);
extern void     get_system_info(struct system_info * si);
extern caddr_t  get_process_info(struct system_info * si,
				 struct process_select * sel,
				 int fake = 0);
