#!/usr/bin/perl -w

use gettime;

my $tv  = new timeval();		# Allocate timeval structure.
$tv->{tv_sec} = $tv->{tv_usec} = 0;	# Turn back the clock.
if (gettime::settimeofday($tv, undef)) {# Will fail unless running as root
	warn("settimeofday() failed, errno = $gettime::errno.\n");
	$gettime::errno = 0;		# This will fail since it is readonly
}
