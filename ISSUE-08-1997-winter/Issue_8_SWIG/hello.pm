# This file was automatically generated by SWIG
package hello;
require Exporter;
require DynaLoader;
@ISA = qw(Exporter DynaLoader);
package helloc;
bootstrap hello;
var_hello_init();
@EXPORT = qw( );

# ---------- BASE METHODS -------------

package hello;

sub TIEHASH {
    my ($classname,$obj) = @_;
    return bless $obj, $classname;
}

sub CLEAR { }


# ------- FUNCTION WRAPPERS --------

package hello;

*greeting = *helloc::greeting;

# ------- VARIABLE STUBS --------

package hello;

*DEFAULT = *helloc::DEFAULT;
*subject = *helloc::subject;
1;
