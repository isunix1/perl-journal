#!/usr/local/bin/perl -w

use strict;
use top;

#
# Create instances of the data structures that we'll be using.
#
my($statics) = new statics();		# contains field names
my($si)	     = new system_info();	# contains field data
my($ps)	     = new process_select();	# contains array of process listings
$ps->{idle}     =  1;
$ps->{"system"} =  0;
$ps->{uid}      = -1;

#
# Initialize the machine characteristics structure
# and pull the field names for later use. The
# routine name() pulls strings from the array until
# NULL is encountered.
#
top::machine_init($statics);
my(@procstates) = names($statics->{procstate_names});
my(@cpustates)  = names($statics->{cpustate_names});
my(@memory)     = names($statics->{memory_names});

top::get_system_info($si);
for (0 .. 60) {
	sleep(1);
	top::get_system_info($si);
	my $handle = top::get_process_info($si, $ps);

	#
	# Here is the load average
	#
	print("load averages");
	for my $i (0 .. 2) {
                my $value = top::ptrvalue($si->{load_avg},$i);
                printf("%s %5.2f", $i == 0 ? ":" : ",", $value);
	}
	printf("\t\t\t\t       %2d:%02d:%02d\n", reverse((localtime())[0..2]));

	#
	# Proc states.
	#
	printf("%d processes: ", $si->{p_total});
	for my $i (0 .. $#procstates) {
                my $value = top::ptrvalue($si->{procstates},$i);
                printf("%d%s", $value, $procstates[$i])	if $value;
	}
	print("\n");

	#
	# Take care of cpu states.
	#
	my $sum = 0;
	for my $i (0 .. $#cpustates) {
		$sum += top::ptrvalue($si->{cpustates}, $i);
	}
	$sum /= 100.0;
	for my $i (0 .. $#cpustates) {
                my $percent = top::ptrvalue($si->{cpustates},$i)/$sum;
                my $value   = $percent == 100.0
                                ? "100"
                                : sprintf("%4.1f", $percent);
                printf("%s %4s%% %s",
                        $i == 0 ? "CPU states:" : ",", $value,
                        $cpustates[$i]);
	}
	print("\n");

	#
	# Here comes memory information.
	#
	print("Mem:  ");
	for my $i (0 .. $#memory) {
                my $value = top::ptrvalue($si->{memory},$i);
                print("$value$memory[$i]")	if $value;
	}
	print("\n");

	print("    ", top::full_format_header("USERNAME"), "\n");
	for my $p (1 .. $si->{p_total}) {
		printf("%2d: %s\n",
			$p, top::full_format_next_process($handle));
	}
}

sub names ($) {
	my $ref   = shift;
	my @names = ();
	for my $i (0..100) {
		my $val = top::ptrvalue($ref, $i);
		last if $val eq "NULL";
		push(@names, $val);
	}
	return(@names);
}
