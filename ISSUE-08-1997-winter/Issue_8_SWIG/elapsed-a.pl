#!/usr/local/bin/perl -w

use strict;
use elapsed;

if (0) {
	my $a = elapsed::elapsed_seconds();
	sleep(1);
	my $b = elapsed::elapsed_seconds();
	sleep(1);
	my $c = elapsed::elapsed_seconds();
	printf("a is %g.\n", $a);
	printf("b is %g.\n", $b);
	printf("c is %g.\n", $c);
}

my $max = @ARGV ? shift : 3000;
for(0..$max) {
	printf("%4d: loop time is %g.\n", $_, elapsed::elapsed_seconds());
}
