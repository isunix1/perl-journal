#!/usr/bin/perl -w

use strict;

my $sub = shift;
die("Must provide a subroutine name on the command line.\n")	unless $sub;

my $echo = 0;
while(<>) {
	$echo = 1	if /^sub\s+$sub\b/;
	print if $echo;
	$echo = 0	if /^}/;
	
}
