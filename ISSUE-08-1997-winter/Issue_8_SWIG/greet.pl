#!/usr/local/bin/perl -w

use strict;
use hello;

printf("The DEFAULT constant is \"%s\".\n", $hello::DEFAULT);
hello::greeting(0);
hello::greeting(1);
hello::greeting(2);
$hello::subject = "oyster";
hello::greeting(3);
hello::greeting('a');
