%module	gettime;	// Alternate to command line arguments for naming

%readonly		// Make all variables read only.
int errno;
%readwrite		// Restore default behavior.

%pragma make_default;	// Ask SWIG to generate default constructors.
struct timeval {
	long    tv_sec;		// seconds
	long    tv_usec;	// and microseconds
};

struct timezone {
	int     tz_minuteswest;	// minutes west of Greenwich
	int     tz_dsttime;	// type of dst correction
};

int gettimeofday(struct timeval * tp,
		 struct timezone * tzp);
int settimeofday(const struct timeval * tp,
		 const struct timezone * tzp);
