From daemon Tue Dec  2 13:38:21 1997
Date: Tue, 2 Dec 1997 11:38:07 -0700 (MST)
From: tipjar@tipjar.com (TipJar LLC)
To: david@tipjar.com, orwant@media.mit.edu
Subject: Direct, not cut and paste

#!/usr/bin/perl
# NewAssignment.pl, by David Nicol

#revision history:
#	date			who		what
#	April 17, 1997		dln		created
#	June 23,1997		dln		added "portion size" and made PreTexts smaller
#	Dec 02,1997		dln		revised script to handle new range.htm format

	
$OnlyTesting = $ARGV[0];	# for real, this is called without arguments

# RegExp fragment to match any (portion size) and (range list)
# following a "landmark" after a minimal number or characters
$RangeMatcher='.*?a range of (\d+) .*? from ([\s0-9to,]+)\.';
#$RangeMatcher=".*a range of (\d+) .* from ([\s0-9to,]+)\.";


# flags to be used in all assignments, before the assignment
# -l2.5 -p300 	means, when the load is above 2.5, pause five minutes
# -i500000	means, print a status report every half million iterations

$PreFlags = " -i500000 -l2.1 -p300";


#############################
#
#    Landmark reference section:
#    last revised December 2, 1997

# the BIG EXPONENTS are now divided into three sections,
# "easiest", "little longer" and "slowest"

# revising December 2: text now reads 
# "These are the easiest exponents to test."
  $PreTextB1="easiest";
  $Flags{'B1'} = "";	# command line flag required to  run this test

#These exponent take a little longer to test
  $PreTextB2="little longer";
  $Flags{'B2'} = "";	# command line flag required to  run this test

#These are the slowest numbers to test
  $PreTextB3="slowest";
  $Flags{'B3'} = "";	# command line flag required to  run this test


# A reclaimed ranges section has been added:
  $PreTextR="Reclaimed";
  $Flags{'R'} = "";


# the text before the Factoring section reads
  $PreTextF="Factoring";
  $Flags{'F'} = "-f ";	# command line flag required to  run this test

# and all the ranges are a period terminated, comma delimited
# series of elements of the form \d+ to \d+

srand(time ^ $$);	# get random.

# edit the next line, leaving only
# the kind(s) of range you want this script 
# to parse out of the web page.

@Suffixes = ('B1','B2','B3','R','F');	# the complete list
#@Suffixes = ('F');			# uncomment to factor only
#@Suffixes = ('R','B2','B3');		# I have a speedy machine



########## end of what needs modified when the range.htm page changes


# choose which kind of search we're doing
 $Suffix = $Suffixes[int rand scalar(@Suffixes)];
 $PreText = ${'PreText'.$Suffix};

print "Chose to search for list after <$PreText>\n";


# it's possible that the network is down.  So we try until lynx -dump returns
# data that match our expectations.  Adding some redundant ppp-up/down code inside
# this loop will make sense if your machine isn't wired to the internet,
# but it should be taken care of by the mprime.sh script
 until ($RangeListing){		# the GETLISTING loop

	# load the "reserve a range" web page into a variable:
	$ReserveARangePage = `lynx -dump http://www.mersenne.org/range.htm`;
	last if ($PortionSize, $RangeListing) = ($ReserveARangePage =~ m/$PreText$RangeMatcher/s);

	if ($OnlyTesting){
		print "Bad parsing for list after <$PreText>\n";
		print " Trying to match <$PreText$RangeMatcher>\n";
		print "got ReserveaRangePage starting ", substr $ReserveARangePage, 0,100;
		die "Only Testing";
	};

	# invoke your ppp-down script here
	# system( 'ppp-down');

	# must have gotten bad data
	die "Bad data from http://www.mersenne.org/range.htm\n" unless $Tries++ < 20;
	sleep 200;	# try once every three hours, twenty minutes, for nearly a week

	# invoke your ppp-up script here
	# system( 'ppp-up');
};


# If the script is running on a really super fast machine and you want a larger
# portion than the reccommended minimum, uncomment and edit this next line:
# $PortionSize *= 2

#print $RangeListing;
$RangeListing =~ s/\s//g;	# lose spaces, tabs, newlines
$RangeListing =~ s/to/ /g;	# have a space instead of the word 'to'

#print $RangeListing;
@Ranges = split /,/,$RangeListing;
#print "\n",@Ranges,"\n"

$MyRange = $Ranges[int rand scalar(@Ranges)];



# narrow MyRange down to a normal size portion
($Low,$High)=split(/ /,$MyRange);

unless ($High - $Low < $PortionSize) {	# for when there's some kind of small missing chink

	#redefine $MyRange to be a normal portion
	$High = $Low + $PortionSize;
	$MyRange="$Low $High";
};


# Define the assignment we have chosen, in final form
$Assignment = "$Flags{$Suffix} $MyRange $PreFlags";
print "Choosing assigment: $Assignment\n" if $OnlyTesting;

# open a stream to the operator's inbox:  assuming you've got
# a mprime alias in /etc/aliases
$Hostname = `hostname`;
open(MAIL, "|mail mprime");
print MAIL <<"END";
Subject: New GIMPS assignment for $Hostname

This message is coming from the mprime script, choosing
a new assignment for $Hostname.

I've randomly chosen to do $Assignment, and have registered it
with George Woltman. 

END

print MAIL "\n Just kidding, this was a test run.  Try again without command line arguments.\n" if  $OnlyTesting;

# Stop now unless we really want to write out an assignment file
# don't overwrite a current assignment when testing regexes
die "only testing" if $OnlyTesting;

# Write $Assignment out to the assignment file
open(ASSIGNMENT, ">assignment") or die "Can't open assignment file, check permissions and ownership";
print ASSIGNMENT $Assignment;
close ASSIGNMENT;


# Stop now unless we really want to reserve our range
# don't annoy Mr. Woltman with tests
die "only testing" if $OnlyTesting;


# Send an E-mail to George Woltman, reserving the range.
open(MAIL,"|mail -s \"Automated assignment choice: $Assignment\" woltman\@magicnet.net");
print MAIL <<"ENDMAIL";

Hi George!

My computer has finished its last assignment and in theory has sent you the
results file.

I have chosen, at random, from http://www.mersenne.org/range.htm,
to do $Assignment

A reply to this message should get back to me if I need to change
that range by 

editing my assignment file, 
killing mprime, 
and running mprime.sh again.

See ya in a couple months!

ENDMAIL




