#!/bin/ash
# This is not a perl script, it is an ash script.
# ash is small; it uses only a quarter of the memory pages bash uses,
# and an even smaller fraction of the pages a Perl process takes.


# put these two lines in your /etc/rc.d/rc.local file:
#	# mprimes project
#	su mprime -c /usr/local/etc/mprime/mprime.sh


cd /usr/local/etc/mprime/		# change to the mprime directory

while
	export ASSIGNMENT="`cat assignment`"

	# put a subject line header at the start of a new results file
	# in the middle, it's okay, it's filterable noise
	echo Subject: results from mprime $ASSIGNMENT >> results
	echo >> results

	date >> results
do 

	# the assignment is in a file called assignment 	# load the assignment variable 

	# while waiting for this to complete, the script should use no CPU and entirely swap out.

	echo Starting mprime $ASSIGNMENT `date` 
	nice -n 15 ./mprime $ASSIGNMENT 


	# quit the loop unless "Please send" appears in the results file
	grep "^Please send the results file to" results || break


	# if execution of the script gets here, we're done with the assignment,
	# so wake up, hand in your results, and ask for another.

	# archive your results
	export ARCHNAME="results`date +%d%b%Y`"
	date >> $ARCHNAME
	echo ARCHIVING RESULTS from assignment $ASSIGNMENT >> $ARCHNAME
	cat results >> $ARCHNAME
	unset ARCHNAME

	# submit your results to George Woltman
	#mail 74473.2626@compuserve.com < results
	#mail woltman@magicnet.net < results
	mail woltman@magicnet.net  < results
	# submit your results to yourself, so you know what you've done
	mail mprime  < results
	
	# delete results file so it's clean for next assignment
	rm results

	# If your machine is not connected directly to internet,
	# you need to bring up your connection before running
	# the perl script; it needs to look at a web page.
	# the commented-out lines below need to be modified for
	# your non-connected system.

	# ppp_up	# this may be a script to reliably get a connection
	# dip isp.dip	# replace this with whatever your system requires
	# wait 40	# my non-net connected machine needs a bit of time
			# to establish its IP connection


	# get a new assignment from the ranges page
	#/usr/bin/perl < NewAssignment.pl
	NewAssignment.pl

	#if you have a .netrc entry that reliably downloads
	#ftp://ftp.mersenne.org/database.gz, uncomment the next line
	#ftp ftp.mersenne.org

	# handle all the mail, then drop the internet connection
	# sendmail -q


	# ppp_down	# the other half of ppp_up
	# dip -k	# drop the connection, if you're still using dip ;-)

done

echo mprime $ASSIGNMENT terminated by error or signal: results not needed sent. | mail mprime


