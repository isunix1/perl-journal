#!perl -wl015

use Mac::AppleEvents;

$params = "kocl:type(cwin), prdt:{ pnam: &#147;Counting&#148; , ppos:[100, 50] }";

$evt = AEBuildAppleEvent('core','crel',typeApplSignature,'PBar',0,0,$params) || die $^E;
$rep = AESend($evt, kAEWaitReply) || die $^E;

print AEPrint($evt)
print AEPrint($rep);
AEDisposeDesc($evt);
AEDisposeDesc($rep);

# NOTES:
#    \015 is the Mac linebreak character, not \012!
#    curly quotes (option-[ and option-shift-[) go around items of 
#       type TEXT.  Remember, these are non-ASCII characters.
#    $^E is similar to $!, but gives platform-specific error info
#    AEs don't do automatic garbage collection, so 
#        we dispose of the event with AEDisposeDesc
