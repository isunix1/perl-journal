#!perl -w
use strict;
use Mac::Apps::PBar;

#create window
my $bar = Mac::Apps::PBar->new('FTP Download','100, 50');

#Module sets each element of hashref in turn
$bar->data({Cap1=>'file: BigFile.tar.gz',Cap2=>'size: 1,230K',MinV=>'0',MaxV=>'1230'});

#set the value of the bar incrementally for 11 iterations
for(0..10) {
	$bar->data({Valu=>$_*123});
	sleep(1);
}

sleep(5);
$bar->close_window;

