#!/usr/bin/perl
#
# The Safety Net Solutions, Inc. Logo as a Gimp plug-in
#
# Written by Aaron Sherman, and distributed under the same terms
# as the Gimp itself. See http://www.gimp.org/ for details.
#
# This is an example script. For purposes of distribution of the logo
# which this program generates the Safety Net logo is a trademark of
# Safety Net Solutions, Inc. All rights reserved. (they made me say it ;)

# Initialize the Gimp library modules
use Gimp qw(:auto);  # The core Gimp API
use Gimp::Fu;        # Gimp registration and data types
# use Gimp::Util;    # Gimp helper functions

use strict;

# Our plug-in function:
sub perl_fu_safety_logo {
    my $width  = shift;
    my $height = shift;
    my $img = gimp_image_new($width, $height, RGB); # Create the new image
    
    # Add a layer for us to work in:
    my $layer = gimp_layer_new($img, $width, $height, RGB_IMAGE,
			       "Safety Logo", 100, NORMAL_MODE);
    gimp_image_add_layer($img, $layer, 0);
    
    # Add background
    my $oldcolor = gimp_palette_get_foreground();
    gimp_palette_set_foreground(gimp_palette_get_background());
    gimp_selection_all($img);
    gimp_bucket_fill($layer, FG_BUCKET_FILL, NORMAL_MODE, 100, 0, 0, 0, 0);
    
    # Draw the vertical and horizontal axes:
    gimp_palette_set_foreground($oldcolor);
    gimp_selection_none($img);
    gimp_paintbrush($layer,0,
		    [$width/2, $height/18, $width/2, $height-$height/18]);
    gimp_paintbrush($layer,0,
		    [$width/18, $height/2, $width-$width/18, $height/2]);
    
    # Draw the diagonal axes:
    my $magic = ($width/18*7) / sqrt(2);
    gimp_paintbrush($layer,0,
		    [$width/2-$magic, $height/2-$magic,
		     $width/2+$magic, $height/2+$magic]);
    gimp_paintbrush($layer,0,
		    [$width/2-$magic, $height/2+$magic,
		     $width/2+$magic, $height/2-$magic]);
    
    # Draw the concentric ellipses:
    for (my $i = 0; $i < 4; $i++) {
	gimp_ellipse_select($img, $width/18*(2+$i),    $height/18*(5+$i),
			    $width/18*(14-$i*2), $height/18*(8-$i*2),
			    SELECTION_REPLACE, 1, 0, 0);
	gimp_edit_stroke($layer);
	gimp_ellipse_select($img, $width/18*(5+$i),   $height/18*(2+$i),
			    $width/18*(8-$i*2), $height/18*(14-$i*2),
			    SELECTION_REPLACE, 1, 0, 0);
	# gimp_edit_stroke will use current foreground color and brush
	gimp_edit_stroke($layer);
    }
    
    # Finish up, and display:
    gimp_selection_none($img);
    gimp_displays_flush();
    return $img;
}

# Register the plug-in:
register("safety_logo", "Render a stand-alone Safety Net Logo image",
	 "Renders the Safety Net Solutions company logo in the " .
	 "currently selected brush and fg/bg colors.",
	 "Aaron Sherman", "(c) 1999, Aaron Sherman",
	 "1999-03-29", "<Toolbox>/Xtns/Render/Safety Logo", "*",
	 [
	  [PF_INT32, "Width",  "Width",  256],
	  [PF_INT32, "Height", "Height", 256]
	  ],
	 \&perl_fu_safety_logo);

# Call Gimp::Fu's main():
exit main();
