#!/usr/bin/perl -w
# 
# mail_attach.pl -- Mail files as attachments
#
# Parameters:
#
# Parameter 1) The sender e-mail address
# Parameter 2) Filename with main message in it.
# Parameter 3) Filename to attach. If more than one, separate with commas
#              and *no* spaces
# Parameter 4) Subject
# Parameter 5+) Destination e-mail addresses. If there are spaces in
#               an address, then enclose it in quotes

use strict;
use Socket;
use MIME::Lite;
use Net::SMTP;

my %ending_map = (     crt => ['application/x-x509-ca-cert' , 'base64'],
                      aiff => ['audio/x-aiff' , 'base64'],
                       gif => ['image/gif' , 'base64'],
                       txt => ['text/plain' , '8bit'],
                       com => ['text/plain' , '8bit'],
                     class => ['application/octet-stream' , 'base64'],
                       htm => ['text/html' , '8bit'],
                      html => ['text/html' , '8bit'],
                     htmlx => ['text/html' , '8bit'],
                       htx => ['text/html' , '8bit'],
                       jpg => ['image/jpeg' , 'base64'],
                       dat => ['text/plain' , '8bit'],
                       hlp => ['text/plain' , '8bit'],
                        ps => ['application/postscript' , '8bit'],
                    'ps-z' => ['application/postscript' , 'base64'],
                       dvi => ['application/x-dvi' , 'base64'],
                       pdf => ['application/pdf' , 'base64'],
                       mcd => ['application/mathcad' , 'base64'],
                      mpeg => ['video/mpeg' , 'base64'],
                       mov => ['video/quicktime' , 'base64'],
                       exe => ['application/octet-stream' , 'base64'],
                       zip => ['application/zip' , 'base64'],
                       bck => ['application/VMSBACKUP' , 'base64'],
                        au => ['audio/basic' , 'base64'],
                       mid => ['audio/midi' , 'base64'],
                      midi => ['audio/midi' , 'base64'],
                     bleep => ['application/bleeper' , '8bit'],
                       wav => ['audio/x-wav' , 'base64'],
                       xbm => ['image/x-xbm' , '7bit'],
                       tar => ['application/tar' , 'base64'],
                  imagemap => ['application/imagemap' , '8bit'],
                       sit => ['application/x-stuffit' , 'base64'],
                       bin => ['application/x-macbase64'],
                       hqx => ['application/mac-binhex40' , 'base64'],
);

my $mail_from    = shift @ARGV;
my $message_file = shift @ARGV;
my @mail_file    = split(",", shift(@ARGV));
my $subject      = shift @ARGV;
my @mail_to      = @ARGV;

# The MIME object
my $mime;

# Slurp in the message text and build up the main part of the mail.
{
    local $/;
    $/ = undef;
    local @ARGV;
    @ARGV = $message_file;
    my $main_message = <>;
    $mime = new MIME::Lite(  From => $mail_from, 
                               To => [@mail_to],
                          Subject => $subject,
			     Type => 'text/plain',
			     Data => $main_message);
}

# Attach each file in turn
foreach (@mail_file) {
    my($type, $ending);
    # Snag the ending
    /.*\.(.+)$/;
    $ending = $1;
    # Is it in our list?
    if (exists($ending_map{$ending})) {
	# Yes, so use the type and encoding from the list
        $type = $ending_map{$ending};
    } else {
	# Nope, so default to text/plain
        $type = ['text/plain', '8bit'];
    }

    # Attach it to the message
    $mime->attach(    Type => $type->[0], 
                  Encoding => $type->[1],
                      Path => $_);
}

# Tell MIME::Lite to use Net::SMTP instead of sendmail.
MIME::Lite->send('smtp', 'localhost', Timeout => 20);
$mime->send;

