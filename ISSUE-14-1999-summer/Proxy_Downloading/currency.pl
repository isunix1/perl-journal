#!/usr/local/bin/perl -w

# Currency converter.
# Usage: currency [amount] [from curr] [to curr]

# We don't need LWP::Simple...
# use LWP::Simple;

# We need these two instead.
use LWP::UserAgent;
use HTTP::Request::Common;

# We won't be using get() this time...
# $_ = get("http://www.oanda.com/converter/classic?value=$ARGV[0]&exch="
#          . uc($ARGV[1]) . "&expr=" . uc($ARGV[2]));

$ua = new LWP::UserAgent();
# Set up your proxy server in the next line.
$ua->proxy('http','http://proxy.mycompany.com:1080');
$resp = $ua->request(GET "http://www.oanda.com/converter/classic?value="
                             . "$ARGV[0]&exch=" . uc($ARGV[1]) . "&expr="
                             . uc($ARGV[2]));
$_ = $resp->{_content};
s/^(.*<!-- conversion result starts)(.*)(<!-- conversion result ends.*)$/$2/s;
s/<[^>]+>//g;
s/[ \n]+/ /gs;
print $_, "\n";
