#!perl -w
use strict;
use Socket;

use vars qw($my_mail_host);
sub smailer($\@\@);

$my_mail_host = "localhost";
my @to = ('<me@localhost>','<you@localhost>');
my $from = '<me@localhost>';
my @message = split(/\n/, <<END_OF_MESSAGE);
From: me@localhost
To: you@localhost
Cc: me@localhost
Subject: Some test mail

Hi! This is some test mail. Boring, huh? :-)
END_OF_MESSAGE;

my $status = smailer($from, @to, @message);
if ($status) {
    if (scalar(%bad_addresses) > 0) {
	print "These addresses didn't work: ",
	      join(' ', keys(%bad_addresses)), "\n";
    } else {
	print "Send failed, code $status ($status_message)\n";
    }
}

# smailer - quicko sub to send mail. Takes from, a reference to an array
# with the to addresses in it, and a reference to an array with
# the actual formatted mail message in it, minus line terminators.
#
# When called in scalar context, this sub returns true or false,
# depending on whether it succeeded or not.
#
# When called in list context, it returns a status, status message,
# and a flattened hash of addresses and fail messages for each
# address, like so:
#
# ($status, $message, %bad_addresses) = smailer($from, @to, @message);
#
# This needs a use Socket to properly work--it's not here since that's best
# placed elsewhere.
sub smailer ($\@\@){
  my ($from, $to_ref, $message_ref) = @_;

  local($/);
  $/ = "\cJ"; # Make sure that we're set to what we want, regardless
              # of the state of the rest of the world. Lines should be
              # CRLF, but often they're just LF because off
              # implementation errors or folks misunderstanding
              # \n. Either way, a linefeed will be the last character
              # on a line

  # Declare the name of the host we're pawning the mail off onto. By
  # default we try localhost--this is the best option if we've got a
  # mailserver on this machine. If not, change it to another host. *YOU
  # MUST HAVE MAIL RELAY PERMISSION ON THIS HOST!*
  my $mailhost_name = $my_mail_host || "localhost";
  my $mailhost_ip = ""; # The IP address for the mailhost. If left blank,
                        # we'll go try to figure it out. Should be in
                        # packed format, so no 1.2.3.4 here.
  my $mailhost_port = 25; # The port to connect to. Except in the most
                          # bizarre of circumstances, this'll be 25.
  my $mailhost_paddr; # Where the packed IP address & port will get stuck
  my $we_are = ""; # Who we are. Fill this in if your mailserver needs to
                   # know. The only way to find this out locally is with
                   # POSIX::uname. Not everyone has it, and POSIX is a
                   # memory pig anyway. If you don't, we'll try a reverse
                   # lookup from the IP address on this end of things after
                   # the connection to the mailserver.

  my $they_said;   # What the other end might have said
  my @bad_addresses; # Any recipient addresses the other end didn't like

  # Translate the port to a number if it's a name
  $mailhost_port = getservbyname($mailhost_port, 'tcp')  if $mailhost_port =~ /\D/;
  # Figure the IP address if we need to
  $mailhost_ip ||= inet_aton($mailhost_name);
  # Build the packed socket address
  $mailhost_paddr = sockaddr_in($mailhost_port, $mailhost_ip);

  # Create the socket
  socket(MAILSOCK, PF_INET, SOCK_STREAM, getprotobyname('tcp')) or die "socket:$!";
  # Open the socket
  connect(MAILSOCK, $mailhost_paddr) or die "Connect error: $!";
  # Dup it
  open(MAILOUTSOCK, ">&MAILSOCK") or die "Erro dupping, $! $^E";
  select(MAILSOCK);
  $| = 1;
  select(MAILOUTSOCK);
  $| = 1;
  select(STDOUT);

  # Unless we know who we are, we'd better go figure it out
  my $stuff = getsockname(MAILSOCK) or die "Hey! $^E $!";
  my ($foo, $bar) = unpack_sockaddr_in($stuff);
  $we_are = gethostbyaddr($bar, AF_INET) unless $we_are;

  # Talk to the server. First fetch the initial 'hi there' message
  $they_said = <MAILSOCK>;
  # Make sure we like it
  if (substr($they_said, 0, 1) ne '2') {
      # not a 2 response. Bail
      close(MAILSOCK);
      close(MAILOUTSOCK);
      if (wantarray) {
	  return undef, "Initial connection failed: $they_said";
      } else {
	  return undef;
      }
  }
  
  # Say hi
  syswrite(MAILOUTSOCK, "HELO $we_are\cM\cJ", length($we_are) + 7);

  # Wait for them to say hi back
  $they_said = <MAILSOCK>;
  # Make sure we like it
  if (substr($they_said, 0, 1) ne '2') {
      # not a 2 response. Bail
      close(MAILSOCK);
      close(MAILOUTSOCK);
      if (wantarray) {
	  return undef, "HELO failed: $they_said";
      } else {
	  return undef;
      }
  }

  # Tell it who the mail's from
  syswrite(MAILOUTSOCK, "MAIL FROM: $from\cM\cJ", length($from) + 13);

  # was it OK?
  $they_said = <MAILSOCK>;
  chomp $they_said;
  # Make sure we like it
  if (substr($they_said, 0, 1) ne '2') {
      # not a 2 response. Bail.
      close(MAILSOCK);
      close(MAILOUTSOCK);
      if (wantarray) {
	  return undef, "MAIL FROM failed: $they_said";
      } else {
	  return undef;
      }
  }

  # Tell 'em who it's going to
  foreach my $recipient (@$to_ref) {
      # Tell it who the mail's from
      syswrite(MAILOUTSOCK, "RCPT TO: $recipient\cM\cJ", length($recipient) + 11);
      
      # was it OK?
      $they_said = <MAILSOCK>;
      chomp $they_said;
      # Make sure we like it
      if (substr($they_said, 0, 1) ne '2') {
	  # not a 2 response. Save it off for later
	  $bad_addresses{$recipient} = $they_said;
      } else {
	  # At least one of the names was OK
	  $going_someplace = 1;
      }
  }
  
  # Did the other end not like anyone?
  if (!$going_someplace) {
      close(MAILSOCK);
      close(MAILOUTSOCK);
      if (wantarray) {
	  return undef, "No valid addresses", %bad_addreses;
      } else {
	  return undef;
      }
  }
  
  # Time for the message
  syswrite(MAILOUTSOCK, "DATA\cM\cJ", 6);
  
  # was it OK?
  $they_said = <MAILSOCK>;
  chomp $they_said;
  # Make sure we like it
  if ((substr($they_said, 0, 1) ne '2')&& (substr($they_said, 0, 1) ne '3')) {
      # not a 2 or 3 response. Bail
      close(MAILSOCK);
      close(MAILOUTSOCK);
      if (wantarray) {
	  return undef, "DATA failed: $they_said";
      } else {
	  return undef;
      }
  }
  

  # Send the message. If a line's got just a period, then send a double
  # period. (SMTP protocol dictates that a message ends with a single
  # period, and we don't want it ending before we're ready)
  foreach my $line (@$message_ref) {
    if ($line eq '.') {
      syswrite(MAILOUTSOCK, "..\cM\cJ", 4);
    } else {
      syswrite(MAILOUTSOCK, "$line\cM\cJ", length($line)+2);
    }
  }
    
  # 'Kay, send the closing period
  syswrite(MAILOUTSOCK, ".\cM\cj", 3);
  # Did they like the mail?
  $they_said = <MAILSOCK>;
  chomp $they_said;
  # Make sure we like it
  if (substr($they_said, 0, 1) ne '2') {
      # not a 2 response. Bail.
      close(MAILSOCK);
      close(MAILOUTSOCK);
      if (wantarray) {
	  return undef, "DATA failed: $they_said";
      } else {
	  return undef;
      }
  }
  

  # Go away
  syswrite(MAILOUTSOCK, "QUIT\cM\cJ", 6);
  # Quit really ought not ever fail, but just in case...
  $they_said = <MAILSOCK>;
  chomp $they_said;
  # Make sure we like it
  if (substr($they_said, 0, 1) ne '2') {
      # not a 2 response. Bail.
      close(MAILSOCK);
      close(MAILOUTSOCK);
      if (wantarray) {
	  return undef, "QUIT failed: $they_said";
      } else {
	  return undef;
      }
  }

  close MAILSOCK;
  close MAILOUTSOCK;
  return 1;

}
