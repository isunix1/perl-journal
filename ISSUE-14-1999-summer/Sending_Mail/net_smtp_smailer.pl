#!/usr/bin/perl -w
use Net::SMTP;

# use the default mailhost configured into the libnet package
my $smtp = Net::SMTP->new;

# First the envelope bits
$smtp->mail('me@localhost');
$smtp->to('me@localhost');
$smtp->to('you@localhost');

# Now for the message itself
$smtp->data();
$smtp->datasend("From: me\@localhost\n");
$smtp->datasend("To: you\@localhost\n");
$smtp->datasend("Cc: me\@localhost\n");
$smtp->datasend("Subject: Some test mail\n");
$smtp->datasend("\n");
$smtp->datasend("Hi! This is some test mail. Boring, huh? :-)\n");
$smtp->dataend();

$smtp->quit;
