#!/usr/bin/perl -w
# mail_mailer.pl - sample mail sending routine using Mail::Mailer
use Mail::Mailer qw(smtp);

my $mailer = new Mail::Mailer 'smtp';

# Hash keys are headers in the outgoing mail
$mailer->open(To => ['you@localhost'],
	      Cc => ['me@localhost'],
	      From => 'me@localhost',
	      Subject => 'Some test mail');

print $mailer "Hi! This is some test mail. Boring, huh? :-)\n";

$mailer->close;

