#!/usr/local/bin/perl -T

use strict;
use CGI qw(param header -no_debug);
use CGI::Carp qw(fatalsToBrowser);
ENV{'PATH'} = '/bin:/usr/bin';

use constant GNUPLOT  => '/usr/local/bin/gnuplot';
use constant LOGFILES => '/home/www/logs';
$| = 1;

# if REQUEST_METHOD is set, then we're a CGI script,
# so we get the logfile name with param() and generate
# the GIF image.
unless (@ARGV) {
    my $logfile = param('file');
    die "Bad log file name: $logfile\n"
	unless $logfile =~ /^([a-zA-Z][\w.-]*)$/;
    $logfile = LOGFILES . "/$1";
    die "Can't open log file $logfile\n" unless -r $logfile;
    generate_gif($logfile);
} 
# otherwise we're running as a regular program, and we
# parse the log file for use by GNUPLOT
else {
    generate_data();
}

# Make the GIF image (as a CGI script)
sub generate_gif {
    my $logfile = shift;
    print header('image/gif');
    open (GP,"|".GNUPLOT) || die "Couldn't open GNUPLOT: $!";
    while (<DATA>) {
	print GP $_;
    }
    print GP "plot '< $0 $logfile'";
    close GP;
}

# Generate the data for use by GNUPLOT
sub generate_data {
    my %HITS;
    while (<>) {
	next unless m!\[\d+/\w+/\d{4}:(\d+):\d+:\d+ [\d+-]+\]!;
	my $hour = $1;
	$HITS{$hour}++;
    }
    foreach (sort {$a<=>$b} keys %HITS) {
	print join("\t",$_,$HITS{$_}),"\n";
    }
}

     __DATA__
     set terminal gif small size 640,480 interlace
     set border
     set boxwidth
     set nogrid
     set nokey
     set nolabel
     set data style boxes
     set noxzeroaxis
     set noyzeroaxis
     set tics out
     set xtics nomirror 0,1,23
     set ytics nomirror
     set xlabel "Hour" 0,0
     set xrange [ -0.75 : 23.75]
     set ylabel "Hits" 0,0
