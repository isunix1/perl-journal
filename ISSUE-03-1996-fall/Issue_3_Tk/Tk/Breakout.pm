
package Tk::Breakout;

# Class "Breakout": constructor, methods, destructor, global class data,
# etcetera.
#
# Stephen O. Lidie, Lehigh University Computing Center, lusol@Lehigh.EDU. 
# 96/07/22

require 5.002;
use English;
use Tk;
use Tk::Canvas;
use Tk::DoOneEvent;
use strict;
@Tk::Breakout::ISA = qw(Tk::Derived Tk::Canvas);
Tk::Widget->Construct('Breakout');

=head1 NAME

 Breakout() - a class module derived from the canvas widget, faking a game
 of Breakout.

=head1 SYNOPSIS

 use Tk::Breakout;

 $breakout = $MW->Breakout(
     -balls  => [{-color    => 'yellow',  -size     => 40,
		  -position => [90, 250], -velocity => [6.0, 9.0]},
                ],
     (other canvas creation options ...)
 );

=head1 DESCRIPTION

 Essentially, move_balls() is invoked to move all of the balls in a
 games's ball list once - from their current to their new postion.
 After moving one ball a call to DoOneEvent() is made to handle pending 
 XEvents.  The *user* of this module has their own main loop which calls
 DoOneEvent() and move_balls() to keep the game active.

 The -balls configure attribute is a list of hash.  Each ball has a color,
 a diameter in pixels, a position given by a canvas X/Y coordinate pair, 
 and a velocity that describes how many pixels to translate the ball in
 the X and Y dimensions for each game tick.

=head1 METHODS

=head2 $breakout->move_balls($speed);

 $speed is the ball speed, as a percentage.

=head1 AUTHOR

 Stephen O. Lidie <lusol@Lehigh.EDU>

=head1 HISTORY

 lusol@Lehigh.EDU, LUCC, 96/07/22
  . Original version 1.0 release.

=head1 COPYRIGHT

 Copyright (C) 1996 - 1996 Stephen O. Lidie. All rights reserved.

 This program is free software; you can redistribute it and/or modify it under
 the same terms as Perl itself.

=cut

sub Populate {

    # Breakout constructor, derived from the canvas widget.
    #
    # We have been called to at last populate the derived widget $dw; call
    # superclass Popluate to pre-massage the widget arguments, then construct
    # the widget, initialize instance data and specify configure() options.
    # 
    # The actual balls and the canvas cursor are configured when Populate()
    # returns.

    my($dw, $args) = @ARG;

    $dw->SUPER::Populate($args);

    my($canvw, $canvh) = ($dw->cget(-width)/2, $dw->cget(-height)/2);
    my($padw, $padh) = (50/2, 6/2);
    my @paddle_shape = ($canvw - $padw, $canvh - $padh,
			$canvw + $padw, $canvh + $padh);
    my $paddle = $dw->create('rectangle', @paddle_shape,
        -fill    => 'orange',
	-outline => 'orange',
    );
    $dw->{'paddle_ID'}     = $paddle;
    $dw->{'last_paddle_x'} = $paddle_shape[0];
    $dw->{'last_paddle_y'} = $paddle_shape[1];

    $dw->Tk::bind('<Motion>' => \&move_paddle);

    $dw->ConfigSpecs(
        -balls  => ['METHOD', undef, undef, [{}]],
        -cursor => ['SELF', undef, undef,
                      ['@images/canv_cur.xbm', 'images/canv_cur.mask',
		       ($dw->configure(-background))[4], 'orange']
                   ],
    ); 

    return $dw;

} # end Populate, Breakout constructor

sub move_one_ball {

    # Move one ball, belonging to one game, one tick, watching for that
    # danged paddle all the while.

    my($canvas, $ball, $speed) = @ARG;

    $speed     = 1.0 unless defined $speed;
    my $fudge  = 3;
    my $radius = $ball->{'radius'};
    my $ballx  = $ball->{'pos'}[0];
    my $bally  = $ball->{'pos'}[1];
    my $velx   = $ball->{'vel'}[0];
    my $vely   = $ball->{'vel'}[1];
    my $minx   = $radius + $fudge;
    my $maxx   = $canvas->Width  - $minx - $fudge;
    my $miny   = $radius + $fudge;
    my $maxy   = $canvas->Height - $miny - $fudge;
    my $bid    = $ball->{'ball_ID'};
    my $sid    = $ball->{'score_ID'};

    if ($ballx <= $minx or $ballx >= $maxx) {
        $velx = -1.0 * $velx;	# reflect ball horizontally
    }

    my($px1, $py1, $px2, $py2) = $canvas->bbox($canvas->{'paddle_ID'});
    my($sweetx, $sweety) = ($ballx, $bally+($radius * ($vely >= 0 ? 1 : -1)));
    my $impact = (($sweetx >= ($px1-$radius) and $sweetx <= ($px2+$radius)) and
		  ($sweety >=  $py1          and $sweety <=  $py2));
    if ($bally <= $miny or $bally >= $maxy or $impact and $speed != 0) {
	if (($impact and
	     $sweetx >= $px1-$radius and $sweetx < $px1 and $velx >= 0) or
	    ($impact and
	     $sweetx <= $px2+$radius and $sweetx > $px2 and $velx <  0)) {
	    $velx = -1.0 * $velx; # paddle edge, re-reflect horizontally
	}
	
	# The ball has impacted the top or bottom wall, or the paddle.  Reflect
	# vertically, update the score and momentarily flash a new ball color.

        $vely = -1.0 * $vely;
	my $score = ($ball->{'score'} += $impact ? 1 : -1);
	$canvas->itemconfigure($sid, -text => $score);
	$canvas->itemconfigure($bid, -fill => $impact ? 'green' : 'red');
	$canvas->after(100,
             sub {$canvas->itemconfigure($bid, -fill => $ball->{'color'})}
        ); 
	
    } # ifend ball impact of some sort

    # Move ball and score items and update instance variables for next time.

    my $deltax = $velx * $speed;
    my $deltay = $vely * $speed;
    $canvas->move($bid, $deltax, $deltay);
    $canvas->move($sid, $deltax, $deltay);
    $ball->{'pos'}[0] = $ballx + $deltax;
    $ball->{'pos'}[1] = $bally + $deltay;
    $ball->{'vel'}[0] = $velx;
    $ball->{'vel'}[1] = $vely;

} # end move_one_ball

sub move_paddle {

    # Process paddle motion.

    my($canvas) = @ARG;

    my $e = $canvas->XEvent;
    my($x, $y) = ($e->x, $e->y);
    $canvas->move($canvas->{'paddle_ID'}, $x - $canvas->{'last_paddle_x'},
		                          $y - $canvas->{'last_paddle_y'});
    ($canvas->{'last_paddle_x'}, $canvas->{'last_paddle_y'}) = ($x, $y);

} # end move_paddle

sub move_balls {

    # Move all the balls belonging to one game instance one tick.  Although
    # not strictly needed because there is typically but a single ball, call
    # DoOneEvent() in case the ball list is large.

    my($canvas, $speed) = @ARG;

    my $ball;
    foreach $ball (@{$canvas->{'balls'}}) {
	$canvas->move_one_ball($ball, $speed);
        DoOneEvent($TK_DONT_WAIT); # be kind and process XEvents as they arise
    }

} # end move_balls

sub balls {

    # This ConfigSpecs method is called to create balls for a game instance.
    # A ball is a canvas oval item and a text item that tags along and shows
    # the ball's score.  All ball instance data, like its position and
    # velocity, is stored in a hash; these ball hashes are kept in a list,
    # which is a game instance variable.  The paddle is raised to the top of
    # the canvas display list so it's never hidden by balls.

    my($canvas, $args) = @ARG;

    my (%ball_defaults) = (
        -color     => 'cyan',
	-size      => 50.0,
	-position  => [245.0,55.0],
	-velocity  => [6.0, 9.0],
    );

    my($plist, %pargs, @margs, $color, $size, $pos, $vel, $radius);
    my($ball, $score);

    # Iterate though the list of hashes, generate default hash values for
    # missing keys, create the canvas oval and text items, and push ball
    # instance data on the the game's ball list.

    foreach $plist (@$args) {
	@margs = grep ! defined $plist->{$ARG}, keys %ball_defaults;
	%pargs = %$plist;
	@pargs{@margs} = @ball_defaults{@margs};
	($color, $size, $pos, $vel) =
	    @pargs{-color, -size, -position, -velocity};
	$radius = $size / 2.0;
	$ball = $canvas->create('oval', 
	    ($pos->[0] - $radius), ($pos->[1] - $radius),
	    ($pos->[0] + $radius), ($pos->[1] + $radius),
	    -fill    => $color,
            -outline => 'black',
	);
	$score = $canvas->create('text', $pos->[0], $pos->[1], -text => '0');
	push @{$canvas->{'balls'}}, {
	    'ball_ID'  => $ball,
	    'score_ID' => $score,
	    'color'    => $color,
	    'radius'   => $radius,
	    'pos'      => [@$pos],
	    'vel'      => [@$vel],
	    'score'    => 0,
        };

    } # forend all balls

    $canvas->raise($canvas->{'paddle_ID'});

} # end balls

1;
