package Tk::DoOneEvent;

require Exporter;
@Tk::DoOneEvent::ISA = qw(Exporter);
@EXPORT = qw($TK_WAIT $TK_DONT_WAIT $TK_X_EVENTS $TK_FILE_EVENTS
             $TK_TIMER_EVENTS $TK_IDLE_EVENTS $TK_ALL_EVENTS);

# Bit patterns describing what you want DoOneEvent() to do, swiped directly
# from the Tk distribution.  If you know where tk.h resides, I suggest using
# "h2ph" to generate the symbol definitions in case the header file changes.

$TK_WAIT         = 0x00;
$TK_DONT_WAIT    = 0x01;
$TK_X_EVENTS     = 0x02;
$TK_FILE_EVENTS  = 0x04;
$TK_TIMER_EVENTS = 0x08;
$TK_IDLE_EVENTS  = 0x10;
$TK_ALL_EVENTS   = $TK_X_EVENTS | $TK_FILE_EVENTS | $TK_TIMER_EVENTS |
	           $TK_IDLE_EVENTS;

1;
__END__
printf("TK_X_EVENTS     =%3x\n", $TK_X_EVENTS);
printf("TK_FILE_EVENTS  =%3x\n", $TK_FILE_EVENTS);
printf("TK_TIMER_EVENTS =%3x\n", $TK_TIMER_EVENTS);
printf("TK_IDLE_EVENTS  =%3x\n", $TK_IDLE_EVENTS);
printf("                  --\n");
printf("TK_ALL_EVENTS   =%3x\n", $TK_ALL_EVENTS);
printf("\nTK_WAIT         =%3x\n", $TK_WAIT);
printf("TK_DONT_WAIT    =%3x\n", $TK_DONT_WAIT);
