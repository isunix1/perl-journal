#!/usr/local/bin/perl -w
# 
# Simple neko animation using non-blocking techniques.  Idea and bitmaps 
# (now color PPM files) based on xneko by Masayuki Koba.
#
# Stephen O. Lidie, Lehigh University Computing Center, lusol@Lehigh.EDU
# 96/06/28.
#
# Copyright (C) 1996 - 1996 Stephen O. Lidie. All rights reserved.
#
# This program is free software; you can redistribute it and/or modify it under
# the same terms as Perl itself.

require 5.002;
use English;
use Tk;
use strict;
use subs qw(animate hide_frame hide_nekos move_frame snooze);
my ($ANIMATE, $BLOCK, $HIDE, $SCAMPER_Y, $SNZ_X, @IDS) = (0, 0, 30, 100, 28);
my ($DELTA_X, $MW) = (16, MainWindow->new);

# Create the canvas and some window items to stuff inside it.  Some widgets
# are realized now, the rest after presentation of the neko frames is complete.

my @bg = qw(-background white);
my @ht = qw(-highlightthickness 0);
my @pk = qw(-side left);
my $C = $MW->Canvas(-height => 150, -width => 500, @ht, @bg)->pack(@pk);
my $f = $MW->Frame(@bg);
$C->create('window', 250, $HIDE, -window => $f);
my $A = $f->Button(-text => 'Presenting neko!', @bg,
		   -command => [$MW => 'bell'])->pack;
my $B = $f->Checkbutton(-text => 'Block?', -variable => \$BLOCK,
			-onvalue => 1, -offvalue => 0, @ht, @bg);
my $D = $MW->Label(-textvariable => \$DELTA_X, @bg);
my $Q = $f->Button(-text => 'Quit', -command => \&exit, @bg);
my $S = $MW->Scale(-orient => 'vertical', -from => 1, -to => 100, @bg, @ht,
		   -variable => \$DELTA_X, -width => 5, -showvalue => 0);

# Create the six Photo images from the color PPM files and display them in a
# row.  The canvas image item ids are stored in the global array @IDS for use
# by the rest of the neko code.  For instance, to perform a canvas operation
# on the neko icon, simply fetch its item id from $IDS[5].  Sorry for using
# hardcoded values, but this is just "proof of concept" code!

my $x = 125;
foreach (qw(left1 left2 sleep1 sleep2 awake icon)) {
    push @IDS, $C->create('image', $x, $SCAMPER_Y,
			  -image => $MW->Photo(-file => "images/$ARG.ppm"));
    $x += 50;
}

# Wait for the main window to appear before hiding the neko frames, otherwise,
# you might never get to see them.

$MW->waitVisibility($MW);
$MW->after(2000, \&hide_nekos);
MainLoop;

sub animate {

    my($id, $done, $delay) = (0, 0, 0);
    my $cb = sub {$done++};

    # Hide the snoozing neko.

    $ANIMATE = 1;
    $A->configure(-state => 'disabled');
    foreach $id (@IDS[2..3]) {hide_frame $id}

    # Awaken neko for 2 seconds, then hide the frame.

    $id = $IDS[4];
    move_frame $id, 460, $SCAMPER_Y;
    $delay = 2000;
    if ($BLOCK) {
	$MW->after($delay);
    } else {
	$MW->after($delay => $cb);
	$MW->waitVariable(\$done);
    }
    hide_frame $id;
    
    # Move neko right to left by exposing successive frames for 0.1 second.

    my($i, $k) = (0, -1);
    $delay = 100;
    for($i = 460; $i >= 40; $i -= $DELTA_X) {
	$id = $IDS[++$k % 2];
	move_frame $id, $i, $SCAMPER_Y;
	if ($BLOCK) {
	    $MW->after($delay);
	} else {
	    $MW->after($delay => $cb);
	    $MW->waitVariable(\$done);
	}
	hide_frame $id;
    }

    # Snooze - neko is tired.

    $ANIMATE = 0;
    $A->configure(-state => 'normal');
    snooze;
}

sub hide_frame {
    
    # Hide a neko frame "under" the neko icon which is top of the display list.

    my $id = shift;
    my ($x, $y) = $C->coords($id);
    $C->move($id, $HIDE-$x, $HIDE-$y);
    $MW->idletasks;
}

sub hide_nekos {

    # First, "hide" the various nekos by moving all of them to ($HIDE,$HIDE).
    # Since the neko icon is the last item in the canvas display list, it ends
    # up being on "top of the pile".  Wait until all the frames are tucked away
    # before reconfiguring the Animation button and packing the Quit button.

    my($i, $done, $rptid, $cb) = ($#IDS, 0, 0, 0);

    $cb = sub {my($ir) = @ARG; hide_frame $IDS[$$ir--]; $done++ if $$ir < 0};
    my $rptid = $MW->repeat(1000 => [$cb, \$i]);

    $MW->waitVariable(\$done);
    $MW->afterCancel($rptid);
    $A->packForget;
    $A->configure(-text => 'Animate', -command => \&animate);
    $B->pack(-side => 'left');
    $A->pack(-side => 'left');
    $Q->pack(-side => 'left');
    $S->pack(-side => 'left', -fill => 'both');
    $C->create('window', 485, $HIDE, -window => $D);

    snooze;
}

sub move_frame {

    # Move a neko frame to an absolute canvas position.

    my($id, $absx, $absy) = @ARG;
    my ($x, $y) = $C->coords($id);
    $C->move($id, $absx-$x, $absy-$y);
    $MW->idletasks;
}

sub snooze {

    my($id, $k, $done) = (0, -1, 0);
    while (1) {
	return if $ANIMATE;
	$id = $IDS[(++$k % 2) + 2];
	move_frame $id, $SNZ_X, $SCAMPER_Y;
	$MW->after(500 => sub {$done++});
	$MW->waitVariable(\$done);
	hide_frame $id;
    }
}
