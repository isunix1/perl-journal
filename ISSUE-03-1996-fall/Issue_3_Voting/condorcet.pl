#!/usr/local/bin/perl
$rcsid='$Id: condorcet.pl,v 0.2 1995/12/31 13:29:33 robla Exp robla $';
# $Log: condorcet.pl,v $
# Revision 0.2  1995/12/31  13:29:33  robla
# Added comment capability to vote data
# Now truly "Condorcet complient" (thanks Mike O.)
# Now handles tie elections correctly (rather than arbitrarily picking a winner)
#
# Revision 0.1.1.1  1995/12/22  04:02:38  robla
# Added more comments
#
# Revision 0.1  1995/12/22  02:46:22  robla
# First version.  The tie-breaker still incorrectly calculates worst
# defeat by votes for, rather than votes against.
#
#    condorcet.pl - tallies ranked preference ballots using Condorcet's method
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
#    For more information about this program, check out my home page
#    at <URL:http://www.eskimo.com/~robla>, or email me (Rob Lanphier
#    <robla@eskimo.com>) for what the web page doesn't answer.
#
#    See $helpstring below for more details.

$helpstring=qq/

 condorcet.pl - tallies ranked preference ballots using Condorcet's method.
 $rcsid

 Copyright (C) 1995  Rob Lanphier

 This program is free software; you can redistribute it and\/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 See source code for more details.

 usage:  perl condorcet.pl [-t] candidate-list ballot-list

 -t    Print the raw tally table. (pair-wise election results)

 candidate-list - contains a two column, comma-separated list of
                  candidate number and candidate names.  Candidate    
        	  numbers can be any non-negative integers of unlimited
		  size.  For efficiency, numbering should start at zero
	          and proceed up in increments of one, but this is not
	          a requirement.

 ballot-list -    this should be a list of ballots, one per line.  Each
  	          line should contain a list of candidate numbers
		  separated by commas.

 Ballots are not checked for validity, and results may be
 unpredictable with bad input.

/;

if ($ARGV[0] =~ /^-[hH\?]$/)
{
    print $helpstring;
    exit;
}
if ($ARGV[0] =~ /^-[tT]$/)
{
    $printtally = 1;
    shift(@ARGV);
}

open (CANDLIST, $clistfilename=shift(@ARGV)) || die "can't open $clistfilename" ;

# Load the candidate list
while (<CANDLIST>)
{   
    if (/(\d+)\s*,\s*(.*)$/)	# If the line starts with a number and
			    	# a comma (they all should)...
    {				# Stuff the candidate's name in an
				# array and set a flag in $candvec
				# corresponding to this candidate number.
	$candidate[$1]=$2;
	vec($candvec, $1, 1)=1;
    }
}

close(CANDLIST);

open (VOTES, $vlistfilename=shift(@ARGV)) || die "can't open $vlistfilename" ;

 #  Variable "declaration"
 #  $tally[candx][candy]
 #
 #  $tally is a two-dimensional array (ala Perl 5) that stores the
 #  number of votes that candx received over candy.


while (<VOTES>)
{				# Boolean vector with a flag set for
				# all "losers" reset with every new
				# ballot.  All are losers until they
				# are listed on a ballot.
    local($loservec)=$candvec;

    s/#.*$//;			# Strip comments

    if (/^\D*$/)		# Ignore blank lines.
    {
	next;
    }

    local(@votelist)=split(/,/, $_);

    foreach $vote (@votelist)	# For each preference listed...
    {
	vec($loservec, $vote, 1) = 0; # Remove the chosen candidate
				      # from the loser vector.
	for ($i = 0; $i<= $#candidate; $i++) # For all candidates...
	{
	    if(vec($loservec,$i,1)) # If they haven't been listed yet...
	    {
		if(defined($tally[$vote][$i])) # They've been beat by
					       # the chosen candidate.
		{
		    $tally[$vote][$i]++;
		}   
		else
		{
		    $tally[$vote][$i]=1;
		}
	    }
	}
    }
    $total_vote++;
}
close(VOTES);

# Variable declaration for @results
# A two-dimensional array storing election results for each candidate
#
# Initialize the @results array.
for ($i=0;$i<=$#candidate;$i++)
{
    if (vec($candvec,$i,1))
    {	     
	$results[$i][0]=0;	# Number of victories for $i
	$results[$i][1]=0;	# Number of ties for $i
	$results[$i][2]=0;	# Number of defeats for $i
	$results[$i][3]=0;	# Worst defeat, as measured by
				# total votes against $i
    }
}
# Now for the pairwise tally...
for ($i=0;$i<$#candidate;$i++)
{
    if (vec($candvec,$i,1))
    {
	for ($j=$i+1;$j<=$#candidate;$j++)
	{
	    if (vec($candvec,$j,1))
	    {
		if(!defined($tally[$i][$j])) # Initialize the tally
					     # array for uninitialized value.
		{
		    $tally[$i][$j] =0;
		}
		if(!defined($tally[$j][$i]))
		{
		    $tally[$j][$i] =0;
		}
		local($itally)=$tally[$i][$j]; # Votes for candidate
					       # number $i
		local($jtally)=$tally[$j][$i]; # Votes for candidate
					       # number $j
		local($x)=($itally <=> $jtally)+1;
		local($y)=2-$x;
		$results[$i][$x]++;
		$results[$j][$y]++;
		if($jtally > $results[$i][3])
		{
		    $results[$i][3]=$jtally; # This is $i's worst defeat.
		}			
		if($itally > $results[$j][3])
		{
		    $results[$j][3]=$itally; # This is $j's worst defeat.
		}
	    }
	}
    }
}


if (!$printtally) 
{
    print "Total votes tallied: $total_vote\n";
}

# This is the loop where the winner is calculated.  The winner is
# stored in an array to deal with the possibility of a tie, in which
# the array grows to accomodate multiple "winners".  God help us if
# there is a tie.

{
    local($min_worst_defeat)=$total_vote;
    for ($i=0;$i<=$#candidate;$i++)
    {
	if (vec($candvec,$i,1))
	{				
	    if($results[$i][3]<$min_worst_defeat)
	    {
		$min_worst_defeat=$results[$i][3];
		@leading_cand_num=($i); # $i is now the hands-down winner, so
		# far
	    }
	    elsif($results[$i][3]==$min_worst_defeat)
	    {
		push(@leading_cand_num, $i); # $i is tied for the lead
		                             # with those already in the
		                             # @leading_cand_num array
	    }
	    if($results[$i][0]==0 &&
	       $results[$i][1]==0)	# If they haven't lost or tied
				  	# any elections, they win.
	    {
		@leading_cand_num=($i);
		print "Smoked 'em\n";
		last;		# That's all she wrote.
	    }
	}
    }
}
if ($printtally) 
{
    # Print out the raw tally table.
    for ($i=0;$i<=$#candidate;$i++)
    {
	if (vec($candvec,$i,1))
	{	     
	    print"\t$candidate[$i]";
	}
    }
    print "\n";
    
    for ($i=0;$i<=$#candidate;$i++)
    {
	if (vec($candvec,$i,1))
	{	     
	    print"$candidate[$i]";
	    for ($j=0;$j<=$#candidate;$j++)
	    {
		if(vec($candvec,$j,1))
		{
		    print"\t$tally[$j][$i]";
		}
	    }	
	    print "\n";
	}
    }
    # End of raw tally table printout.
}
else
{
    # Now for the moment we've been waiting for.  This is where we
    # announce the winner(s)
    if($#leading_cand_num==0)	# i.e. if there is only one
			     	# leading_cand_num, they win
    {
	print"The winner is $candidate[$leading_cand_num[0]]\n\n";
    }
    else			# Oh, hell...
    {
	print"There has been a tie\nThe winners are:\n";
	for ($i=0;$i<=$#leading_cand_num;$i++)
	{
	    print"The winner is $candidate[$leading_cand_num[$i]]\n";
	}
	print"\n";
    }
    print "Pairwise elections won-lost-tied:\n";
    
    for ($i=0;$i<=$#candidate;$i++)
    {
	if (vec($candvec,$i,1))
	{	     
	    if(!defined($results[$i][0]))
	    {
		$results[$i][0]=0;
	    }
	    if(!defined($results[$i][1]))
	    {
		$results[$i][1]=0;
	    }
	    if(!defined($results[$i][2]))
	    {
		$results[$i][2]=0;
	    }
	    print "$candidate[$i] $results[$i][2]-$results[$i][0]-$results[$i][1]";
	    print " (votes against in worst defeat (or closest victory): ";
	    print "$results[$i][3])\n";
	}
    }
}






