#!/usr/local/bin/perl
#Time-stamp: "1999-10-07 00:48:19 MDT sburke@stonehenge.netadventure.net"

use strict;
my $Debug = 0;
my($word_re, $meter_re, $pron_re) = @ARGV[0,1,2];

if($meter_re =~ m<^[/_]+$>) {
  $meter_re =~ s</><[12]>g;
  $meter_re =~ s<_><[20]>g;
  $meter_re = '^' . $meter_re . '$';
}

print "# Word RE: <$word_re>  Meter RE: <$meter_re>  Pron RE: <$pron_re>\n";

die "You need at least one stipulation for word, meter, or pronunciation!"
  unless length $word_re or length $meter_re or length $pron_re;

my $search_file = 'mpron.dat';
open(IN, $search_file)
 or die "Can't open $search_file: $!";

my @bits;
my $matches = 0;
my $lines = 0;
while(<IN>) {
  chomp;
  @bits = split "\t", $_;
  next unless @bits == 3;
  ++$lines;
  ++$matches, print $_, "\n"
   if
     (!length($word_re)  || $bits[0] =~ m/$word_re/oi)
     and
     (!length($meter_re) || $bits[1] =~ m/$meter_re/o)
     and
     (!length($pron_re)  || $bits[2] =~ m/$pron_re/o )
  ;
  if($Debug) {
   last if $. > 2000;
  }
}
print "# $matches matches across $lines lines in ", time - $^T, " seconds.\n";
exit;
__END__
