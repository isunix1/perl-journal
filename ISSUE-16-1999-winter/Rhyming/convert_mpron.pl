#!/usr/local/bin/perl
#Time-stamp: "1999-10-07 00:49:01 MDT sburke@stonehenge.netadventure.net"

use strict;
@ARGV = 'mobypron.unc' unless @ARGV;
my($word, $pron, $meter, $next_stress_flag);
my $Debug = 0;
# $/ = "\cm"; # may be necessary

open(OUT, ">mpron.dat");

while(<>) {
  chomp;
  ($word, $pron) = split(' ', $_, 2);
  next unless $pron;

  $meter = '';
  $next_stress_flag = '0';
  foreach my $x ($pron =~ m<[',]|[-\&yYaeiouAEIOU\@]+>g) {
    if($x eq ',') {
      $next_stress_flag = '2'; # secondary stress
      next;
    } elsif($x eq "'") {
      $next_stress_flag = '1'; # primary stress
      next;
    }
    $meter .= $next_stress_flag;
    $next_stress_flag = '0';
  }
  $meter =~ tr/0/2/ if $meter =~ m/^0+$/s;
   # So "stressless" one-syllable words all get stress.
   # Also needed for multiword phrases mode of monosyllabic
   # words, like "base_load".

  $pron =~ tr<', /_><>d;
   # remove stress marks, word separators, and the mystery slashes
  sleep(0), printf "%10s %-20s %s\n", $meter, $word, $pron if $Debug;
  print OUT join("\t", $word, $meter, $pron), "\n";
  last if $Debug and $. > 1000;
}
close(OUT);
exit;
__END__
