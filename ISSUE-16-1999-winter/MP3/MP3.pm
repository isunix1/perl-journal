package Apache::MP3;
# file: Apache/MP3.pm
use strict;
use Apache::Constants qw(:common);
use MPEG::MP3Info;
use IO::Dir;
use Apache::File;
# Intercept requests for audio/mpegurl (.pls) files and 
# convert them into an appropriately-formatted playlist.
# Intercept requests for audio/x-shoutcast-stream (.mps)
# and convert them into appropriate shoutcast/icecast 
# output to install:
#
# AddType audio/mpeg .mp3
# AddType audio/mpegurl .pls
# AddType audio/x-shoutcast-stream .mps
#
# <Files ~ "\.(pls|mps)$">
#   SetHandler perl-script
#   PerlHandler Apache::MP3
# </Files>
# entry point for mod_perl
sub handler {
  my $r = shift;
  my $filename = $r->filename;
  # Reconstruct the requested URL.
  my $server_url = join '', 'http://',
                            $r->server->server_hostname,
                            ":",
                            $r->get_server_port;
  my $filename = $r->filename;
  my ($basename) = $filename  =~ m!([^/]+)\.[^/.]*$!;
  (my $directory   = $filename) =~ s!/[^/]+$!!;      
  (my $virtual_dir = $r->uri)   =~ s!/[^/]+$!!;      
  if ($r->content_type eq 'audio/mpegurl') {
    # If this is a request for a file of type 
    # audio/mpegurl, strip off the extension and look 
    # for a directory containing the name.  Generate a 
    # playlist from all mp3 files in the directory.
    return dir2playlist($r, "$directory/$basename", undef,
      "$server_url/$basename/") if -d "$directory/$basename";
    # If none found, search for a file of type audio/mpeg 
    # sharing the basename, and generate a playlist from that.
    return dir2playlist($r, $directory, $basename,
                                 "$server_url$virtual_dir/");
  } 
  # Otherwise is this a request for stream data?
  elsif ($r->content_type eq 'audio/x-shoutcast-stream') {
    my ($mp3_file) = search4mp3($r,$directory,$basename);
    return DECLINED unless $mp3_file;
    return send_stream($r,"$directory/$mp3_file",$server_url);
  }
}

 # search for an mp3 file that matches a basename
sub search4mp3 {
  my ($r,$dir,$basename) = @_;
  my $pattern = quotemeta $basename;
  my @mp3;
  my $dh = IO::Dir->new($dir) || return;
  while ( defined($_ = $dh->read) ) {
    next if $pattern && !/^$pattern(\.\w+)?$/;
    next if $r->lookup_file("$dir/$_")->content_type 
                                                 ne 'audio/mpeg';
    push (@mp3, $_);
  }
  return @mp3;
}

# send the playlist...
sub dir2playlist {
    my ($r, $dir, $basename, $url) = @_;
    my @mp3 = search4mp3($r, $dir, $basename);
    return DECLINED unless @mp3;
    $r->content_type('audio/mpegurl');
    $r->send_http_header;
    return OK if $r->header_only;
    $r->print ("[playlist]\r\n\r\n");
    $r->print ("NumberOfEntries=",scalar(@mp3),"\r\n");
    for (my $i=1;$i<=@mp3;$i++) {
      (my $file = $mp3[$i-1]) =~ s/(\.[^.]+)?$/.mps/;
      $r->print ("File$i=$url$file\r\n");
    }
    return OK;
}

# send the music stream...
sub send_stream {
    my ($r, $file, $url) = @_;
    my $tag  = get_mp3tag($file);
    my $info = get_mp3info($file);
    return DECLINED unless $info;  # not a legit mp3 file?
    my $fh = Apache::File->new($file) || return DECLINED;
    my $title = $tag->{TITLE} || $url . $r->uri;
    foreach ( qw(ARTIST ALBUM YEAR COMMENT) ) {
        $title .= ' - ' . $tag->{$_} if $tag->{$_};
    }
    my $genre = $tag->{GENRE} || 'unknown';
    $r->print("ICY 200 OK\r\n");
    $r->print("icy-notice1:<BR>This stream requires a 
             shoutcast/icecast compatible player.<BR>\r\n");
    $r->print("icy-notice2:Apache::MP3 module<BR>\r\n");
    $r->print("icy-name:$title\r\n");
    $r->print("icy-genre:$genre\r\n");
    $r->print("icy-url:$url\r\n");
    $r->print("icy-pub:1\r\n");
    $r->print("icy-br:$info->{BITRATE}\r\n");
    $r->print("\r\n");
    return OK if $r->header_only;
    $r->send_fd($fh);
    return OK;
}
1;
__END__

