/* allocate the interpreter object */
PerlInterpreter *my_perl = perl_alloc(); 

/*
 * initialize the interpreter
 * set some global variables
 * initialize the stack 
 */ 

perl_construct(my_perl);

/*
 * intialize global variables %INC, $/, $0, $$, etc.
 * parse command line arguments
 * create perl entry point to C libraries, e.g. DynaLoader
 * "compile" the code into perl's interal format, or "parse tree"
 *
 */

perl_parse(my_perl, xs_init, argc, argv, (char **)NULL);

/*
 * interpret the script (walk the parse tree)
 */

perl_run(my_perl);

/*
 * "destroy" objects and free memory allocated for
 * the script's symbols, etc.
 */

perl_destruct(my_perl);

/*
 * free memory allocated for the PerlInterpreter object itself
 */

perl_free(my_perl);

