#!/usr/bin/perl -w
############################################################
#
# File: sample1.pl
#
# Description: 
# Draws a basic multi-set line chart with GDGraph3d
#
# Created: 31.May.2000 by Jeremy Wadsack for Wadsack-Allen Digital Group
# Copyright (C) 2000 Wadsack-Allen. All rights reserved.
#
# This script is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.
############################################################
# Date        Modification                            Author
# ----------------------------------------------------------
#
############################################################
use strict;

#**** Line numbering in the article starts below here ****
use GD::Graph::lines3d;

# Create an array of data
my @data = ( 
	["Jan","Feb","Mar","Apr","May","Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
	[  860,  813, 1694,  957, 1805, 1835, 2335, 1272, 1562, 2296, 2123, 1882,],
	[ 1249,  483, 1731, 1617, 1166, 1761, 1111, 1635, 2158, 2007, 2250, 2858,],
	[  747, 1216,  525, 1028, 1053, 1860, 2282, 1871, 2143, 1845, 2924, 2858,],
	[ 1220,  864, 1325,  967, 1200, 1371, 1759, 1512, 1484, 1683, 1965, 2458,],
	[ 1276,  639,  931, 1288, 2049,  909, 1617, 1432, 1615, 2605, 2712, 2429,],
);

# Make a new graph object that is 600 pixels wide by 400 pixels high
my $graph = new GD::Graph::lines3d( 600, 400 );

# Set some labels
$graph->set( 
	x_label           => 'Month, 1999',
	y_label           => 'Revenue ($US)',
	title             => 'Monthly revenue for 1999',
	dclrs             => ['#9999CC', '#CCCC66', '#339900', '#990000', '#FFCC33'],
	boxclr            => '#FFFFCC',
	long_ticks        => 1,
);

# Plot the graph to a GD object
my $gd = $graph->plot( \@data );

# Figure out what the default output format is
my $format = $graph->export_format;

# Now open a file locally and write it
open(IMG, ">sample.$format") or die $!;
	binmode IMG;
	print IMG $gd->$format();
close IMG;