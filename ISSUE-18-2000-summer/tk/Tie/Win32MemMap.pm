# This TIEHASH package allows a user to use hash structures for sharing memory between
# applications on Win32.
#
# See POD at end of document.
#
# Author:   Grant Hopwood.
# Date:     10/21/1999.
# Modified: 03/27/2000 - Added persistent caching of the hash structure for more optimized 
#                        for/each() iterations. Tidied up the code and error handling.

package Tie::Win32MemMap;
require 5.005;

use strict;
use Carp;
use Win32::MemMap;  # http://wwws01.valero.com/perl/packages
use Win32::Mutex;
use Storable qw(freeze thaw);
use vars qw(@ISA @EXPORT @EXPORT_OK $VERSION);
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(
	MEM_NEW_SHARE
	MEM_VIEW_SHARE
);
@EXPORT_OK = qw();
$VERSION = '1.02';

sub _fatal ($$);
#------------------------------------------------------------------------------
# CONSTANTS.
sub MEM_NEW_SHARE  () {1}
sub MEM_VIEW_SHARE () {0}

#------------------------------------------------------------------------------
# TIEHASH class, list
# Constructor for the Tied object.
# tie %h, 'Tie::Win32MemMap', { Create => 1|0 [,MapName => '...' [,Size => ##]] }
sub TIEHASH {
	my ($class, $rhash) = @_;
	my $attr = {};
	
	unless (ref($rhash) eq 'HASH' and exists $rhash->{Create}) {
		croak "Usage: tie %h, 'Tie::Win32MemMap', { Create => 1|0 [,MapName => \$name [,Size => \$size]] }\n";
	}
	
	my $mutexname   = exists $rhash->{MapName} ? $rhash->{MapName} : 'Default';
	$attr->{mutex}  = new Win32::Mutex(0, "//Mutex/$mutexname");
	$attr->{objtype}= $rhash->{Create};
	
	my $obj = new Win32::MemMap;
   if ($attr->{objtype}) {
      $attr->{mem} = $obj->OpenMem("//MemMap/$mutexname", 
      	exists $rhash->{Size} ? $rhash->{Size} : 100000)
         	or croak "Failed to create a shared Memory Space: //MemMap/$mutexname\n";
   }
   else {
      $obj->GetMapInfo("//MemMap/$mutexname", \my $mapinfo)
         or croak "No such Shared Memory space: //MemMap/$mutexname\n";
      $attr->{mem} = $obj->MapView("//MemMap/$mutexname", $mapinfo->{Size}, 0)
         or croak "Failed to create a MapView to an existing shared Memory Space: //MemMap/$mutexname\n";
   }

	return bless $attr, $class;
}  

#------------------------------------------------------------------------------
# STORE this, key, value
# Inserts a key/value pair into the tied hash.
sub STORE {
	my ($self, $key, $val) = @_;
   my $rhash;
      
   $self->{mutex}->wait(2000) or _fatal $self, "STORE: Timeout on Mutex object.\n$key/$val\n";

   if ($self->{mem}->GetDataSize > 0) {
      $self->{mem}->Read(\my $str, 0, $self->{mem}->GetDataSize);
	   $rhash = thaw($str);
   }

	$rhash->{$key} = $val;
   $self->{mem}->Write(\freeze($rhash), 0);
	$self->{mutex}->release;

	return $rhash->{$key};
}

#------------------------------------------------------------------------------
# FETCH this, key
# Retrieves a key/value pair from the tied hash.
sub FETCH {
	my ($self, $key) = @_;
   my $rhash;
   
   $self->{mutex}->wait(2000) or _fatal $self, "FETCH: Timeout on Mutex object.\n$key\n";

	if ($self->{mem}->GetDataSize > 0) {
      $self->{mem}->Read(\my $str, 0, $self->{mem}->GetDataSize);
	   $rhash = thaw($str);
   }

	$self->{mutex}->release;
	return $rhash->{$key};
}

#------------------------------------------------------------------------------
# FIRSTKEY this
# Returns the first key/value pair in the tied hash. 
{
	my $tdata; # Prevents conditions where the hash might be changed in between iterations
	           # by another process mapped to the memory share. Also reduces the number of 
	           # thaws required for each iteration.
	sub FIRSTKEY {
		my $self = shift;
	
	   $self->{mutex}->wait(2000) or _fatal $self, "FIRSTKEY: Timeout on Mutex object.\n";
	
		if ($self->{mem}->GetDataSize > 0) {
	      $self->{mem}->Read(\my $str, 0, $self->{mem}->GetDataSize);
		   $tdata = thaw($str);
	   }
	   
	   $self->{mutex}->release;
	   my $a = keys %$tdata;
	   each %$tdata;
	}

#------------------------------------------------------------------------------
# NEXTKEY this, lastkey 
# Returns the next key/value pair for the tied hash.
# Iterated subsequently after FIRSTKEY.
	sub NEXTKEY {
		each %$tdata;
	}
}

#------------------------------------------------------------------------------
# CLEAR this 
# Clears all values from the tied hash. 
sub CLEAR {
	my $self = shift;
   my $rhash;

   $self->{mutex}->wait(2000) or _fatal $self, "CLEAR: Timeout on Mutex object.\n";

	if ($self->{mem}->GetDataSize > 0) {
      $self->{mem}->Read(\my $str, 0, $self->{mem}->GetDataSize);
	   $rhash = thaw($str);
   }
	
	my $ret = %$rhash = ();
   $self->{mem}->Write(\freeze($rhash), 0);

	$self->{mutex}->release;
	return $ret;
}

#------------------------------------------------------------------------------
# DELETE this, key 
# Deletes a key from the tied hash.
sub DELETE {
	my ($self, $key) = @_;
   my $rhash;

   $self->{mutex}->wait(2000) or _fatal $self, "DELETE: Timeout on Mutex object.\n$key\n";

	if ($self->{mem}->GetDataSize > 0) {
      $self->{mem}->Read(\my $str, 0, $self->{mem}->GetDataSize);
	   $rhash = thaw($str);
   }

	my $ret = delete $rhash->{$key};
   $self->{mem}->Write(\freeze($rhash), 0);
	
	$self->{mutex}->release;
	return $ret;	
}

#------------------------------------------------------------------------------
# EXISTS this, key 
# Verify that key exists with the tied hash. 
sub EXISTS {
	my ($self, $key) = @_;
   my $rhash;

   $self->{mutex}->wait(2000) or _fatal $self, "EXISTS: Timeout on Mutex object.\n$key\n";

	if ($self->{mem}->GetDataSize > 0) {
      $self->{mem}->Read(\my $str, 0, $self->{mem}->GetDataSize);
	   $rhash = thaw($str);
   }
	
	$self->{mutex}->release;
	return exists $rhash->{$key};
}

#------------------------------------------------------------------------------
# DESTROY this
# Destructor for the Tied object.
sub DESTROY {
	($_[0]->{objtype} ? $_[0]->{mem}->Close :	$_[0]->{mem}->UnmapView);
}

# This function should never be called, but is implemented for any rare 
# cases that may occur.
sub _fatal ($$) {
	$_[0]->DESTROY;
	croak $_[1];	
}

1;
__END__

=head1 NAME

Tie::Win32MemMap - TieHash functionality for Win32-MemMap

=head1 REQUIREMENTS

	Win32-MemMap http://wwws01.valero.com/perl/packages
	Storable

=head1 SYNOPSIS

	use Tie::Win32MemMap;
	tie(%h, 'Tie::Win32MemMap', { Create => MEM_NEW_SHARE, MapName => 'Test', Size => 100000 });

=head1 DESCRIPTION

This TIEHASH package will tie a hash structure to a shared memory space on Windows NT. 
This allows Perl applications to communicate any kind of data between themselves using the 
fastest method possible.

=head1 CONSTANTS

	MEM_NEW_SHARE
	MEM_VIEW_SHARE

=head1 CONSTRUCTOR

tie %h, 'Tie::Win32MemMap', { Create => 1|0 [,MapName => $name [,Size => $size]] }

Arguements are passed as an anonymous hash. The only required arguement is B<Create>.

B<Create>: If set to true, a new memory share is created in memory, otherwise an attempt to
map a 'View' to an existing share (created by another process) is made. I<See CONSTANTS>.

B<MapName>: The name used to identify the memory share. If not provided, the name 'Default'
is used.

B<Size>: The size in bytes to be allocated for the memory share. If not provided, a default
size of 100,000 bytes will be allocated. I<This arguement is ignored for 'Views'>.

=head1 EXAMPLES

	# Parent.pl
	use Tie::Win32MemMap;
	tie(%h, 'Tie::Win32MemMap', { Create => MEM_NEW_SHARE, MapName => 'Test', Size => 10000 });
	$h{key}  = [qw(apple bananna orange)];
	$h{key2} = "this is my string\n";
	while (1) { sleep 1 }
	__END__
	
	# Child.pl
	use Tie::Win32MemMap;
	tie(%h, 'Tie::Win32MemMap', { Create => MEM_VIEW_SHARE, MapName => 'Test' });
	print $h{key}->[1], "\n";
	print $h{key2}, "\n";
	__END__

=head1 NOTES

I haven't checked to see what happens to processes that have mapped views to a memory share 
when the process that created the share terminates prematurely.

=head1 VERSION

	Module can be found at http://wwws01.valero.com/perl/packages
	03/27/2000 - Version 1.02

=head1 AUTHOR

	Grant Hopwood
	hopwoodg@valero.com

=head1 COPYRIGHT

	Hack n' slash babee.
	
	Win32::MemMap is copyright Amine Moulay Ramdane (aminer@generation.net).