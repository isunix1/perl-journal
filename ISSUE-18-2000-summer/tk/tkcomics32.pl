#!/usr/local/bin/perl -w
#
# tkcomics - display comics courtesy of http://www.comics.com
#
# Because LWP::Simple and LWP::UserAgent can block, do the network
# I/O in another thread, or, child, and using fileevent() or
# waitVariable() to keep events flowing.
#
# Add a Stop/Cancel button that kills the pipe.
#
# Command line options:
#
#   pipe_open_fileevent
#   tcp_socket_fileevent
#   win32_memmap_waitvariable


use MIME::Base64;
use Tk;
#use Tk::JPEG;
#use Tk::PNG;
use subs qw/get_url show_comic status stop_get unix_pipe win32_memmap/;
use strict;

my $mw = MainWindow->new;
my $photo = '';
my $status = '';
my($eof, $pid);
my %ext = qw/
    gif gif
    jpg jpeg
    png png
    tif tiff
    xbm xbm
    ppm ppm
/;				# file extension => Photo format map
my $help = '<Button-1> fetch comic, <Button-2> interrupt transfer';

my $s = $mw->Label(-textvariable => \$status, -width => 100);
my $lb = $mw->Scrolled('Listbox');
my $l = $mw->Label;
$s->pack(qw/-side bottom -fill x -expand 1/);
$lb->pack(qw/side left -fill y -expand 1 -anchor w/);
$l->pack(-side => 'right');

# Fetch the main comics page, build a hash of comic URLs
# indexed by comic name,  note the total comic count, and
# populate a Listbox.  Listbox B1 fetches and displays a
# comic, B2 anywhere cancels a transfer.

my $comics_home = 'http://www.comics.com';
my $comics = get_url $comics_home or die "Can't get $comics_home.";

my(%comics, $comic_url, $comic);
foreach (split /\n/, $comics) {
    next unless /OPTION\s+VALUE/i;
    if (($comic_url, $comic) = m\"([^"]+)">(.*)\) {
        $comic =~ s/\t//g;
        $comic =~ s/\r//g;
        $comics{$comic} = $comic_url;
    }
}
status 'Found ' . scalar(keys %comics) . ' comics, '. $help;

foreach (sort keys %comics) {
    $lb->insert('end', $_);
}

$lb->bind('<ButtonRelease-1>' => \&show_comic);
$mw->bind('<ButtonRelease-2>' => \&stop_get);

MainLoop;

sub get_url {

    my($url) = @_;
    
    status "Fetching $url";

    # The parent creates and initializes a new chunck of shared
    # memory, then starts a child process that shares the same
    # memory.  The parent waits for the child to run lwp-request
    # and save the web content by (essentailly) doing a
    # waitvariable() on one particular hash element.

    use Win32::Process;
    use Tie::Win32MemMap;

    my %content;
    tie %content, 'Tie::Win32MemMap', {
	Create  => MEM_NEW_SHARE,
	MapName => 'tkcomics',
    };
    $content{'COMPLETE'} = 0;
    $content{'CONTENT'}  = '';

    Win32::Process::Create(
        my $child,
        'c:\\perl\\bin\\perl.exe',
        "perl tkcwin32.kid $url",
        0,
        NORMAL_PRIORITY_CLASS,
       '.',
    ) or die Win32::FormatMessage(Win32::GetLastError);

    $eof = 0;
    $mw->update;

    while ( $content{'COMPLETE'} != 1 ) {
	last if $eof == -1;
	$mw->update;
    }

    my $content = $content{'CONTENT'};
    (my $response, $content) = $content =~ /(.*?)\n\n(.*)/is if $content;
    return wantarray ? ($response, $content) : $content;
                   
} # end get_url

sub show_comic {

    # Using the active listbox element, find the comic URL and fetch
    # its contents.  Within the content is another URL pointing to
    # the actual image, which we then fetch, convert to a Photo and
    # then display.  $eof is -1 if any transfer was aborted.

    my($lb) = @_;

    my $comic = $lb->get('active');
    $comic =~ s/\s\(\d+\)//;
    my $comic_url = $comics{$comic};
    my $comic_html = get_url 
	$comic_url =~ /^http:/i ? $comic_url : "$comics_home$comic_url";
    return if $eof == -1;

    my($image_url) =
	$comic_html =~ m\.*<IMG SRC="([^"]+)".*? ALT="(today|daily)\is;
    my ($response, $image) = get_url "$comics_home$image_url";
    return if $eof == -1;
    my($bytes) = $response =~ /Content-Length: (\d+)/is;

    my $photo_data = encode_base64($image);
    $photo->delete if UNIVERSAL::isa($photo => 'Tk::Photo');
    my($ext) = $image_url =~ /\.(.*)?/;
    $ext ||= 'gif';
    status "Creating $bytes byte $ext Photo";
    $photo = $mw->Photo(-data => $photo_data, -format => $ext{lc $ext});
    $l->configure(-image => $photo);

    my $index = $lb->index('active');
    $comic .= " ($bytes)";
    $lb->delete($index);
    $lb->insert($index, $comic);
    $lb->see($index);
    $lb->update;

    status $help;

} # end show_comic

sub status {
    $status = $_[0];
    $mw->idletasks;
}

sub stop_get {
    status "Stopping transfer ...";
    $mw->after(5000, sub {status $help});
    $eof = -1;
    kill 'TERM', $pid if defined $pid;;
}
