# WaitBox - a "Please Wait" Dialog with a progress bar.

use Tk::WaitBox;
use vars qw/$top/;

sub WaitBox {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'WaitBox - a "Please Wait" Dialog with a progress bar.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $utxt = 'Initializing...';
    my $wb;
    $wb = $top->WaitBox(
        -txt1          => 'Saving PICT file',
        -canceltext    => 'Stop',
	-cancelroutine =>
	    sub {
	        print "You selected Stop.\n";
	        $wb->unShow;
		$utxt = undef; # tell progress loop to stop
	        });

    # Create a percent-complete label and progress bar in the "user frame".
    # Since this is a WaitBox, the progress loop can be really simple-minded.

    my $u = $wb->Subwidget('uframe');
    $u->pack(-expand => 1, -fill => 'both');
    $u->Label(-textvariable => \$utxt)->pack(-expand => 1, -fill => 'both');

    my $base = $u->Frame(qw/ -relief sunken -borderwidth 2 -height 20/);
    $base->pack(qw/-side left -anchor w -expand 1 -fill both/);
    $base->packPropagate(0);
    my $bar = $base->Frame(qw/-borderwidth 2 -relief raised -height 20
        -width 0 -background blue/);
    $bar->pack(qw/-fill y -side left/);

    $top->Button(qw/-text WaitBox -command/ =>
        sub {
	    $wb->Show;
	    for my $i (1 .. 100) {
		after 100;
		$bar->configure(-width => int($i / 100 * $base->Width));
		$utxt = int(100 * $i / 100) . "% Complete";
		$wb->update;
		last unless defined $utxt;
            };
	    $wb->unShow;
	})->grid;
}
