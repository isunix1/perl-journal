# DialogBox - like Dialog, but the top frame can contain anything.

use Tk::DialogBox;
use vars qw/$top/;

sub DialogBox {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'DialogBox - like Dialog, but the top frame can contain anything.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $d = $top->DialogBox(qw/-title DialogBox -buttons/ => [qw/OK Cancel/]);
    my $c = $d->add(qw/Canvas -width 160 -height 40/)->grid;
    $c->createPolygon(qw/10 10 20 5 10 30 15 30 20 20 10 15 10 10 -fill red/);
    $c->createText(qw/100 20 -text/ => 'A strange polygon');
    $top->Button(qw/-text DialogBox -command/ =>
        sub{
            print "You selected button ", $d->Show, ".\n";
	})->grid;
}
