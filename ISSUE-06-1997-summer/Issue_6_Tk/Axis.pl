# Axis - a canvas with X and Y axes (a derived widget).

use Tk::Axis;
use vars qw/$top/;

sub Axis {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'Axis - a canvas with X and Y axes (a derived widget).',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my$t = $top->Axis(qw/-xmax 10 -ymax 10/)->grid;                        
    $t->create('line', $t->plx(1.5), $t->ply(1.5), $t->plx(6), $t->ply(9));
}
