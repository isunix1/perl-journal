# DialogBox2 - this demonstration displays a Photo image.

use Tk::DialogBox;
use vars qw/$top/;

sub DialogBox2 {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'DialogBox2 - this demonstration displays a Photo image.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $d = $top->DialogBox(qw/-title DialogBox2 -buttons/ => [qw/OK Cancel/]);
    $d->add('Frame')->grid;
    my $frame = $d->Subwidget('frame');
    $frame->Message(-text => 'This DialogBox has a populated frame.')->grid;
    $frame->Label(-image => $d->Photo(-file =>
        Tk->findINC('demos/images/teapot.ppm')))->grid;
    $top->Button(qw/-text DialogBox -command/ =>
        sub{
            print "You selected button ", $d->Show, ".\n";
	})->grid;
}
