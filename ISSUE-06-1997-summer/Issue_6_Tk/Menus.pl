# Menus - do neat menu stuff we haven't seen before.

use vars qw/$top/;

sub puts {print "$_[0]\n"}

sub Menus {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'Menus - do neat menu stuff we haven\'t seen before.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $menubar = $top->Frame(qw/-bd 5 -relief ridge -bg orange/)->grid;

    my $aircraft = 'B-52';
    my $file = $menubar->Menubutton(qw/-text File -relief raised -menuitems/ =>
	    	       [
			[Button    => '~Open', -command => [\&puts, 'Open']],
			[Separator => ''],
			[qw/Cascade ~Aircraft -menuitems/ =>
			 [
			  [qw/Radiobutton B-52  -variable/ => \$aircraft],
			  [qw/Radiobutton B-2   -variable/ => \$aircraft],
			  [qw/Radiobutton F-117 -variable/ => \$aircraft],
			  [qw/Radiobutton F-22  -variable/ => \$aircraft],
		         ]],
			[Separator => ''],
			[Button => '~Popup Menu'],
			[Separator => ''],
			[Button    => '~Quit', -command =>[\&puts, 'Quit'] ],
		       ]
		      );
    $file->grid(qw/-row 0 -column 0/);

    my $p = $menubar->Menu(qw/-tearoff 0 -menuitems/ =>
	 	  [
		    [qw/Button Hi -command/ => [\&puts, 'Hi']],
		    [qw/Checkbutton frog -onvalue 1 -offvalue 0/], 
		   ],
		 ); 
    $file->entryconfigure('Popup Menu', -command => [$p => 'Popup']);

    my $o = $menubar->Optionmenu(-textvariable => \$aircraft,
				 -options      => [qw/B-52 B-2 F-117 F-22/],
				 -command      => sub {print "$aircraft!\n"},
				 -relief       => 'raised');
    $o->grid(qw/-row 0 -column 1/);
}
