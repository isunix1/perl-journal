# Font - find X fonts.

use Tk::Font;
use vars qw/$top/;

sub Font {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name             => $demo,
        -text             => 'Font - find X fonts.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $font = $top->Font(qw/foundry adobe family times point 120/);
    $top->Label(-text => $font, -font => $font)->grid;
    $font = $top->Font('*-courier-medium-r-normal-*-*');
    $top->Label(-text => $font, -font => $font)->grid;
}
