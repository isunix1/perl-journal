# NoteBook - display windows with a notebook metaphor.

use Tk::NoteBook;
use Tk::LabEntry;
use vars qw/$top/;

sub NoteBook {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'NoteBook - display windows with a notebook metaphor.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $name = "Rajappa Iyer";
    my $email = "rsi\@somewhere.com";
    my $os = "Linux";

    # Create the NoteBook and add 2 pages, which are really Frames with tabs.
    # Given that, we create LabEntry widgets and pack them as usual.

    my $n = $top->NoteBook(qw/-ipadx 6 -ipady 6/)->pack;
    my $address = $n->add(qw/address -label Address     -underline 0/);
    my $pref    = $n->add(qw/pref    -label Preferences -underline 0/);

    $address->LabEntry(-label => "Name:             ", -width => 20,
		       -labelPack => [qw/-side left -anchor w/],
		       -textvariable => \$name)->pack(qw/-anchor nw/);
    $address->LabEntry(-label => "Email Address:", -width => 50,
		       -labelPack => [qw/-side left -anchor w/],
		       -textvariable => \$email)->pack(qw/-anchor nw/);
    $pref->LabEntry(-label => "Operating System:",-width => 15,
		    -labelPack => [qw/-side left/],
		    -textvariable => \$os)->pack(qw/-anchor nw/);
    
    $top->Button(qw/-text Update -command/ =>
	sub {
	    print "name = $name, email = $email, os = $os\n";
	})->pack;
}
