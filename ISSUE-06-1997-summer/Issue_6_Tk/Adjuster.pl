# Adjuster - allow packed widgets to be adjusted by a user.

use Tk::Adjuster;
use vars qw/$top/;

sub Adjuster {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'Adjuster - allow packed widgets to be adjusted by a user.',
    );
    $top = $demo_widget->top;	# get grid master
    my(@pack) = qw/-fill both -expand 1/;
    my $t1 = $top->Text(qw/-width 20 -height 5/)->packAdjust(@pack);
    foreach (1 .. 10) {$t1->insert('end', "Text 1, line $ARG\n")}
    my $t2 = $top->Text(qw/-width 20 -height 5/)->pack(@pack);
    foreach (1 .. 10) {$t2->insert('end', "Text 2, line $ARG\n")}
}
