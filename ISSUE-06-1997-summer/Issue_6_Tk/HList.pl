# HList - A hierarchial listbox widget.

use Tk::HList;
use subs qw/show_dir/;
use vars qw/$top/;

sub HList {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'HList - A hierarchial listbox widget.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $h = $top->Scrolled(qw\HList -separator / -itemtype imagetext\)->grid;
    $h->{file_image} = $top->Bitmap(-file => Tk->findINC('file.xbm'));
    $h->{fold_image} = $top->Bitmap(-file => Tk->findINC('folder.xbm'));

    $h->configure(-command => sub {
	print "Selected $ARG[0], size=", $h->info('data', $ARG[0]) ,".\n";
    });

    my $text = Tk->findINC('demos');
    chdir $text;
    show_dir '.', $text, $h;
}

sub show_dir {
    my($entry_path, $text, $h) = @ARG;
    opendir H, $entry_path;
    my(@dirent) = grep ! /^\.\.?$/, sort(readdir H);
    closedir H;
    my($file_img, $fold_img) = ($h->{file_image}, $h->{fold_image});
    $h->add($entry_path, -text => $text, -image => $fold_img, -data => 'DIR');

    while ($ARG = shift @dirent) {
	my $file = "$entry_path/$ARG";
	if (-d $file) {
	    show_dir $file, $ARG, $h;
	} else {
	    my $size = -s $file;
	    $h->add($file, -text => $ARG, -image => $file_img, -data => $size);
	}
    }

} # end show_dir
