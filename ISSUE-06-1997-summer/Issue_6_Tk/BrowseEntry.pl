# BrowseEntry - a LabEntry widget with a pulldown list.

use Tk::BrowseEntry;
use vars qw/$top/;

sub BrowseEntry {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'BrowseEntry - a LabEntry widget with a pulldown list.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $var;
    my $be = $top->BrowseEntry(-label => "Label", -variable => \$var)->grid;
    foreach my $opt (1 .. 15) {$be->insert('end', "opt$opt")}
}
