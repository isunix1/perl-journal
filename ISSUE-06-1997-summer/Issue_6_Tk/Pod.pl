# Pod - Display Plain Old Documentation.

use Tk::Pod;
use subs qw/pod/;
use vars qw/$top/;

sub Pod {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'Pod - Display Plain Old Documentation.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    $top->Button(-text => 'Show Pod', -command =>
        [$top => 'Pod', -file, pod])->grid;
}

sub pod {

    # Write POD data to a file.  Depending upon your application, -file
    # could easily refer to $0, ___FILE__, or even __DATA__.

    my $pod = '/tmp/pod.pl.pod';
    open(P, ">$pod") or die "Cannot open POD file $pod:  $OS_ERROR";
    print P << 'PODEND';

=head1 NAME

Breakout() - a simple game of Breakout.

=head1 SYNOPSIS

 use Tk::Breakout;

 $breakout = $MW->Breakout(
     -balls  => [{-color    => 'yellow',  -size     => 40,
                  -position => [90, 250], -velocity => [6.0, 9.0]},
                ],
     (other canvas creation options ...)
 );

=head1 DESCRIPTION

The -balls configure attribute is a list of hash.  Each ball has a color,
a diameter in pixels, a position given by a canvas X/Y coordinate pair, 
and a velocity that describes how many pixels to translate the ball in
the X and Y dimensions for each game tick.

=head1 METHODS

=over 4

=item B<move_balls($speed);>

$speed is the ball speed, as a percentage.
Essentially, move_balls() is invoked to move all of the balls in a
games's ball list once - from their current to their new postion.
After moving one ball a call to DoOneEvent() is made to handle pending 
XEvents.  The I<user> of this module has their own main loop which calls
DoOneEvent() and move_balls() to keep the game active.

=back

=cut
PODEND
    close P;
    return $pod;
}
