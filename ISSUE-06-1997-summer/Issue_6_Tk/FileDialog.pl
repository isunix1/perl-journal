# FileDialog - a fancier version of FileSelect.

use Tk::FileDialog;
use vars qw/$top/;

sub FileDialog {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'FileDialog - a fancier version of FileSelect.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $fdialog = $top->FileDialog('Path' => $ENV{HOME});
    $top->Button(qw/-text FileDialog -command/ => 
        sub {
	    my $fname = $fdialog->Show;
	    print "You selected $fname.\n" if defined $fname;
	    print "You canceled.\n" unless defined $fname;
        })->grid;
}
