# Dial - a widget similar to an automobile speedometer.

use Tk::Dial;
use vars qw/$top/;

sub Dial {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'Dial - a widget similar to an automobile speedometer.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    $top->Dial(qw/-margin 20 -radius 48 -min 0 -max 100 -value 0 -format %d/)->grid;
}
