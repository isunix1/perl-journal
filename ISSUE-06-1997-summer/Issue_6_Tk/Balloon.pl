# Balloon - popup MacOS-like help balloons.

use Tk::Balloon;
use vars qw/$top/;

sub Balloon {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'Balloon - popup MacOS-like help balloons.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $s = $top->Label(qw/-width 20 -borderwidth 2 -relief sunken/)->grid;
    my $t = $top->Text(qw/-width 20 -height 5 -background azure/)->grid;
    my $balloon = $top->Balloon(-statusbar => $s);
    $balloon->attach($s, -balloonmsg => 'See Balloon help here',
		     -statusmsg => 'This is a Label widget');
    $balloon->attach($t, -balloonmsg => 'Type Text Here',
		     -statusmsg => 'This is a Text widget');
}
