# FileSelect - a file/directory selector widget.

use Tk::FileSelect;
use vars qw/$top/;

sub FileSelect {
    my($demo) = @ARG;
    my $demo_widget = $MW->WidgetDemo(
        -name => $demo,
        -text => 'FileSelect - a file/directory selector widget.',
	-geometry_manager => 'grid',
    );
    $top = $demo_widget->top;	# get grid master
    my $fselect = $top->FileSelect(-directory => $ENV{HOME});
    $top->Button(qw/-text FileSelect -command/ => 
	sub {
	    my $fname = $fselect->Show;
	    print "You selected $fname.\n" if defined $fname;
            print "You canceled.\n" unless defined $fname;
	})->grid;
}
