#                              -*- Mode: Perl -*- 
# Man/Index.pm -- manual index implementation
# ITIID           : $Source: Man-Index/Man/Index.pm $ $Revision: 1.10 $
# Author          : Ulrich Pfeifer
# Created On      : Fri 25 Apr 14:19:19 1997
# Last Modified By: Ulrich Pfeifer
# Last Modified On: Sun Apr 27 12:39:19 1997
# Language        : Perl
# Status          : Demo for TPJ
# 
# (C) Copyright 1997, Ulrich Pfeifer, all rights reserved.
# 

package Man::Index;

use strict;
use vars qw($VERSION);
use IO::File;
use File::Path;

# use Text::English;
BEGIN {
  eval { require Text::English };
  unless ($@ eq '') {
    eval qq[sub Text::English::stem { \@_ }];
  }
}
use Carp;

# Best used with DB_File ;-) Make sure that you use version 1.86 of
# the db libraries. The 1.85 version will core dump after several
# thousand manuals and forget terms from time to time.
require AnyDBM_File;
BEGIN {
  shift @AnyDBM_File::ISA;      # NDBM_File sucks :-(
}

# $Format: "$VERSION = \"$ProjectVersion$\";"$ 
$VERSION = "0.19";

sub new {
  my $type  = shift;
  my %parm  = @_;
  my $self  = {};

  my $index = $parm{'index'} || croak "No 'index' argument given\n";
  my $rw    = $parm{rw};
  my $flags = ($rw)?O_RDWR:O_RDONLY;

  unless (-d $index) {
    if ($rw) {
      mkpath($index,1,0755) || croak "Could not mkpath '$index': $!\n";
      $flags |= O_CREAT;
    } else {
      croak "No index directory '$index': $!\n";
    }
  }
  tie %{$self->{documents}}, 'AnyDBM_File', "$index/documents", $flags, 0644
    or die "Could not tie '$index/documents': $!\n";
  tie %{$self->{doc_entry}}, 'AnyDBM_File', "$index/doc_entry", $flags, 0644
    or die "Could not tie '$index/doc_entry': $!\n";
  tie %{$self->{doc_no}},    'AnyDBM_File', "$index/doc_no",    $flags, 0644
    or die "Could not tie '$index/doc_no': $!\n";

  bless $self, $type;
}

sub documents {
  my $self = shift;
  my $term = shift;

  if (@_) {
    $self->{documents}->{$term} .= pack 'ww', @_;
  } else {
    unpack 'w*', $self->{documents}->{$term}; 
  }
}

sub num_documents {shift->{doc_entry}->{__maximum_document_number__}}

sub make_document_entry {
  my ($self, $path, $maxtf, $headline) = @_;
  my  $docno = ++$self->{doc_entry}->{__maximum_document_number__};

  $self->{doc_entry}->{$docno}  = pack('S',$maxtf) . join($;, $path, $headline);
  $self->{doc_no}->{$path}      = $docno;
  return $docno;
}

sub maxtf {
  my ($self, $doc_no) = @_;
  unpack 'S', $self->{doc_entry}->{$doc_no};
}

sub path {
  my ($self, $doc_no) = @_;

  (split /$;/, substr($self->{doc_entry}->{$doc_no},2))[0];
}

sub headline {
  my ($self, $doc_no) = @_;

  (split /$;/, substr($self->{doc_entry}->{$doc_no},2))[1];
}

sub is_reasonable {
  my ($self, $path) = @_;

  return if $self->{doc_no}->{$path}; # don't index twice
  return if -l $path;                 # don't index symbolic links ...
  return unless -s _ > 100;           # to small to be a real manual

  1;
}

sub open_file {
  my ($self, $path) = @_;
  my $fh;
  
  if ($path =~ /\.gz/) {
    $fh = new IO::File "gzip -cd $path|";
  } else {
    $fh = new IO::File "< $path";
  }
  carp "Could not read '$path': $!\n" unless $fh;
  $fh;
}

sub add_document {
  my ($self, $path) = @_;
  
  # check for symbolic links, and the like
  return unless $self->is_reasonable($path);

  # handle compression
  my $fh = $self->open_file($path) || return;

  my ($headline, $maxtf, %freqency, $line);
  while (defined($line = <$fh>)) {
    if (!$headline and $line =~ /\.S[Hh]( NAME)?/) {
      $headline = $self->get_headline($fh);
    }
    for my $term ($self->words($line)) {
      $freqency{$term}++;
      $maxtf = $freqency{$term} if $freqency{$term} > $maxtf;
    }
  }
  $headline ||= $path;
  my $doc_number = $self->make_document_entry($path, $maxtf, $headline);
  while (my($term, $freq) = each %freqency) {
    $self->documents($term, $doc_number, $freq);
  }
  return ($doc_number, $headline);
}

# This works only 99% on my box (Debian Linux). It's hard to extract
# the headline without actually running nroff :-(
sub get_headline {
  my ($self, $fh)= @_;
  my ($headline, $line);

  $line = <$fh> while !$line or $line =~ /^(\.IX|NAME)/;

  if ($line =~ s/\\?-/-/) {           # found the headline
    $headline = $line;
  } elsif ($line =~ s/^\.Nm\s+//i) {  # hack for some linux man pages
    $headline = $line . ' - ';
    $line = <$fh>;
    $line =~ s/^\.Nd\s+//i;
    $headline .= $line;
  }
  for ($headline) {
    s/\\f.//g;                        # remove \fB and the like
    s/\.[A-Z]+//g;                    # remove .IP ...
    s/\s+/ /g; s/\\//g;               # squeeze spaces, remove escapes
  }
  $headline;
}

sub search_raw {                      # asumes query words *are* normalized
  my $self  = shift;
  my (%score);

 TERM:
  for my $term (@_) {
    if (my %post = $self->documents($term)) {
      my $n   = scalar keys %post;    # document frequency
      my $idf = log($self->num_documents/$n);

      next TERM if $idf == 0;         # term occurs in *every* document
      for my $doc_no (keys %post) {
        my $maxtf  = $self->maxtf($doc_no);
        $score{$doc_no} += $post{$doc_no} / $maxtf * $idf;
      }
    }
  }
  \%score;
}

sub search {
  my $self  = shift;
  $self->search_raw($self->words(shift));
}


my %STOP;

sub words {
  my ($self, $text) = @_;

  for ($text) {
    s/\\f.//g;                        # \fB and the like
    tr/A-Za-z0-9/ /cs;
  }

  Text::English::stem(grep /../, grep !$STOP{$_}, split(' ', $text));
}

# initialize the stop word list
for (map split(' ', $_), <DATA>) {
  $STOP{$_}=1;
}

1;

__DATA__
about above according across actually adj after afterwards again
against all almost alone along already also although always among
amongst an and another any anyhow anyone anything anywhere are aren
around as at be became because become becomes becoming been before
beforehand begin beginning behind being below beside besides between
beyond billion both but by can can cannot caption co co could couldn
did didn do does doesn don down during eg eight eighty either else
elsewhere end ending enough etc even ever every everyone everything
everywhere except few fifty first five vfor former formerly forty
found four from further had has hasn have haven he he he ll he hence
her here here hereafter hereby herein hereupon hers herself him
himself his how however hundred ll ve ie if in inc indeed instead into
is isn it it its itself last later latter latterly least less let let
like likely ltd made make makes many maybe me meantime meanwhile might
million miss more moreover most mostly mr mrs much must my myself
namely neither never nevertheless next nine ninety no nobody none
nonetheless noone nor not nothing now nowhere of off often on once one
one only onto or other others otherwise our ours ourselves out over
overall own per perhaps rather recent recently same seem seemed
seeming seems seven seventy several she she she ll she should shouldn
since six sixty so some somehow someone something sometime sometimes
somewhere still stop such taking ten than that that ll that that ve
the their them themselves then thence there there there ll there re
there there ve thereafter thereby therefore therein thereupon these
they they they ll they re they ve thirty this those though thousand
three through throughout thru thus to together too toward towards
trillion twenty two under unless unlike unlikely until up upon us used
using very via was wasn we we we ll we re we ve well were weren what
what ll what what ve whatever when whence whenever where where
whereafter whereas whereby wherein whereupon wherever whether which
while whither who who who ll who whoever whole whom whomever whose why
will with within without won would wouldn yes yet you you you ll you
re you ve your yours yourself yourselves
