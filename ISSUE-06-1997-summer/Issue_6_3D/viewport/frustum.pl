#!/usr/bin/perl -w
#
# Creates a defaultly-sized GL viewport, and the view frustum

use OpenGL;

sub glInit {
    # Creates the OpenGL viewport to render into
    glpOpenWindow();

    # Create the view frustum, with the near clipping plane
    # at z = 1.0 and the far clipping plane at z = 20
    glMatrixMode( GL_PROJECTION );
    glFrustum( -1.0, 1.0, -1.0, 1.0, 1.0, 20.0 );
  }

### Main program body

# Initialize any GL stuff
glInit();

print "Press any key to exit\n";

while ( <> ) {
    exit;
  }
