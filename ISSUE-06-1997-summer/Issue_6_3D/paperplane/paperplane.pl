#!/usr/bin/perl -w
#
# $Id: paperplane.pl,v 1.2 1997/04/06 02:08:21 descarte Exp descarte $
#
# Copyright (c)1997 Alligator Descartes <descarte@hermetica.com>
#
# perl implementation of the ``paperplane'' demo by Mark Kilgard
#
# $Log: paperplane.pl,v $
# Revision 1.2  1997/04/06 02:08:21  descarte
# Added resize and expose event handlers.
#

use OpenGL;
use POSIX;       # For atan()
require 'plane.pl';

$MAXPLANES = 20;
$currentPlane = 0;

### Initialization function
sub myinit {

    # Create the viewport
    glpOpenWindow( width => 300, height => 300,
                   attributes => [ GLX_RGBA, GLX_DOUBLEBUFFER ],
                   mask => StructureNotifyMask );

    # Setup the ConfigureNotify event handler
    $eventHandler{&ConfigureNotify} =
        sub {
            my ( $event, $width, $height ) = @_;
            print "Resizing viewport to $width x $height\n";
            glViewport( 0, 0, $width, $height );
          };

    # Set the z-buffer clear depth
    glClearDepth( 1.0 );

    # Set the z-buffer clear colour to black
    glClearColor( 0.0, 0.0, 0.0, 0.0 );

    # Define the view frustum
    glMatrixMode( GL_PROJECTION );
    glFrustum( -1.0, 1.0, -1.0, 1.0, 1.0, 20 );

    # Get ready to specify some objects in the scene!
    glMatrixMode( GL_MODELVIEW );

    # Now, add three planes to begin with
    srand( $$ );
    addPlane();
    addPlane();
    addPlane();

  }

### Adds a plane to the scene
sub addPlane {

    $planes[$currentPlane] = new plane;
    my $randValue = rand( 6 );
    SWITCH: {
        if ( $randValue >= 0 && $randValue < 1 ) {
            $planes[$currentPlane]->setColour( 1.0, 0.0, 0.0 );
            last SWITCH;
          }
        if ( $randValue >= 1 && $randValue < 2 ) {
            $planes[$currentPlane]->setColour( 1.0, 1.0, 1.0 );
            last SWITCH;
          }
        if ( $randValue >= 2 && $randValue < 3 ) {
            $planes[$currentPlane]->setColour( 0.0, 1.0, 0.0 );
            last SWITCH;
          }
        if ( $randValue >= 3 && $randValue < 4 ) {
            $planes[$currentPlane]->setColour( 1.0, 0.0, 1.0 );
            last SWITCH;
          }
        if ( $randValue >= 4 && $randValue < 5 ) {
            $planes[$currentPlane]->setColour( 1.0, 1.0, 0.0 );
            last SWITCH;
          }
        if ( $randValue >= 5 && $randValue < 6 ) {
            $planes[$currentPlane]->setColour( 0.0, 1.0, 1.0 );
            last SWITCH;
          }
      }

    # Set the speed of each plane now
    $planes[$currentPlane]->setSpeed( rand( 20 ) * 0.001 + 0.02 );
    if ( rand() & 0x01 ) {
        my $speed = $planes[$currentPlane]->getSpeed();
        $planes[$currentPlane]->setSpeed( $speed *= -1 );
      }

    # Set the angle of the plane
    $planes[$currentPlane]->setTheta( ( rand() * 256 ) * 0.1111 );

    # Update the planes location and orientation
    $planes[$currentPlane]->tick();

    $currentPlane++;
  }

### Handles reshapes of the window
sub myReshape {
    display();
  }

### Display callback
sub display {

    # Clear the z-buffer
    glClear( GL_DEPTH_BUFFER_BIT );

    # And disable depth-testing just now
    glDisable( GL_DEPTH_TEST );

    # Make sure we're smooth-shading just now. We want to
    # do this since we want to blend the colours of the
    # background polygon
    glShadeModel( GL_SMOOTH );

    # Draw the graded black -> blue polygon first
    glBegin( GL_POLYGON );
        glColor3f( 0, 0, 0 );
        glVertex3f( -20, 20, -19 );
        glVertex3f( 20, 20, -19 );
        glColor3f( 0, 0, 1 );
        glVertex3f( 20, -20, -19 );
        glVertex3f( -20, -20, -19 );
    glEnd();

    # Enable depth-testing...
    glEnable( GL_DEPTH_TEST );

    # And now flat-shade stuff
    glShadeModel( GL_FLAT );

    # Draw each plane.........
    for ( $i = 0 ; $i < $currentPlane ; $i++ ) {
#        if ( $blah ) {
            glPushMatrix();
            glTranslatef( $planes[$i]->getX(), $planes[$i]->getY(), $planes[$i]->getZ() );
            glRotatef( 290.0, 1.0, 0.0, 0.0 );
            glRotatef( $planes[$i]->getAngle(), 0.0, 0.0, 1.0 );
            glScalef( 1.0 / 3.0, 1.0 / 4.0, 1.0 / 4.0 );
            glTranslatef( 0.0, -4.0, -1.5 );
            glBegin( GL_TRIANGLE_STRIP );
                glVertex3f(-7.0, 0.0, 2.0);
                glVertex3f(-1.0, 0.0, 3.0);
                my $red = $planes[$i]->getRed();
                my $green = $planes[$i]->getGreen();
                my $blue = $planes[$i]->getBlue();
                glColor3f( $red, $green, $blue );
                glVertex3f(-1.0, 7.0, 3.0);
                # Left side
                glColor3f( 0.6 * $red, 0.6 * $green, 0.6 * $blue );
                glVertex3f(0.0, 0.0, 0.0);
                glVertex3f(0.0, 8.0, 0.0);
                # Right side
                glVertex3f(1.0, 0.0, 3.0);
                glVertex3f(1.0, 7.0, 3.0);
                # Final tip of right wing
                glColor3f( $red, $green, $blue);
                glVertex3f(7.0, 0.0, 2.0);
            glEnd();
            glPopMatrix();
#          }
      }

    glFlush();
    glXSwapBuffers();
  }

### Main body of program

myinit();
myReshape();
display();

while ( 1 ) {
    # Check for any outstanding X events
    while ( $pendingEvent = XPending ) {
        my @event = &glpXNextEvent;
        if ( $s = $eventHandler{$event[0]} ) {
            &$s( @event );
          }
      }

    # Move all the planes
    for ( $i = 0 ; $i < $currentPlane ; $i++ ) {
        $planes[$i]->tick();
      }

    # Redisplay the new scene
    display();
  }
