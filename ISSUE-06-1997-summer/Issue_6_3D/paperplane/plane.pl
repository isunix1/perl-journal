#!/usr/bin/perl -w
#
# $Id: plane.pl,v 1.1 1997/04/05 05:35:39 descarte Exp descarte $
#
# Copyright (c)1997 Alligator Descartes <descarte@hermetica.com>
#
# perl class encapsulating a paper-plane
#
# $Log: plane.pl,v $
# Revision 1.1  1997/04/05 05:35:39  descarte
# Initial revision
#

package plane;

use POSIX;       # For atan()

### Constructs a new plane object
sub new {
    my $self = {};
    $self->{speed} = 0;
    $self->{red} = 0;
    $self->{green} = 0;
    $self->{blue} = 0;
    $self->{theta} = 0;
    $self->{"x"} = 0;
    $self->{"y"} = 0;
    $self->{"z"} = 0;
    $self->{angle} = 0;
    bless( $self );
    return $self;
  }


# Gets the X position of the plane
sub getX {
    ( $self ) = @_;
    return $self->{"x"};
  }

# Gets the Y position of the plane
sub getY {
    ( $self ) = @_;
    return $self->{"y"};
  }

# Gets the Z position of the plane
sub getZ {
    ( $self ) = @_;
    return $self->{"z"};
  }

# Retrurns the angle of the plane
sub getAngle {
    ( $self ) = @_;
    return $self->{angle};
  }

# Sets the colour of the plane.
#
# ARGV[0]: Red component
# ARGV[1]: Green component
# ARGV[2]: Blue component
sub setColour {
    ( $self, $red, $green, $blue ) = @_;

    $self->{red} = $red;
    $self->{green} = $green;
    $self->{blue} = $blue;
  }

# Gets the red component of the plane's colour
sub getRed {
    ( $self ) = @_;
    return $self->{red};
  }

# Gets the green component of the plane's colour
sub getGreen {
    ( $self ) = @_;
    return $self->{green};
  }

# Gets the blue component of the plane's colour
sub getBlue {
    ( $self ) = @_;
    return $self->{blue};
  }

# Returns a string containing the R:G:B values of the plane
sub getColour {
    ( $self ) = @_;
    return $self->{red} . ":" . $self->{green} . ":" . $self->{blue};
  }

# Returns the speed of the plane
sub getSpeed {
    ( $self ) = @_;
    return $self->{speed};
  }

# Sets the speed of the plane
#
# ARGV[0]: The new speed
sub setSpeed {
    ( $self, $speed ) = @_;
    $self->{speed} = $speed;
  }

# Sets the theta of the plane
#
# ARGV[0]: The new theta
sub setTheta {
    ( $self, $theta ) = @_;
    $self->{theta} = $theta;
  }

# Updates the position of the plane by calculating the new position and
# rotation
sub tick {
    ( $self ) = @_;
    $self->{theta} += $self->{speed};
    my $theta = $self->{theta};
    $self->{z} = -9 + ( 4 * cos( $theta ) );
    $self->{"x"} = 4 * sin( 2 * $theta );
    $self->{"y"} = sin( $theta / 3.4 ) * 3;
    $self->{angle} = ( ( POSIX::atan( 2.0 ) + ( 3.14 / 2 ) ) * sin( $theta ) - ( 3.14 / 2 ) ) * 180 / 3.14;
    if ( $self->{speed} < 0.0 ) {
        $self->{angle} += 180;
      }
  }

1;
