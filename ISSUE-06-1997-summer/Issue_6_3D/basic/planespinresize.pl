#!/usr/bin/perl -w
#
# planespin.pl
#
# Spins a paperplane. This demostrates icky single-buffering.
#
# WARNING: For some odd reason, this will crash upon resizing to a size
#          larger than the original. Be warned!

use OpenGL;

# The angle the plane will be rotated on the y-axis
$angle = 0;

### Initialization function
sub glInit {

    # Creates a 200 x 200, double-buffered viewport that will handle
    # *Notify X events
    glpOpenWindow( width => 400, height => 400,
                   attributes => [ GLX_RGBA, GLX_DOUBLEBUFFER ],
                   mask => StructureNotifyMask );

    $eventHandler{&ConfigureNotify} =
        sub {
            my ( $event, $width, $height ) = @_;
            # Upon reception of a ConfigureNotify event,
            # resize the GL viewport to the desired size
            print "Resizing viewport to $width x $height\n";
            glViewport( 0, 0, $width, $height );
          };

    # Define the view frustum
    glMatrixMode( GL_PROJECTION );
    glFrustum( -1.0, 1.0, -1.0, 1.0, 1.0, 20 );

    # Get ready to specify some objects in the scene!
    glMatrixMode( GL_MODELVIEW );
  }

### Display callback
sub display {

    # Clear the frame-buffer and z-buffer from previous renders
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    # Make sure we're smooth-shading just now. We want to
    # do this since we want to blend the colours of the
    # background polygon
    glShadeModel( GL_SMOOTH );

    # Draw the graded black -> blue polygon first
    glBegin( GL_POLYGON );
        glColor3f( 0, 0, 0 );
        glVertex3f( -20, 20, -19 );
        glVertex3f( 20, 20, -19 );
        glColor3f( 0, 0, 1 );
        glVertex3f( 20, -20, -19 );
        glVertex3f( -20, -20, -19 );
    glEnd();

    # Enable depth-buffering
    glEnable( GL_DEPTH_TEST );

    # Push the original matrix for model transformation onto the
    # matrix stack for safe-keeping
    glPushMatrix();

    # Load the identity matrix to start with a clean slate
    glLoadIdentity();

    # Translate the origin 
    glTranslatef( 0, 0, -10 );

    # Rotate everything by 290 degress around the x-axis
    glRotatef( 290.0, 1.0, 0.0, 0.0 );

    # Rotate everything by this incremented angle around the
    # y-axis
    glRotatef( $angle, 0.0, 0.0, 1.0 );

    # Scale things
    glScalef( 1.0 / 3.0, 1.0 / 4.0, 1.0 / 4.0 );

    # Yet another translation!
    glTranslatef( 0.0, -4.0, -1.5 );

    # Begin drawing a triangle strip
    glBegin( GL_TRIANGLE_STRIP );
        glVertex3f(-7.0, 0.0, 2.0);
        glVertex3f(-1.0, 0.0, 3.0);
        my $red = 1;
        my $green = 0;
        my $blue = 0;
        glColor3f( $red, $green, $blue );
        glVertex3f(-1.0, 7.0, 3.0);
        # Left side
        glColor3f( 0.6 * $red, 0.6 * $green, 0.6 * $blue );
        glVertex3f(0.0, 0.0, 0.0);
        glVertex3f(0.0, 8.0, 0.0);
        # Right side
        glVertex3f(1.0, 0.0, 3.0);
        glVertex3f(1.0, 7.0, 3.0);
        # Final tip of right wing
        glColor3f( $red, $green, $blue);
        glVertex3f(7.0, 0.0, 2.0);
    glEnd();

    # Pop the original model matrix off the stack
    glPopMatrix();

    # Update the display
    glFlush();
    glXSwapBuffers();
  }

### Main body of program

glInit();
display();

while ( 1 ) {

    # Check for any outstanding X events
    while ( $pendingEvent = XPending ) {
        my @event = &glpXNextEvent;
        if ( $s = $eventHandler{$event[0]} ) {
            &$s( @event );
          }
      }

    # Increment the angle around the y-axis the plane
    # will be rotated
    $angle += 5;

    # Make sure the angle doesn't go over 360. That's
    # just silly
    $angle %= 360;

    # Re-display everything
    display();
  }
