#!/usr/bin/perl -w

use Devel::DumpStack qw(stack_as_string);
use HTML::Entities;

sub my_die {
    select(STDOUT);$|=1;
    printf(<<"EOF", $?, $!, stack_as_string());
Content-Type: text/html

<HTML>
<HEAD>
<TITLE>System Error</TITLE>
</HEAD>

<BODY>
<H1>System Error</H1>
A seriously bad system error happened:<P>

<B>Exit Status</B>: %d<BR>
<B>Error String</B>: %s<P>

<B>Stack Dump</B>:
<PRE>
%s
</PRE>

</BODY>
</HTML>
EOF
  exit;
}

BEGIN {
  $SIG{__WARN__} = $SIG{__DIE__} = \&my_die;
}

$a = undef + 4;
exit;