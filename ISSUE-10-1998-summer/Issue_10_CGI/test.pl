#!/usr/local/bin/perl

# preliminaries to satisfy taint checks
$ENV{PATH} = '/bin:/usr/bin';
$ENV{IFS} = '';

$PASSWD = "/usr/bin/passwd";
$SU = '/bin/su';

($rv,$msg) = set_passwd('wanda','slutty','test000');
print $msg,"\n";

sub set_passwd ($$$) {
    require "chat2.pl";
    my $TIMEOUT = 1;

    my($user,$old,$new) = @_;

    my $h = chat::open_proc($SU,'-c',$PASSWD,$user) 
	|| return (undef,"Couldn't open $SU -c $PASSWD: $!");
    
    # wait for su's prompt for password
    my $rv = &chat::expect($h,$TIMEOUT,
			   'Password:'=>"'ok'");
    $rv eq 'ok'	|| return (undef,"Didn't get su password prompt.");
    chat::print($h,"$old\n");

    # wait for prompt for old password
    $rv = &chat::expect($h,$TIMEOUT,
			'Enter old password:'=>"'ok'",
			'incorrect password' =>"'not ok'");
    $rv || return (undef,"Didn't get prompt for old password.");
    $rv eq 'not ok' && return (undef,"Old password is incorrect.");

    # print old password
    chat::print($h,"$old\n");
    $rv = &chat::expect($h,$TIMEOUT,
			   'Enter new password: '=>"'ok'",
			   'Illegal'=>"'not ok'");
    $rv || return (undef,"Timed out without seeing prompt for new password.");
    $rv eq 'not ok' && return (undef,"Old password is incorrect.");

    # print new password
    chat::print($h,"$new\n");
    ($rv,$msg) = &chat::expect($h,$TIMEOUT,
			       'Re-type new password: ' => "'ok'",
			       '([\s\S]+)Enter new password:' => "('rejected',\$1)"
			       );
    $rv || return (undef,"Timed out without seeing 2d prompt for new password.");
    $rv eq 'rejected' && return (undef,$msg);

    # reconfirm password
    chat::print($h,"$new\n");
    $rv = &chat::expect($h,$TIMEOUT,
			'Password changed' => "'ok'");
    $rv || return (undef,"Password program failed at very end.");
    chat::close($h);

    return (1,"Password changed successfully for $Q::user.");
}

sub relinquish_privileges {
    $) = $(;
    $> = $<;
}

sub create_form {
    print
	start_form,
	table(
	      TR({align=>RIGHT},
		 th('User name'),   td(textfield(-name=>'user')),
		 th('Old password'),td(password_field(-name=>'old'))),
	      TR({align=>RIGHT},
		 th('New password'),td(password_field(-name=>'new1')),
		 th('Confirm new password'),td(password_field(-name=>'new2'))),
	      ),
	    hidden(-name=>'referer',-value=>referer()),
	    submit('Change Password'),
	    end_form;
}

sub do_error ($) {
    print font({-color=>'red'},b('Error:'),shift," Password not changed.");
}
