
# Mow.pm - mowing module.
#
# Stephen O. Lidie@Lehigh.EDU, 97/09/15.

package Mow;

use 5.004;
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw/$CHLOROPHYLL $COLOR $CUT $D2R $PPF $SIDE $TURN/;

$CHLOROPHYLL = '#8395ffff0000';	# rye-grass-green, maybe
$COLOR = 0xffff;		# initial line color, maximum saturation
$CUT = (38 / 12);		# cut width in feet
$D2R = 3.14159265 / 180.0;	# map degrees to radians
$PPF = 2;			# pixels/foot
$SIDE = 100;			# length of side of square mowing area, in feet
$TURN = (27 / 12);		# turn radius in feet

1;
