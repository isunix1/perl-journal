#!/usr/bin/perl

use Regex;


push @times, time;
my $e = Regex->parse(q{((0|1|2|3|4|5|6|7|8|9)+|::)*});
push @times, time;
my $m = NFA->compile($e);
push @times, time;

$s = '::1234::5678901234567890::::1235467890::888:';
my $r = NFA_Exec->match($m, $s);
push @times, time;
print "String `$s' ", $r ? 'matched' : 'did not match', "\n";


$r = ($s =~ /^(?:\d+|::)*$/);
push @times, time;
print "String `$s' ", $r ? 'matched' : 'did not match', "\n";

my $j = 0;
foreach $t (@times) {
  $ot = $t;
  $t -= $j; 
  $j = $ot;
}

print "Elapsed: (@times) seconds.\n";
