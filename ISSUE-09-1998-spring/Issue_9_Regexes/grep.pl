#!/usr/bin/perl
#
# Version of `grep' that does not use Perl's
# built-in regular expressions.
#
# Author: Mark-Jason Dominus (mjd-tpj-regex@plover.com)
# This program is in the PUBLIC DOMAIN.
#

use Regex;

my $pattern = shift or die "Usage: $0 pattern [filename...]\n";

### We would like to do this:
#$pattern = ".*$pattern.*";  # Use normal `grep' semantics
### But OMAR has not implemented `.' yet.  If your Regex.pm has an
### implementation of `.', you should uncomment that line.

my $regex   = Regex->parse($pattern);
my $machine = NFA->compile($regex);  # Build the machine ONCE.

while (<>) {
  chomp;
  # Use the machine MANY TIMES without rebuilding it
  my $it_did_match = NFA_Exec->match($machine, $_);
  print $_, "\n" if $it_did_match;
} continue {
  print STDERR "$. lines processed.\n" if $. % 25 == 0;
}

