#!/usr/bin/perl -w

use strict;
use XML::Parser;

die "Usage: xmlfaq.pl <file>" unless @ARGV == 1;
my $parser = new XML::Parser(Handlers => {Start => \&handle_start,
                                          End   => \&handle_end,
			                  Char  => \&handle_char});
$parser->parsefile($ARGV[0]);

sub handle_start {
    my $p = shift;
    my $el = shift;

    if ($el =~ /\bheader\b/i) {
	print "<CENTER><HR>";
    } elsif ($el =~ /\btitle\b/i) {
	print "<H1>";
    } elsif ($el =~ /\bauthor\b/i) {
	print "Author: <B>";
    } elsif ($el =~ /\bversion\b/i) {
	print "Version: <B>";
    } elsif ($el =~ /\babstract\b/i) {
	print '<FONT SIZE=+1><B>Abstract</B></FONT><P>';
    } elsif ($el =~ /\bsection\b/i) {
	print "<UL>";
    } elsif ($el =~ /\bquestion\b/i) {
	print "<li><B>Q:</B>";
    } elsif ($el =~ /\banswer\b/i) {
	print "<dl><B>A:</B>";
    }
}

sub handle_char {
    my ($p, $data) = @_;
    print $data;
}

sub handle_end {
    my $p = shift;
    my $el = shift;

    if ($el =~ /\bheader\b/i) {
	print "<HR></CENTER>";
    } elsif ($el =~ /\btitle\b/i) {
	print "</H1>";
    } elsif ($el =~ /\bauthor\b/i) {
	print "</B><BR>";
    } elsif ($el =~ /\bversion\b/i) {
	print "</B><BR>";
    } elsif ($el =~ /\babstract\b/i) {
	print "<P>\n";
    } elsif ($el =~ /\bsection\b/i) {
	print "</UL>";
    } elsif ($el =~ /\bquestion\b/i) {
	print "</li>";
    } elsif ($el =~ /\banswer\b/i) {
	print "</dl>";
    }
}
















