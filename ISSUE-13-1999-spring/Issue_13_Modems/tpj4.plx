#! perl -w

use strict;
use Win32::SerialPort;

my $ob = Win32::SerialPort->new ('COM2') || die;

$ob->user_msg(1);	# misc. warnings
$ob->error_msg(1);	# hardware and data errors

$ob->baudrate(38400);
$ob->parity("none");
## $ob->parity_enable(1);   # for any parity except "none"
$ob->databits(8);
$ob->stopbits(1);
$ob->handshake('rts');

$ob->write_settings;
$ob->save("tpj4.cfg");

print "wrote configuration file tpj4.cfg\n";

undef $ob;
