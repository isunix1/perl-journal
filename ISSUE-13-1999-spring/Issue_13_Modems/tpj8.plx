#! perl -w

use strict;
use Win32::SerialPort 0.11;

my $ob = Win32::SerialPort->start ("tpj4.cfg") || die;

my $baud = $ob->baudrate(1200);
print "baud for background demo: $baud\n";

$ob->read_interval(0);
$ob->read_const_time(10000);

$ob->write("ATE0X4\r");
sleep 1;
my $result = $ob->input;
print "result = $result\n";

$ob->write("AT&V\r");
print "Starting 500 character background read\n";
my $in = $ob->read_bg(500);
my $done = 0;
my $blk;
my $err;
my $out;
for (;;) {
    ($blk, $in, $out, $err)=$ob->status;
    print "got $in characters so far..\n";
    sleep 1;
    ($done, $in, $result) = $ob->read_done(0);
    last if $done;
}
print "got = $in\nresult = $result\n";

$baud = $ob->baudrate(38400);
sleep 2;
$result = $ob->input;
print "\n\n....And now the rest = \n$result\n";

undef $ob;
