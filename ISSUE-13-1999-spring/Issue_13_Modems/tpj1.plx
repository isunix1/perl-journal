#! perl -w

use strict;
use Win32::SerialPort;

my $ob = Win32::SerialPort->new ('COM2') || die;

my $baud = $ob->baudrate;
my $parity = $ob->parity;
my $data = $ob->databits;
my $stop = $ob->stopbits;
my $hshake = $ob->handshake;

print "B = $baud, D = $data, S = $stop, P = $parity, H = $hshake\n";

undef $ob;
