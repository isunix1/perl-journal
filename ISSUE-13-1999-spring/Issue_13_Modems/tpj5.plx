#! perl -w

use strict;
use Win32::SerialPort 0.11;

my $ob = Win32::SerialPort->start ("tpj4.cfg") || die;

my $baud = $ob->baudrate;
print "baud from configuration: $baud\n";

$ob->write("ATE0X4\r");
sleep 1;
my $result = $ob->input;
print "result = $result\n";

$ob->write("AT&V\r");
sleep 2;
$result = $ob->input;
print "result = $result\n";

undef $ob;
