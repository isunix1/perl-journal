#! perl -w

use strict;
use Win32::SerialPort 0.13;

my $ob;
my $file = "tpj4.cfg";

sub waitfor {
    my $timeout=Win32::GetTickCount() + (1000 * shift);
    $ob->lookclear;  # clear buffers
    my $gotit = "";

    for (;;) {
        return unless (defined ($gotit = $ob->lookfor));
        if ($gotit ne "") {
            my ($found, $end) = $ob->lastlook;
            return $gotit.$found;
        }
        return if ($ob->reset_error);
	return if (Win32::GetTickCount() > $timeout);
    }
}

# =============== execution begins here =======================

$ob = Win32::SerialPort->start ($file) or die "Can't start $file\n";

$ob->error_msg(1);		# use built-in error messages
$ob->user_msg(1);

$ob->are_match("BUSY","CONNECT","OK","NO DIALTONE","ERROR","RING",
	       "NO CARRIER","NO ANSWER");

$ob->write("ATE0X4\r");
printf "%s\n", waitfor(1);  # 1 second

print "\nStarting Dial\n";
$ob->write("ATDT6765707\r");  # number will vary
printf "%s\n", waitfor(20);

## $ob->purge_rx || die "couldn't purge";

print "\n5 seconds to failure..\n";
waitfor(5) || print "Timed Out\n";

undef $ob;
