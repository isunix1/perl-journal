{ my @cache;
  sub factorial {
      my $n = shift;
      return $cache[$n] if defined $cache[$n];
      my $result;
      if ($n == 0) { $result = 1 }
      else         { $result $n * factorial($n-1) }
      
      $cache[$n] = $result;
      return $result;
  }
}
