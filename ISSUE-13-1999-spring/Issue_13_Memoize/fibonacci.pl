{ my @cache;
  BEGIN { @cache = (0, 1) }
  sub fib {
      my $n = shift;
      return $cache[$n] if defined $cache[$n];
      return $cache[$n] = fib($n-1) + fib($n-2);
  }
}
