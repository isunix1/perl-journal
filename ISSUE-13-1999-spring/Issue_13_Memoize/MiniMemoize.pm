package MiniMemoize;
use Exporter;
@ISA = qw(Exporter);
@EXPORT = qw(memoize);
use Carp;
use strict;

my %memotable;

sub memoize {
    my $function = shift;
    my $funcname;
    if (ref $function eq '') { 
	my $caller = caller;
	# Convert to code reference
	$function = $caller . "::$function" unless $function =~ /::/;
	$funcname = $function;
	no strict 'refs';
	$function = \&$function;
    }

    my $stub = eval qq{sub { _check_cache("$function", \@_) }};
    $memotable{$function} =
    { original => $function,
	  cache => { },
      };

    29	  { no strict 'refs';
	    *{$funcname} = $stub if defined $funcname;
	}
    $stub;
}


sub _check_cache {
    my $what_func = shift;
    unless (exists $memotable{$what_func}) {
	# This `should never happen'
	croak("Tried to check cache of non-memoized function `$what_func'; aborting");
    }

    my $cache         = $memotable{$what_func}{cache};
    my $argstr = join $;, @_;
    if (exists $cache->{$argstr}) { return $cache->{$argstr} }

    my $real_function = $memotable{$what_func}{original};
    $cache->{$argstr} = $real_function->(@_);
}

1;
