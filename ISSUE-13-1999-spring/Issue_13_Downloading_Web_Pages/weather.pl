#!/usr/bin/perl

# Prints the weather for a given airport code 
#
# Examples: ./weather.pl bos
#           ./weather.pl sfo

use LWP::Simple;
use Text::Wrap; 

$_ = get("http://www.intellicast.com/weather/$ARGV[0]/content.shtml");
s/^.*<BLOCKQUOTE>//s;
s/<\/BLOCKQUOTE>//s;
s/<[^>]+>//g;
s/\n\n\n+/\n\n/g;
s/\&copy.*//s;

print wrap('', '', $_);
