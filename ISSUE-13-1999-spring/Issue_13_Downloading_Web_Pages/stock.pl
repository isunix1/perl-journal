#!/usr/bin/perl

# Pulls a delayed stock quote from Salomon Smith Barney's web site.
#
#        ./stock.pl ibm
#
# or whatever stock ticker symbol you want.

use LWP::UserAgent;
use HTTP::Request::Common;

$response = (new LWP::UserAgent)->request(POST 
	     'http://www.smithbarney.com/cgi-bin/benchopen/quoteget', 
	     [search_type => "1",
	      search_string => "$ARGV[0]",
	      ]);

exit -1 unless $response->is_success;
$_ = $response->{_content};
s/<[^>]+>//g;
s/^.*recent close[^a-zA-Z0-9]+//s;
@t = split(/\n\n+/);
print shift(@t), "\n";
@h = split(/\n/, shift(@t));
foreach (@h){
    ($f = shift(@t)) =~ s/\n//g;
    next unless $f =~ /\d/;
    print $_, ": ", $f, " ";
    print " at ", shift(@t) if /L/;
    print "\n";
}
