#!/usr/bin/perl -w
# Need *either* state *or* zip

use LWP::UserAgent;
use HTTP::Request::Common;

$ua = new LWP::UserAgent;
$resp = $ua->request(POST 'http://www.usps.com/cgi-bin/zip4/zip4inq',[@ARGV]);

exit -1 unless $resp->is_success;
($_ = $resp->{_content}) =~ s/^.*address is:<p>\n//si;
s/Version .*$//s;
s/<[^>]+>//g;
print;
