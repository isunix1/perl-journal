#!/usr/bin/perl

use LWP::Simple;
use Text::Wrap;

while (sleep 300) { 
    $_ = get("http://www.cnn.com");
    s/^.*Top Table//s;
    s/<[^>]+>//g;
    s/FULL STORY.*$//s;
    s/^.*>\s+//s;
    s/\n\n+/\n\n/g;
    if ($old ne $_) { print wrap('', '', $_); $old = $_ }
}
