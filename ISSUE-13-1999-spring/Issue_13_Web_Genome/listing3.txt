Listing 3:  This script reads the "scorsese" Person object from a
networked ACEDB database and updates it.

#!/usr/local/bin/perl

use Ace;
my $db = Ace->connect(-port=>200008,
                      -host=>'stein.cshl.org') 
	|| die "Can't connect ",Ace->error;
            
my $scorsese = $db->fetch(Person => 'scorsese')
	|| die "Can't get scorsese object ",Ace->error;

my (@phone) = $scorsese->Phone;
unless (@phone) {
   $scorsese->add(Phone => '555-1212');
   $scorsese->commit || die "Can't commit ",Ace->error;
}

