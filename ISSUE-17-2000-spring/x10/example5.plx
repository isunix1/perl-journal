#!/usr/bin/perl

use vars qw(%config_parms);
use ControlX10::CM11 qw( :FUNC 2.06 );

use strict;

## emulate a CM11A and xx::SerialPort (test mode) for testing

package SerialStub;

sub reset_error	{ return 0; }

sub new {
    my $class = shift;
    my $self  = {};
    $self->{"_T_INPUT"}		= chr(0xa5);	# power-fail
    return bless ($self, $class);
}

sub write {
    return unless (@_ == 2);
    my $self = shift;
    my $wbuf = shift;
    my $response = "";
    return unless ($wbuf);
    my @loc_char = split (//, $wbuf);
    my $f_char = ord (shift @loc_char);
    if ($f_char == 0x00) {
	$response = chr(0x55);
	$self->fakeinput($response);
	return 1;
    }
    elsif ($f_char == 0xc3) {
	$response = chr(0x03).chr(0x02).chr(0x6e).chr(0x62); # A2AJ
	$self->fakeinput($response);
	return 1;
    }
    else {
	my $ccount = 1;
	my $n_char = "";
	foreach $n_char (@loc_char) {
	    $f_char += ord($n_char);
	    $ccount++;
	}
	$response = chr($f_char & 0xff);
	$self->fakeinput($response);
	return $ccount;
    }
}

sub fakeinput {
    my $self = shift;
    return unless (@_);
    $self->{"_T_INPUT"} = shift;
    1;
}

sub input {
    return undef unless (@_ == 1);
    my $self = shift;
    my $result = "";

    if ($self->{"_T_INPUT"}) {
	$result = $self->{"_T_INPUT"};
	$self->{"_T_INPUT"} = "";
	return $result;
    }
}

######################### End CM11A emulator

package main;

use strict;

my $serial_port = SerialStub->new ();

######################### End of port initialization.

my $no_block = 1;

read_cm11($serial_port, $no_block);

print "-------\n\n";
print "Sending A1 ON\n";
send_cm11($serial_port, 'A1');
send_cm11($serial_port, 'AJ');
print "Sending A1 OFF\n";
send_cm11($serial_port, 'A1');
send_cm11($serial_port, 'AK');

print "-------\n\n";
my $incoming = chr(0x5a);
$serial_port->fakeinput($incoming);	# force "input waiting"

if (read_cm11($serial_port, $no_block)) {
    my $datain = receive_cm11($serial_port);
    print "Received $datain\n" if (defined $datain);
}

print "Sending A2 OFF\n";
send_cm11($serial_port, 'A2');
send_cm11($serial_port, 'AK');

__END__
