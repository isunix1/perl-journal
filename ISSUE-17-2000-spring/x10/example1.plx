#!/usr/bin/perl -w

################ emulate a CM17A and xx::SerialPort for testing

package SerialStub;
use strict;

sub new {
    my $class = shift;
    my $self  = {};
    return bless ($self, $class);
}

sub dtr_active {1}

sub rts_active {1}

sub pulse_dtr_off {		# "1" bit
    my $self = shift;
    my $delay = shift || 1;     # minimum length of pulse
    select (undef, undef, undef, $delay/1000);
    return $delay;
}

sub pulse_rts_off {		# "0" bit
    my $self = shift;
    my $delay = shift || 1;
    select (undef, undef, undef, $delay/1000);
    return $delay;
}

######################### End CM17A emulator

package main;

use ControlX10::CM17 qw( send_cm17 0.06 );
use strict;
use vars qw(%config_parms);
$main::config_parms{debug} = "";	# used inside CM17.pm

my $serial_port = SerialStub->new ();

print "Turning address A1 ON\n";
ControlX10::CM17::send($serial_port, 'A1J');
print "Turning address A1 OFF\n\n";
ControlX10::CM17::send($serial_port, 'A1K');

$main::config_parms{debug} = "X10";	# print verbose description
print "Turning address A1 ON with debugging\n";
send_cm17($serial_port, 'A1J');
print "Turning address A1 OFF\n";
send_cm17($serial_port, 'A1K');

__END__
