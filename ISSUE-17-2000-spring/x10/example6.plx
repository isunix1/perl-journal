#!/usr/bin/perl

# Usage: perl example6.plx PORT

use vars qw($OS_win $port %config_parms);
$main::config_parms{debug} = "";	# used inside CM11.pm

############## We start with some cross-platform black magic

BEGIN {
    $| = 1;
    $OS_win = ($^O eq "MSWin32") ? 1 : 0;

        # This must be in a BEGIN in order for the 'use' to be conditional
    if ($OS_win) {
        eval "use Win32::SerialPort 0.19";
    }
    else {
        eval "use Device::SerialPort 0.07";
    }
    die "$@\n" if ($@);
} # End BEGIN

use ControlX10::CM11 qw( :FUNC 2.06 );
use strict;

my $DUMMY = 0;	# set true to run without a CM11
my $serial_port; 

  # takes port name as command line parameter
if ($OS_win) {
    $port = shift @ARGV || "COM1";
    $serial_port = Win32::SerialPort->new ($port,1);
}
else {
    $port = shift @ARGV || "/dev/ttyS0";
    $serial_port = Device::SerialPort->new ($port,1);
}
die "Can't open serial port $port: $^E\n" unless ($serial_port);

######################### End of black magic.

$serial_port->error_msg(1);	# use built-in error messages
$serial_port->user_msg(0);
$serial_port->databits(8);
$serial_port->baudrate(4800);
$serial_port->parity("none");
$serial_port->stopbits(1);
$serial_port->dtr_active(1);
$serial_port->handshake("none");
$serial_port->write_settings || die "Could not set up port\n";

######################### End of port initialization.

my $reps = 30;
my $block = 0;
my $a2_state = 'AK';	# OFF
my $a2_new = 'AK';	# OFF

while ($reps-- > 0) {
    print ".";
    if (read_cm11($serial_port, $block)) {
        my $datain = receive_cm11($serial_port);
        if (defined $datain) {
            print "\nReceived $datain\n";
	    $a2_new = 'AJ' if ($datain =~ /A1AJ/);
	    $a2_new = 'AK' if ($datain =~ /A1AK/);
	}
    }
    $a2_new = 'AJ' if ($DUMMY && $reps == 15);

        # A2 follows detected changes in A1
    if ($a2_state ne $a2_new) {
	if ($a2_new eq 'AK') {
            print "\nSending A2 OFF\n";
            $a2_new = $a2_state = 'AK';
        }
        else {
            print "\nSending A2 ON\n";
            $a2_new = $a2_state = 'AJ';
        }
        send_cm11($serial_port, 'A2') unless $DUMMY;
        send_cm11($serial_port, $a2_state) unless $DUMMY;
    }
}
print "\n";

$serial_port->close || die "\nclose problem with $port\n";
undef $serial_port;

__END__
