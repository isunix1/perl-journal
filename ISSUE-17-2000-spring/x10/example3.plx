#!/usr/bin/perl -w

use vars qw($OS_win $port %config_parms);
$main::config_parms{debug} = "";	# used inside CM17.pm

############## We start with some cross-platform black magic

BEGIN {
    $| = 1;
    $OS_win = ($^O eq "MSWin32") ? 1 : 0;

        # This must be in a BEGIN in order for the 'use' to be conditional
    if ($OS_win) {
        eval "use Win32::SerialPort 0.19";
    }
    else {
        eval "use Device::SerialPort 0.07";
    }
    die "$@\n" if ($@);
} # End BEGIN

use ControlX10::CM17 qw( send_cm17 0.06 );
use strict;

my $serial_port; 

  # takes port name as command line parameter
if ($OS_win) {
    $port = shift @ARGV || "COM1";
    $serial_port = Win32::SerialPort->new ($port,1);
}
else {
    $port = shift @ARGV || "/dev/ttyS0";
    $serial_port = Device::SerialPort->new ($port,1);
    print "\n=== Bypassing ioctls ===\n\n" unless $serial_port->can_ioctl;
}
die "Can't open serial port $port: $^E\n" unless ($serial_port);

$serial_port->handshake("none");
  # CM17 does not care about other parameters unless pass-through port used

######################### End of black magic.

print "Turning address A1 ON\n";
send_cm17($serial_port, 'A1J');
print "Turning address A2 ON\n";
send_cm17($serial_port, 'A2J');
print "Turning address A3 ON\n";
send_cm17($serial_port, 'A3J');
print "Turning address A4 ON\n";
send_cm17($serial_port, 'A4J');
print "Turning address A5 ON\n";
send_cm17($serial_port, 'A5J');

print "Turning all house A OFF\n";
send_cm17($serial_port, 'AP');

$serial_port->close || die "\nclose problem with $port\n";

__END__
