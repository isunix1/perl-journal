From rjk@linguist.dartmouth.edu Sun Feb 20 22:57:09 2000
Date: Sun, 20 Feb 2000 22:52:26 -0500
From: Ronald J Kimball <rjk@linguist.dartmouth.edu>
To: contest@tpj.com
Subject: poetry: sonnet.pl
Mime-Version: 1.0
Content-Type: text/plain; charset=us-ascii
X-Mailer: Mutt 0.95.4i

#!perl -w

# sonnet.pl
# Ronald J Kimball, rjk@linguist.dartmouth.edu
# February 20, 2000

use strict;

use vars qw/$VERSION/;
$VERSION = '0.1';

my $VERBOSE = 0;

# data file format:
# word<tab>stress<tab>pronunciation
# stress is a sequence of 0s, 1s, and 2s, one per syllable
#   0: no stress; 1: primary stress; 2: secondary stress
# pronounciation is based on IPA, minus stress marks and slashes
#   and with two-character vowel sounds remapped to one character
#   example: chipmunk  10  tSIpm@Nk
my $datafile = 'mywords.dat';


# begin!

open(DATAF, $datafile) or die "Can't open $datafile: $!\n";

# list of regexes to match seven line-ending words
my @stress = ('\t[12]\t|\t[02][12]\t|\t[012]{0,3}[12][02][12]\t') x 7;


# choose seven words for the first halves of each rhyming pair
my @first = choose_entries(\@stress);

print join("\n", @first), "\n\n"
  if $VERBOSE;


# choose seven words to rhyme with the first seven words
my @second = find_rhymes(\@first, \@stress);

# check for non-rhyming words
{
  my $no_rhyme;
  for (my $i=0; $i<=$#second; ++$i) {
    if (not defined $second[$i]) {
      warn "Poet's block!  No rhyme for $first[$i]\n";
      $no_rhyme = 1;
    }
  }
  exit 1 if $no_rhyme;
}

print join("\n", @second), "\n\n"
  if $VERBOSE;


# order the words for the rhyming scheme ABAB CDCD EFEF GG
my @ends = (@first[0,1], @second[0,1],
            @first[2,3], @second[2,3],
            @first[4,5], @second[4,5],
            $first[6], $second[6]);

# complete the lines of the sonnet
my @sonnet = fill_lines(@ends);

# capitalize the first word in each line
# extract just the words from the full entries
@sonnet = map {
                $_->[0] = ucfirst $_->[0];
                [ map { (split /\t/, $_, 2)[0] } @{$_} ]
              } @sonnet;

# indent the last two lines
foreach (@sonnet[-2, -1]) {
  unshift @{$_}, ' ';
}

# print the sonnet
foreach (@sonnet) {
  print "@{$_}\n";
}
print "\n";


# done!
exit;


# choose_entries
# choose entries at random from the data file
# 0 arguments:
#   chooses one word, returns a scalar
# 1 argument:
#   argument is a ref to an array of regexes
#   chooses one word per regex, returns a list
# >1 arguments:
#   each argument is a ref to an array of regexes
#   chooses one word per regex, returns a list of lists
sub choose_entries {
  my @stress = @_;

  # seek to beginning of data file
  seek(DATAF, 0, 0) or die "Can't seek in $datafile: $!\n";

  if (@stress) {
    # with arguments, choose one entry for each

    my @entry;
    my @count;

    # generate a loop to choose the entries
    # avoid recompiling the regexes
    my $eval = <<'    EOT';
      while (<DATAF>) {
    EOT


    # generate one if statement per regex
    for (my $i=0; $i<=$#stress; ++$i) {

      for (my $j=0; $j<=$#{$stress[$i]}; ++$j) {
        $eval .= <<"        EOT";
        if (\$_ =~ /\$stress[$i][$j]/o) {
          \$count[$i][$j]++;
          if (rand(\$count[$i][$j]) < 1) {
            chomp(\$entry[$i][$j] = \$_);
          }
        }
        EOT
      }

    }

    $eval .= <<'    EOT';
      }
    EOT

    # do it!
    eval $eval;
    die $@ if $@;

    if (@stress == 1) {
      # flatten if only one list of words
      return @{$entry[0]};
    } else {
      return @entry;
    }

  } else {
    # without arguments, just choose one entry

    my $entry;
    my $count;

    while (<DATAF>) {
      $count++;
      if (rand($count) < 1) {
        chomp($entry = $_);
      }
    }

    return $entry;

  }
} # choose_entries


# find_rhymes
# choose rhyming entries at random from the data file
# first argument is a ref to an array of entries
# second optional argument is a ref to an array of regexes
# chooses one word for each entry/regex pair, returns a list
# (rhymes are determined only by the last syllable of each word)
sub find_rhymes {
  my @entry = @{ $_[0] };
  my @stress;
  my @count;
  my @match;

  if ($_[1]) {
    @stress = @{ $_[1] };
  }

  # extract the last syllable from each word
  my @rhyme = map quotemeta, last_syllable(@entry);

  seek(DATAF, 0, 0) or die "Can't seek in $datafile: $!\n";

  my $eval = <<'  EOT';
  while (<DATAF>) {
    chomp;  
  EOT

  # generate one if statement per entry
  for (my $i=0; $i<=$#entry; ++$i) {

    # skip if unable to find last syllable
    if (not defined $entry[$i]) {
      $match[$i] = undef;
      next;
    }

    $eval .= <<"    EOT";
    if (\$_ ne \$entry[$i] and /\$rhyme[$i]\$/o) {
    EOT

    if (defined $stress[$i]) {
      $eval .= <<"      EOT";
      next unless /\$stress[$i]/o;
      EOT
    }

    $eval .= <<"    EOT";
      \$count[$i]++;
      if (not int rand \$count[$i]) {
        \$match[$i] = \$_;
      }
    }
    EOT
  }

  $eval .= <<'  EOT';
  }
  EOT

  # do it!
  eval $eval;
  die $@ if $@;

  return @match;
} # find_rhymes


BEGIN {

# regexes to match iambic words from 1 to 5 syllables
my @stress_re = (
  # regexes to match words beginning with an unstressed syllable
  [
    qw/
      0
      \t[02]\t
      \t[02][12]\t
      \t[02][12][02]\t
      \t(?:[02][12][02][12]|0[012]0[012])\t
      \t(?:[02][12][02][12][02]|0[012]0[012]0)\t
    /
  ],
  # regexes to match words beginning with a stressed syllable
  [
    qw/
      0
      \t[12]\t
      \t[12][02]\t
      \t(?:[12][02][12]|[012]0[012])\t
      \t(?:[12][02][12][02]|[012]0[012]0)\t
      \t(?:[12][02][12][02][12]|[012]0[012]0[012])\t
    /
  ]
);

# fill_lines
# choose entries at random to fill each line to 10 syllables
# each argument is an entry which ends a line
# chooses entries to fill each line, returns a list of lists
# includes original entries in return value
sub fill_lines {
  my(@entries) = @_;

  my @searches;

  my $entry;
  foreach $entry (@entries) {

    my($word, $syllables) = split /\t/, $entry;

    # syllable position to fill to
    my $max = 10 - length $syllables;

    # current syllable position
    my $pos = 1;

    my @search;

    while ($pos <= $max) {

      # choose number of syllables in next word
      # distribution: 11 222 33 4 5
      my $syl = int(rand 3) + int(rand 3);
      $syl ||= 5;

      if ($syl + $pos > $max) {
        $syl = $max - $pos + 1;
      }

      # save appropriate regex for syllable count, position
      # (odd: unstressed first; even: stressed first)
      push @search, $stress_re[not $pos & 1][$syl];

      $pos += $syl;
    }

    # save regexes for current line
    push @searches, [ @search ];

  }

  # choose all the entries in one pass
  my @lines = choose_entries(@searches);

  # include original entries at the ends of the lines
  for (my $i=0; $i<=$#lines; ++$i) {
    print join("\n", @{$lines[$i]}), "\n\n"
      if $VERBOSE;

    push @{$lines[$i]}, $entries[$i];
  }

  return @lines;
} # fill_lines

}


BEGIN {

# vowels: IPA, with two-character vowel sounds remapped to one:
# eI -> e, aI -> !, Oi -> ~, AU -> ^, oU -> o, [@] -> [, (@) -> (
my @vowels = qw/& @ A e - E i I ! ~ ^ O o u U y Y [ (/;

# regex to match the last syllable in a word
# one vowel sound followed by any number of non-vowels
my $syll_re = '[' . join('', map quotemeta, @vowels) . ']' .
              '[^' . join('', map quotemeta, @vowels) . ']*$';

# last_syllable
# extract the last syllable from the pronunciation of a word
# each argument is an entry from the data file
# returns a list
# dies if the last syllable cannot be determined
sub last_syllable {
  my(@entry) = @_;
  my @syll;

  foreach (@entry) {
    if (/($syll_re)/o) {
      push @syll, $1;
    } else {
      die "Can't match last syllable in '$_'\n";
      # push @syll, undef;
    }
  }
  return @syll;
} # last_syllable

}

__END__

Ronald J Kimball
rjk@linguist.dartmouth.edu

sonnet.pl 0.1 generates sonnets, inasmuch as the poems are 14 lines long,
in iambic pentameter, and match the rhyming scheme ABAB CDCD EFEF GG.
Any apparent meaning in the generated poems is purely coincidental.

**NOTE** Use of this program requires the associated file mywords.dat.
[See URL below.]  That file was generated from the Moby Pronunciator
database using a modified version of Sean Burke's convert_mpron.pl.  My
conversion script, my_convert.pl, skips words with embedded spaces, words
with capital letters, and words which don't fit an iambic pattern.  Also,
two-character vowel sounds are remapped to one character.

The short version of mywords.dat is a further result of running join with
the ENABLE wordlist, culling out all words which are not in both lists.
The resulting mywords.dat contains 56415 entries and is 1182K in size.

Download this version of mywords.dat in gzip format (405K) at:

<http://linguist.dartmouth.edu/~rjk/mywords.dat.gz>


Because of the use of indented here-doc terminators (i.e. <<'  EOT'),
spaces should not be converted to tabs when editing this script.


Additional Resources:

Moby Project:
<http://www.dcs.shef.ac.uk/research/ilash/Moby/>

Sean Burke's Moby Pronunciator resources:
<http://www.netadventure.net/~sburke/bounce.cgi/mpron/>

My modified Moby Pronunciator conversion script:
<http://linguist.dartmouth.edu/~rjk/my_convert.pl>

ENABLE word list:
<http://personal.riverusers.com/~thegrendel/software.html>


