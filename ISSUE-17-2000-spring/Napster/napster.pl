#!/usr/bin/perl

use strict;
use IO::File;
use MP3::Napster;
use vars qw(@SEARCH $COUNTER);
    
use constant LINK_SPEED => LINK_DSL;
use constant DOWNLOAD_PATH => './songs';
use constant PLAYER => 'mpg123 -';
    
$| = 1;
    
my $nap = MP3::Napster->new || die "Couldn't connect: ",MP3::Napster->error,"\n";
    
END {
  abort();               # abort any pending downloads
  print "[ logging out, wait... ]\n";
  $nap->disconnect if defined $nap;
}
    
# set up the download directory
mkdir DOWNLOAD_PATH, 0666 
  or die "Couldn't make directory ",DOWNLOAD_PATH,": $!\n" 
  unless -d DOWNLOAD_PATH;
$nap->download_dir(DOWNLOAD_PATH);
    
setup_callbacks($nap);
    
exit 0 unless login();
print "\n";
    
while (<>) {
  chomp;
  if (m!^/!) { # A command, since it begins with a slash
      my ($command,$args) = m!^/(\w+)\s*(.*)!;
      $command = lc $command;
      $nap->channels, next      if $command eq 'channels';
      join_channel($args), next if $command eq 'join';
      search($args), next       if $command eq 'search';
      download($args), next     if $command eq 'download';
      status($args), next       if $command eq 'status';
      play($args), next         if $command eq 'play';
      users($args), next        if $command eq 'users';
      whois($args), next        if $command eq 'whois';
      ping($args), next         if $command eq 'ping';
      abort($args), next        if $command eq 'abort';
      exit(0), next             if $command eq 'quit';
      help();
      next;
  }
  speak($_);
}
    
sub login {
    print "login: ";
    chomp (my $login = <>);
    print "password: ";
    system "stty -echo </dev/tty" unless $ENV{EMACS};
    chomp (my $pass = <>);
    system "stty echo </dev/tty" unless $ENV{EMACS};
    print "\n";
    if (my $email = $nap->login($login, $pass, LINK_SPEED)) {
        print "[ Logged in with email $email ]\n";
        return 1;
    } else {
        print "[ Couldn't log in: ", $nap->error, " ]\n";
        return undef;
    }
}
    
sub join_channel {
    my $chan = shift;
    if (my $users = $nap->join_channel($chan)) {
        print "[ Joining $chan, $users users present ]\n";
    } else {
      print "[ Join unsuccessful: ",$nap->error," ]\n";
    }
}
    
sub users {
    unless ($nap->channel) {
      print "[ no current channel ]\n";
      return;
    }
    foreach ($nap->users) {
      printf "\t[ %-20s sharing %4d files on a %-9s line ]\n", 
             $_, $_->sharing, $_->link;
    }
}
    
sub speak {
    $nap->public_message(shift) || 
      print "[ ERROR: no channel selected ]\n";
}
    
sub search {
    my $args = shift;
    undef @SEARCH;
    $COUNTER = 0;
    print "[ searching... ]\n";
    @SEARCH = $nap->search($args);
    print "[ search done. ", scalar @SEARCH, " songs found ]\n";
}
    
sub download {
    my $args  = shift;
    my $fh    = shift;
    my (@num) = $args =~ /(\d+)/g;
    unless (@num) {
        print "[ usage: download <song_no> <song_no> <song_no>.... ]\n";
        return;
    }
    foreach (@num) {
        my $index = $_-1;
        my $song = $SEARCH[$index];
        unless ($song) {
            print "[ $_: No such song identified on last search ]\n";
            return;
        }
        if (my $d = $song->download($fh)) {
            $d->interval(200000);  # set reporting interval
            print "[ $song: starting download ]\n";
        } else {
            print "[ $song: ",$nap->error," ]\n";
        }
    }
}
    
sub play {
    my $args = shift;
    my $fh = IO::File->new('|' . PLAYER);
    print "[ Couldn't open player ".PLAYER.": $! ] \n" unless $fh;
    download($args,$fh);
}
    
# print download status
sub status {
    unless (my @downloads = $nap->downloads) {
        print "[ no downloads in progress ]\n";
    } else {
        for my $d (@downloads) {
            my $song = $d->song;
            my $status = $d->status;
            print "[ $song: $status, ", $d->bytes, " of ", 
                  $d->expected_size, " bytes ]\n";
        }
    }
}
    
# abort
sub abort {
    my $args = shift;
    $args ||= '.';  # by default, abort 'em all
    for my $d ($nap->downloads) {
        my $song = $d->song;
        next unless $song =~ /$args/;
        print "[ $song: aborting ]\n";
       $d->done(1);
    }
}
    
sub whois {
    my $args = shift;
    foreach (split /\s+/,$args) {
        if (my $user = $nap->whois($_)) {
            my $profile = $user->profile;
            $profile =~ s/^/\t/gm;
            print $profile,"\n\n";
        } else {
            print "[ $_: ",$nap->error," ]\n";
        }
    }
}
    
sub ping {
    my $args = shift;
    foreach (split /\s+/,$args) {
        print " [ $_ ",$nap->ping($_) ? 'is pingable' 
              : 'is NOT pingable', " ]\n";
    }
}
    
sub help {
    print "[ COMMANDS: /channels /join /users /whois /search /download /status /play /abort /ping /quit ]\n";
}
    
############ callbacks #################
# 
sub setup_callbacks {
    my $nap = shift;
      
    my $user_speaks = sub { 
      my ($nap,$message) = @_;
      my ($channel,$nickname,$mess) = $message =~/^(\S+) (\S+) (.*)/;
      print "$nickname: $mess\n";
    };
    
    my $user_joins = sub {
      my ($nap,$message) = @_;
      my ($channel, $nickname, $sharing, $link_type) = 
         $message =~/^(\S+) (\S+) (\S+) (\S+)/;
      print "\t==> $nickname joins $channel: sharing $sharing files on a $LINK{$link_type} line <==\n";
    };
      
    my $user_exits = sub {
      my ($nap,$message) = @_;
      my ($channel,$nickname) = $message =~/^(\S+) (\S+)/;
      print "\t==> $nickname has left $channel <==\n";
    };
        
    my $list_channel = sub {
      my ($nap,$channel) = @_;
      printf "[ %-15s %-40s %3d users ]\n", $channel, $channel->topic,
             $channel->users;
    };
      
    my $channel_topic = sub {
      my ($nap,$message) = @_;
      my ($channel,$banner) = $message =~ /^(\S+) (.*)/;
      print "[ \U$channel\E: $banner ]\n";
    };
      
    my $search = sub {
      my ($nap,$song) = @_;
      (my $link = $song->link) =~ s/^LINK_//;
      printf "%3d. %-18s %-3dkbps %-3.1fM  %-8s %-50s\n",
             ++$COUNTER, $song->owner, $song->bitrate, 
             $song->size/1E6, , $link, $song->name;
  };
 
    my $stats =  sub { 
      my ($users, $files, $gigs) = split /\s+/, $_[1];
      print "\t** SERVER STATS: $files files, $users users, ($gigs gigs) **\n";
    };
    
    my $transfer_progress = sub { 
      my ($nap, $download) = @_;
      my $song = $download->song;
      print "\t[ $song: ", $download->bytes, " bytes ]\n";
    };
      
    my $transfer_done = sub { 
      my ($nap, $download) = @_;
      my $song = $download->song;
      print "\t[ $song done: ", $download->status, " ]\n";
      if ($download->status ne 'download complete' && $download->local_path) {
          print "\t[ $song incomplete: unlinking file ]\n";
          unlink $download->local_path;
      }
    };
      
    $nap->callback(PUBLIC_MESSAGE_RECVD, $user_speaks);
    $nap->callback(USER_JOINS,           $user_joins);
    $nap->callback(USER_DEPARTS,         $user_exits);
    $nap->callback(CHANNEL_ENTRY,        $list_channel);
    $nap->callback(CHANNEL_TOPIC,        $channel_topic);
    $nap->callback(SEARCH_RESPONSE,      $search);
    $nap->callback(SERVER_STATS,         $stats);
    $nap->callback(TRANSFER_IN_PROGRESS, $transfer_progress);
    $nap->callback(TRANSFER_DONE,        $transfer_done);
    $nap->callback(MOTD,                 sub { print $_[1], "\n"; } );
}



