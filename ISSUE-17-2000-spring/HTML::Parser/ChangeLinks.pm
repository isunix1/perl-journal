#!/usr/bin/perl -w -i.bak

use strict;

package ChangeLinks;
use base "HTML::Parser";

sub start {
    my ($self, $tag, $attr, $attrseq, $origtext) = @_;

    # we're only interested in changing <A ...> tags
    unless ($tag =~ /^a$/) {
        print $origtext;
        return;
    }

    if (defined $attr->{'href'}) {
        $attr->{'href'} =~ s[foo\.bar\.com][www\.bar\.com/foo];
    }

    print "<A ";
    # print each attribute of the <A ...> tag
    foreach my $i (@$attrseq) {
        print $i, qq(="$attr->{$i}" );
    }
    print ">";
}

sub text {
    my ($self, $text) = @_;
    print $text;
}

sub comment {
    my ($self, $comment) = @_;
    print "<!--", $comment, "-->";
}

sub end {
    my ($self, $tag, $origtext) = @_;
    print $origtext;
}

my $p = new ChangeLinks;
while (<>) {
    $p->parse($_);
}
$p->eof;
