#!/usr/bin/perl -w

use strict;

# define the subclass
package IdentityParse;
use base "HTML::Parser";

sub text {
    my ($self, $text) = @_;
    # just print out the original text
    print $text;
}

sub comment {
    my ($self, $comment) = @_;
    # just print out the original text with comment marker
    print "<!--", $comment, "-->";
}

sub start {
    my ($self, $tag, $attr, $attrseq, $origtext) = @_;
    # just print out the original text, which includes brackets
    print $origtext;
}

sub end {
    my ($self, $tag, $origtext) = @_;
    # just print out the original text, which includes brackets
    print $origtext;
}

my $p = new IdentityParse;
$p->parse_file("index.html");

