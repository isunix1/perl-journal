#!/usr/bin/perl -w

use strict;

package GetSummary;
use base "HTML::Parser";

my $meta_contents;
my $h1 =    "";
my $title = "";

# set state flags
my $h1_flag    = 0;
my $title_flag = 0;

sub start {
    my ($self, $tag, $attr, $attrseq, $origtext) = @_;

    if ($tag =~ /^meta$/i && $attr->{'name'} =~ /^description$/i) {
        # set if we find <META NAME="DESCRIPTION" CONTENT="...">
        $meta_contents = $attr->{'content'};
    } elsif ($tag =~ /^h1$/i && ! $h1) {
        # set state if we find <H1> or <TITLE>
        $h1_flag = 1;
    } elsif ($tag =~ /^title$/i && ! $title) {
        $title_flag = 1;
    }
}

sub text {
    my ($self, $text) = @_;
    # If we're in <H1>...</H1> or <TITLE>...</TITLE>, save text    
    if ($h1_flag)    { $h1    .= $text; } 
    if ($title_flag) { $title .= $text; }
}

sub end {
    my($self, $tag, $origtext) = @_;

    # reset appropriate flag if we see </H1> or </TITLE>
    if ($tag =~ /^h1$/i)    { $h1_flag = 0; }
    if ($tag =~ /^title$/i) { $h1_flag = 0; }
}
 
my $p = new GetSummary;
while (<>) {
    $p->parse($_);
}
$p->eof;

print "Summary information: ", $meta_contents || $h1 || $title ||
    "No summary information found.", "\n";
