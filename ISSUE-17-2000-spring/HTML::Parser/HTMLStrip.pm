#!/usr/bin/perl -w

use strict;

package HTMLStrip;
use base "HTML::Parser";

sub text {
    my ($self, $text) = @_;
    print $text;
}

my $p = new HTMLStrip;
# parse line-by-line, rather than the whole file at once
while (<>) {
    $p->parse($_);
}
# flush and parse remaining unparsed HTML
$p->eof;
