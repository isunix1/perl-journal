   package Window;
   
   my $_id = 1; 
   sub new { bless { _id => $_id++ }, $_[0] }
   
   sub receive_event {
       my ($self, $event, $mode) = @_; 
       if ($event->isa(Event) && $mode->isa(OffMode)) 
         { print "No window operations available in OffMode\n" } 
       else 
         { print "Window $self->{_id} can't handle a ",  ref($event),
                 " event in ", ref($mode), " mode\n" } 
   }
   
   package ModalWindow; @ISA = qw( Window );
   
   sub receive_event {
       my ($self, $event, $mode) = @_; 
       if ($event->isa(AcceptEvent)) { 
         if ($mode->isa(OffMode)) 
           { print "Modal window $self->{_id} can't accept in OffMode!\n" }
         else 
           { print "Modal window $self->{_id} accepts!\n" } 
       } 
       elsif ($event->isa(ReshapeEvent)) 
         { print "Modal windows can't handle reshape events\n" } 
       else 
         { $self->SUPER::receive_event($event,$mode) } 
   }
   
   package MovableWindow; @ISA = qw( Window );
    
   sub receive_event {
       my ($self, $event, $mode) = @_; 
       if ($event->isa(MoveEvent) && $mode->isa(OnMode)) 
         { print "Moving window $self->{_id}!\n" } 
       else 
         { $self->SUPER::receive_event($event,$mode) } 
   }
   
   package ResizableWindow; @ISA = qw( MovableWindow );
    
   sub receive_event {
       my ($self, $event, $mode) = @_; 
       if ($event->isa(MoveAndResizeEvent) && $mode->isa(OnMode)) 
         { print "Moving and resizing window $self->{_id}!\n" } 
       elsif ($event->isa(ResizeEvent) && $mode->isa(OnMode)) 
         { print "Resizing window $self->{_id}!\n" } 
       else 
         { $self->SUPER::receive_event($event,$mode) } 
   }

