   package Window;
   
   my $_id = 1;
   sub new { bless { _id => $_id++ }, $_[0] }
   
   use Class::Multimethods;
   
   multimethod receive_event => ("Window", "Event", "Mode")
       => sub { print "Window $_[0]->{_id} can't handle a ",
                ref($_[1]), " event in ", ref($_[2]), " mode\n" };
   
   multimethod receive_event => ("Window", "Event", "OffMode")
       => sub { print "No window operations available in OffMode\n" };
   
   multimethod receive_event => ("ModalWindow", "ReshapeEvent", "Mode")
       => sub { print "Modal windows can't handle reshape events\n" };
   
   multimethod receive_event => ("ModalWindow", "AcceptEvent", "Mode")
       => sub { print "Modal window $_[0]->{_id} accepts!\n" };
   
   multimethod receive_event => ("ModalWindow", "AcceptEvent", "OffMode")
       => sub { print "Modal window $_[0]->{_id} can't accept in OffMode!\n" };
   
   multimethod receive_event => ("MovableWindow", "MoveEvent", "OnMode")
       => sub { print "Moving window $_[0]->{_id}!\n" };
   
   multimethod receive_event => ("ResizableWindow", "ResizeEvent", "OnMode")
       => sub { print "Resizing window $_[0]->{_id}!\n" };
   
   multimethod receive_event => ("ResizableWindow", "MoveAndResizeEvent", "OnMode")
       => sub { print "Moving and resizing window $_[0]->{_id}!\n" };
   
   package ModalWindow;     @ISA = qw( Window );
   package MovableWindow;   @ISA = qw( Window );
   package ResizableWindow; @ISA = qw( MovableWindow );

