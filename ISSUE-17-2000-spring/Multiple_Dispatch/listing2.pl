   package Window;
   
   my $_id = 1;
   sub new { bless { _id => $_id++ }, $_[0] }
   
   my %table;
   
   sub init {
       my ($param1,$param2,$param3,$handler) = @_;
       foreach my $p1 ( @$param1 ) {
           foreach my $p2 ( @$param2) {
               foreach my $p3 ( @$param3) {
                   $table{$p1}{$p2}{$p3} = $handler;
               }
           }
       }
   }
   
   my $windows  = [qw(Window ModalWindow MovableWindow ResizableWindow )];
   my $events   = [qw(Event ReshapeEvent AcceptEvent
                      MoveEvent ResizeEvent MoveAndResizeEvent )];
   my $modes    = [qw(Mode OnMode OffMode ModalMode )];
   
   init $windows, $events, $modes                                       #case 0
       => sub { print "Window $_[0]->{_id} can't handle a ",
                      ref($_[1]), " event in ", ref($_[2]), " mode\n" };
   
   init $windows, $events, [qw(OffMode)]                                #case 1
       => sub { print "No window operations available in OffMode\n" };
   
   init [qw(ModalWindow)],                                              #case 2
        [qw(ReshapeEvent ResizeEvent MoveEvent MoveAndResizeEvent)],
        $modes
       => sub { print "Modal windows can't handle reshape events\n" };
   
   init [qw(ModalWindow)], [qw(AcceptEvent)], $modes                    #case 3
       => sub { print "Modal window $_[0]->{_id} accepts!\n" };
   
   init [qw(ModalWindow)], [qw(AcceptEvent)], [qw(OffMode)]             #case 4
       => sub { print "Modal window $_[0]->{_id} can't accept in OffMode!\n" };
   
   init [qw(MovableWindow ResizableWindow)],                            #case 5
        [qw(MoveEvent MoveAndResizeEvent)],
        [qw(OnMode)]
       => sub { print "Moving window $_[0]->{_id}!\n" };
   
   init [qw(ResizableWindow)], [qw(ResizeEvent)], [qw(OnMode)]          #case 6
       => sub { print "Resizing window $_[0]->{_id}!\n" };
   
   init [qw(ResizableWindow)], [qw(MoveAndResizeEvent)], [qw(OnMode)]   #case 7
       => sub { print "Moving and resizing window $_[0]->{_id}!\n" };
   
   sub receive_event {
       my ($type1, $type2, $type3) = map {ref} @_;
       my $handler = $table{$type1}{$type2}{$type3};
       die "No suitable handler found" unless $handler;
       $handler->(@_);
   }
   
   package ModalWindow;     @ISA = qw( Window );
   package MovableWindow;   @ISA = qw( Window );
   package ResizableWindow; @ISA = qw( MovableWindow );

