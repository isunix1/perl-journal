#
# use getatomname or internatom depending on the value looked up, but
# cache both

package XATOM;
use Tie::Hash;

sub TIEHASH {
    my ($name, $d) = @_;
    my $self={}; bless $self; 
    $self->{"display"} = $d;
    $self->{"cache"} = {};
    return $self;
}

sub FETCH {
    my $self = shift;
    my $key = shift;
    my $r = $self->{"cache"}->{$key};
    if (! defined($r)) {
	if ($key =~ /^\d*$/) {
	    $r = xlib::x_getatomname($self->{"display"},$key);
	} else {
	    $r = xlib::x_internatom($self->{"display"},
				    $X::defines{"xTrue"},$key);
	}

	$self->{"cache"}->{$key} = $r;
	$self->{"cache"}->{$r} = $key;
    }
    return $r;
}
sub EXISTS {
    my $self = shift;
    my $key = shift;
    return exists $self->{"cache"}->{$key};
}
sub STORE {
    my $self = shift;
    my ($key, $value) = @_;	# value irrelevant
    my $a = xlib::x_internatom($self->{"display"},$X::defines{"xFalse"}, $key);
    $self->{"cache"}->{$a} = $key;
    return $self->{"cache"}->{$key} = $a;
}
sub DELETE {
    my $self = shift;
    my ($key, $value) = @_;
    # delete $self->{"refs"}->{$key};
    # can't actually delete something...
    -1;
}
sub FIRSTKEY {
    my $self = shift;
    my $a = scalar keys %{$self->{"cache"}};
    return each %{$self->{"cache"}};
}
sub NEXTKEY {
    my $self = shift;
    my $lastkey = shift;
    return each %{$self->{"cache"}};
}

sub DESTROY {
    my $self = shift;
}
1;
