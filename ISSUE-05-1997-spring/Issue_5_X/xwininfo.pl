#!/usr/bin/perl 

# get libraries 
require "./xbase.pl"; 
require "./xatomhash.pl";

# open the display 
$xopen = &xlib::x_open_display($ENV{"DISPLAY"}); 
die $xlib::status if not defined $xopen; 

# map atoms to properties 
tie %xatom, XATOM, $xopen;

# print all atoms for window $w on display $d 
sub printatoms { 
my ($d, $w) = @_; 
my @atoms = xlib::x_listproperties($d,$w); 
for $i (@atoms) { 	
my $n = $xatom{$i}; 	
my @x = xlib::x_getproperty($d,
$X::defines{"xFalse"}, $w, $i, 			
$X::defines{"AnyPropertyType"}, 0,255); 
	my $ptn = ($x[2]==0)?"*none*":($xatom{$x[2]}); 
	printf "0x%x (%s)[$ptn]<%s>", $i, $n, $x[1]; 
} 
print "\n"; 
}

# query display $d window $w for children; 
# print info with indentation $n 
sub query { 
my ($d,$w,$n)=@_; 
my @lf = &xlib::x_querytree($d, $w); 
shift @lf;			 # lose the rep, we just want the kids
my $i; 
for $i (@lf) { 
	printf("%s%x", " " x $n, $i); 
	printatoms($d,$i); 	
query($d,$i,$n+1); 
} 
} 

# handle the top level directly 
print "root: ";
printatoms($xopen,xlib::defaultroot($xopen)); 

# now recursively handle the children 
query($xopen,undef,0); 

# we're done. 
xlib::x_closedisplay($xopen); 
