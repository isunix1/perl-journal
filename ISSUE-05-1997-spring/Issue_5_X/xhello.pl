#!/usr/bin/perl

require "./xbase.pl";

$xopen = &xlib::x_open_display($ENV{"DISPLAY"});
die $xlib::status if not defined $xopen;

$fnt = xlib::x_openfont($xopen, "fixed");
$win = xlib::x_create_window($xopen, 8,
			     xlib::defaultroot($xopen),
			     100, 200, 300, 400, 5,
			     $X::defines{"InputOutput"},
			     $X::defines{"CopyFromParent"},
			     $X::defines{"CWBackPixel"}
			     | $X::defines{"CWEventMask"},
			     $xlib::root_white,
			     $X::defines{"ButtonPressMask"}
			     |$X::defines{"ButtonReleaseMask"}
			     |$X::defines{"ExposureMask"}
			     |$X::defines{"KeyPressMask"}
			     );
$gc = xlib::x_create_gc($xopen, $win,
			$X::defines{"GCForeground"}
			| $X::defines{"GCBackground"}
			| $X::defines{"GCFont"},
			$xlib::root_black, $xlib::root_white, $fnt);
#printf "fnt %x win %x gc %x\n", $fnt,$win,$gc;
#sleep 1;
$st = xlib::x_clear_area($xopen, $win, 0, 0,0,0,0);
$xlib::handler{"Expose"} = sub {
  xlib::x_imagetext8($xopen, $win, $gc, 50, 75, "hello world");
};
$xlib::handler{"KeyPress"} = sub {
    my $rep = xlib::xlibconvert("keyButtonPointer", $xopen->{"readqueue"});
    xlib::debugxlib($rep);
};

xlib::x_mapwindow($xopen, $win);
while(defined($st = xlib::handle_event($xopen))) { # assume error for now?
    print "event: $st\n";
}
xlib::x_closedisplay($xopen);
