#!perl

require "./xbase.pl";

$xopen = &xlib::x_open_display($ENV{"DISPLAY"});
die $xlib::status if not defined $xopen;

print &xlib::x_beep($xopen,25),"\n";
xlib::x_closedisplay($xopen);
