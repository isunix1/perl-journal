#!perl

require "./xbase.pl";

$xopen = &xlib::x_open_display($ENV{"DISPLAY"});
die $xlib::status if not defined $xopen;

$xlib::debugpkt = 1;
@lf = &xlib::x_listfonts($xopen,shift);
print "fontnames: ", join("\n    name: ",@lf), "\n";

close $xopen->{"XFD"};
