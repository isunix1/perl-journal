package X;
%xpackof = (
"Atom", "L",
"BOOL", "C",
"BYTE", "C",
"BYTE bpad", "x",
"BYTE pad", "x",
"BYTE pad1", "x",
"BYTE pad2", "x",
"BYTE pad3", "x",
"CARD16", "S",
"CARD16 pad", "x2",
"CARD16 pad1", "x2",
"CARD16 pad2", "x2",
"CARD32", "L",
"CARD32 pad", "x4",
"CARD32 pad00", "x4",
"CARD32 pad1", "x4",
"CARD32 pad2", "x4",
"CARD32 pad3", "x4",
"CARD32 pad4", "x4",
"CARD32 pad5", "x4",
"CARD32 pad6", "x4",
"CARD32 pad7", "x4",
"CARD8", "C",
"CARD8 pad", "x",
"CARD8 pad1", "x",
"Colormap", "L",
"Cursor", "L",
"Drawable", "L",
"Font", "L",
"GContext", "L",
"GetFontPath", "CCS",
"GetInputFocus", "CCS",
"GetKeyboardControl", "CCS",
"GetModifierMapping", "CCS",
"GetPointerControl", "CCS",
"GetPointerMapping", "CCS",
"GetScreenSaver", "CCS",
"GrabServer", "CCS",
"INT16", "S",
"INT32", "L",
"INT8", "C",
"KEYCODE", "C",
"KeyButMask", "S",
"KeyCode", "C",
"KeySym", "L",
"ListExtensions", "CCS",
"Mask", "L",
"NoOperation", "CCS",
"Pixmap", "L",
"QueryKeymap", "CCS",
"Time", "L",
"UngrabServer", "CCS",
"VisualID", "L",
"Window", "L",
"XID", "L",
"XWDFileHeader", "LLLLLLLLLLLLLLLLLLLLLLLLL",
"b", "LC20",
"char", "C",
"circulate", "CCSLLLCxxx",
"clientMessage", "x4L",
"colormap", "CCSLLCCxx",
"configureNotify", "CCSLLLSSSSSCx",
"configureRequest", "CCSLLLSSSSSSx4",
"createNotify", "CCSLLSSSSSCx",
"destroyNotify", "CCSLL",
"enterLeave", "CCSLLLLSSSSSCC",
"expose", "CCSLSSSSSx2",
"focus", "CCSLCxxx",
"graphicsExposure", "CCSLSSSSSSCxxx",
"gravity", "CCSLLSSx4x4x4x4",
"int", "L",
"keyButtonPointer", "CCSLLLLSSSSSCx",
"l", "LLLLLL",
"long", "L",
"mapNotify", "CCSLLCxxx",
"mapRequest", "CCSLL",
"mappingNotify", "CCSCCCx",
"noExposure", "CCSLSCx",
"property", "CCSLLLCxx2",
"reparent", "CCSLLLSSCxxx",
"resizeRequest", "CCSLSS",
"s", "LSSSSSSSSSS",
"selectionClear", "CCSLLL",
"selectionNotify", "CCSLLLLL",
"selectionRequest", "CCSLLLLLL",
"u", "CCS",
"unmapNotify", "CCSLLCxxx",
"visibility", "CCSLCxxx",
"xAllocColorCellsReply", "CxSLSSx4x4x4x4x4",
"xAllocColorCellsReq", "CCSLSS",
"xAllocColorPlanesReply", "CxSLSx2LLLx4x4",
"xAllocColorPlanesReq", "CCSLSSSS",
"xAllocColorReply", "CxSLSSSx2Lx4x4x4",
"xAllocColorReq", "CxSLSSSx2",
"xAllocNamedColorReply", "CxSLLSSSSSSx4x4",
"xAllocNamedColorReq", "CxSLSxx",
"xAllowEventsReq", "CCSL",
"xArc", "SSSSSS",
"xBellReq", "CCS",
"xChangeActivePointerGrabReq", "CxSLLSx2",
"xChangeGCReq", "CxSLL",
"xChangeHostsReq", "CCSCxS",
"xChangeKeyboardControlReq", "CxSL",
"xChangeKeyboardMappingReq", "CCSCCx2",
"xChangeModeReq", "CCS",
"xChangePointerControlReq", "CxSSSSCC",
"xChangePropertyReq", "CCSLLLCx3L",
"xChangeSaveSetReq", "CCSL",
"xChangeWindowAttributesReq", "CxSLL",
"xCharInfo", "SSSSSS",
"xCirculateWindowReq", "CCSL",
"xClearAreaReq", "CCSLSSSS",
"xCloseFontReq", "CxSL",
"xColorItem", "LSSSCx",
"xConfigureWindowReq", "CxSLSx2",
"xConnClientPrefix", "CxSSSSx2",
"xConnSetup", "LLLLSSCCCCCCCCx4",
"xConnSetupPrefix", "CCSSS",
"xConvertSelectionReq", "CxSLLLLL",
"xCopyAreaReq", "CxSLLLSSSSSS",
"xCopyColormapAndFreeReq", "CxSLL",
"xCopyGCReq", "CxSLLL",
"xCopyPlaneReq", "CxSLLLSSSSSSL",
"xCreateColormapReq", "CCSLLL",
"xCreateCursorReq", "CxSLLLSSSSSSSS",
"xCreateGCReq", "CxSLLL",
"xCreateGlyphCursorReq", "CxSLLLSSSSSSSS",
"xCreatePixmapReq", "CCSLLSS",
"xCreateWindowReq", "CCSLLSSSSSSLL",
"xDeletePropertyReq", "CxSLL",
"xDepth", "CxSx4",
"xDestroySubwindowsReq", "CxSL",
"xDestroyWindowReq", "CxSL",
"xError", "CCSLSCxx4x4x4x4x4",
"xEvent", "C32",
"xFillPolyReq", "CxSLLCCx2",
"xFontProp", "LL",
"xForceScreenSaverReq", "CCS",
"xFreeColormapReq", "CxSL",
"xFreeColorsReq", "CxSLL",
"xFreeCursorReq", "CxSL",
"xFreeGCReq", "CxSL",
"xFreePixmapReq", "CxSL",
"xGenericReply", "CCSLLLLLLL",
"xGetAtomNameReply", "CxSLSx2x4x4x4x4x4",
"xGetAtomNameReq", "CxSL",
"xGetFontPathReply", "CxSLSx2x4x4x4x4x4",
"xGetGeometryReply", "CCSLLSSSSSx2x4x4",
"xGetGeometryReq", "CxSL",
"xGetImageReply", "CCSLLx4x4x4x4x4",
"xGetImageReq", "CCSLSSSSL",
"xGetInputFocusReply", "CCSLLx4x4x4x4x4",
"xGetKeyboardControlReply", "CCSLLCCSSx2C32",
"xGetKeyboardMappingReply", "CCSLx4x4x4x4x4x4",
"xGetKeyboardMappingReq", "CxSCCx2",
"xGetModifierMappingReply", "CCSLx4x4x4x4x4x4",
"xGetMotionEventsReply", "CxSLLx4x4x4x4x4",
"xGetMotionEventsReq", "CxSLLL",
"xGetPointerControlReply", "CxSLSSSx2x4x4x4x4",
"xGetPointerMappingReply", "CCSLx4x4x4x4x4x4",
"xGetPropertyReply", "CCSLLLLx4x4x4",
"xGetPropertyReq", "CCSLLLLL",
"xGetScreenSaverReply", "CxSLSSCCx2x4x4x4x4",
"xGetSelectionOwnerReply", "CxSLLx4x4x4x4x4",
"xGetSelectionOwnerReq", "CxSL",
"xGetWindowAttributesReply", "CCSLLSCCLLCCCCLLLSx2",
"xGetWindowAttributesReq", "CxSL",
"xGrabButtonReq", "CCSLSCCLLCxS",
"xGrabKeyReq", "CCSLSCCCxxx",
"xGrabKeyboardReply", "CCSLx4x4x4x4x4x4",
"xGrabKeyboardReq", "CCSLLCCx2",
"xGrabPointerReply", "CCSLx4x4x4x4x4x4",
"xGrabPointerReq", "CCSLSCCLLL",
"xHostEntry", "CxS",
"xImageText16Req", "CCSLLSS",
"xImageText8Req", "CCSLLSS",
"xImageTextReq", "CCSLLSS",
"xInstallColormapReq", "CxSL",
"xInternAtomReply", "CxSLLx4x4x4x4x4",
"xInternAtomReq", "CCSSx2",
"xKeymapEvent", "CC31",
"xKillClientReq", "CxSL",
"xListExtensionsReply", "CCSLx4x4x4x4x4x4",
"xListFontsReply", "CxSLSx2x4x4x4x4x4",
"xListFontsReq", "CxSSS",
"xListFontsWithInfoReply", "CCSLSSSSSSLSSSSSSLSSSSCCCCSSL",
"xListFontsWithInfoReq", "CxSSS",
"xListHostsReply", "CCSLSx2x4x4x4x4x4",
"xListHostsReq", "CxS",
"xListInstalledColormapsReply", "CxSLSx2x4x4x4x4x4",
"xListInstalledColormapsReq", "CxSL",
"xListPropertiesReply", "CxSLSx2x4x4x4x4x4",
"xListPropertiesReq", "CxSL",
"xLookupColorReply", "CxSLSSSSSSx4x4x4",
"xLookupColorReq", "CxSLSxx",
"xMapSubwindowsReq", "CxSL",
"xMapWindowReq", "CxSL",
"xOpenFontReq", "CxSLSxx",
"xPixmapFormat", "CCCxx4",
"xPoint", "SS",
"xPolyArcReq", "CxSLL",
"xPolyFillArcReq", "CxSLL",
"xPolyFillRectangleReq", "CxSLL",
"xPolyLineReq", "CCSLL",
"xPolyPointReq", "CCSLL",
"xPolyRectangleReq", "CxSLL",
"xPolySegmentReq", "CxSLL",
"xPolyText16Req", "CxSLLSS",
"xPolyText8Req", "CxSLLSS",
"xPolyTextReq", "CxSLLSS",
"xPutImageReq", "CCSLLSSSSCCx2",
"xQueryBestSizeReply", "CxSLSSx4x4x4x4x4",
"xQueryBestSizeReq", "CCSLSS",
"xQueryColorsReply", "CxSLSx2x4x4x4x4x4",
"xQueryColorsReq", "CxSL",
"xQueryExtensionReply", "CxSLCCCCx4x4x4x4x4",
"xQueryExtensionReq", "CxSSxx",
"xQueryFontReply", "CxSLSSSSSSLSSSSSSLSSSSCCCCSSL",
"xQueryFontReq", "CxSL",
"xQueryKeymapReply", "CxSLC32",
"xQueryPointerReply", "CCSLLLSSSSSx2x4",
"xQueryPointerReq", "CxSL",
"xQueryTextExtentsReply", "CCSLSSSSLLLx4",
"xQueryTextExtentsReq", "CCSL",
"xQueryTreeReply", "CxSLLLSx2x4x4x4",
"xQueryTreeReq", "CxSL",
"xRecolorCursorReq", "CxSLSSSSSS",
"xRectangle", "SSSS",
"xReparentWindowReq", "CxSLLSS",
"xReq", "CCS",
"xResourceReq", "CxSL",
"xRotatePropertiesReq", "CxSLSS",
"xSegment", "SSSS",
"xSendEventReq", "CCSLLC32",
"xSetAccessControlReq", "CCS",
"xSetClipRectanglesReq", "CCSLSS",
"xSetCloseDownModeReq", "CCS",
"xSetDashesReq", "CxSLSS",
"xSetFontPathReq", "CxSSxx",
"xSetInputFocusReq", "CCSLL",
"xSetMappingReply", "CCSLx4x4x4x4x4x4",
"xSetModifierMappingReply", "CCSLx4x4x4x4x4x4",
"xSetModifierMappingReq", "CCS",
"xSetPointerMappingReply", "CCSLx4x4x4x4x4x4",
"xSetPointerMappingReq", "CCS",
"xSetScreenSaverReq", "CxSSSCCx2",
"xSetSelectionOwnerReq", "CxSLLL",
"xStoreColorsReq", "CxSL",
"xStoreNamedColorReq", "CCSLLSxx",
"xTextElt", "CC",
"xTimecoord", "LSS",
"xTranslateCoordsReply", "CCSLLSSx4x4x4x4",
"xTranslateCoordsReq", "CxSLLSS",
"xUngrabButtonReq", "CCSLSx2",
"xUngrabKeyReq", "CCSLSx2",
"xUngrabKeyboardReq", "CxSL",
"xUngrabPointerReq", "CxSL",
"xUninstallColormapReq", "CxSL",
"xUnmapSubwindowsReq", "CxSL",
"xUnmapWindowReq", "CxSL",
"xVisualType", "LCCSLLLx4",
"xWarpPointerReq", "CxSLLSSSSSS",
"xWindowRoot", "LLLLLSSSSSSLCCCC",
"xrgb", "SSSx2",
); # xpackof
%xtypeof = (
"Atom", "CARD32", 
"Colormap", "CARD32", 
"Cursor", "CARD32", 
"Drawable", "CARD32", 
"Font", "CARD32", 
"GContext", "CARD32", 
"GetFontPath", "CARD8 reqType:CARD8 data:CARD16 length", 
"GetInputFocus", "CARD8 reqType:CARD8 data:CARD16 length", 
"GetKeyboardControl", "CARD8 reqType:CARD8 data:CARD16 length", 
"GetModifierMapping", "CARD8 reqType:CARD8 data:CARD16 length", 
"GetPointerControl", "CARD8 reqType:CARD8 data:CARD16 length", 
"GetPointerMapping", "CARD8 reqType:CARD8 data:CARD16 length", 
"GetScreenSaver", "CARD8 reqType:CARD8 data:CARD16 length", 
"GrabServer", "CARD8 reqType:CARD8 data:CARD16 length", 
"KEYCODE", "CARD8", 
"KeyButMask", "CARD16", 
"KeyCode", "CARD8", 
"KeySym", "CARD32", 
"ListExtensions", "CARD8 reqType:CARD8 data:CARD16 length", 
"Mask", "CARD32", 
"NoOperation", "CARD8 reqType:CARD8 data:CARD16 length", 
"Pixmap", "CARD32", 
"QueryKeymap", "CARD8 reqType:CARD8 data:CARD16 length", 
"Time", "CARD32", 
"UngrabServer", "CARD8 reqType:CARD8 data:CARD16 length", 
"VisualID", "CARD32", 
"Window", "CARD32", 
"XID", "CARD32", 
"XWDFileHeader", "CARD32 header_size:CARD32 file_version:CARD32 pixmap_format:CARD32 pixmap_depth:CARD32 pixmap_width:CARD32 pixmap_height:CARD32 xoffset:CARD32 byte_order:CARD32 bitmap_unit:CARD32 bitmap_bit_order:CARD32 bitmap_pad:CARD32 bits_per_pixel:CARD32 bytes_per_line:CARD32 visual_class:CARD32 red_mask:CARD32 green_mask:CARD32 blue_mask:CARD32 bits_per_rgb:CARD32 colormap_entries:CARD32 ncolors:CARD32 window_width:CARD32 window_height:INT32 window_x:INT32 window_y:CARD32 window_bdrwidth", 
"b", "Atom type:INT8 bytes 20", 
"circulate", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window event:Window window:Window parent:BYTE place", 
"clientMessage", "Window window", 
"colormap", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window window:Colormap colormap:BOOL new:BYTE state", 
"configureNotify", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window event:Window window:Window aboveSibling:INT16 x:INT16 y:CARD16 width:CARD16 height:CARD16 borderWidth:BOOL override", 
"configureRequest", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window parent:Window window:Window sibling:INT16 x:INT16 y:CARD16 width:CARD16 height:CARD16 borderWidth:CARD16 valueMask", 
"createNotify", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window parent:Window window:INT16 x:INT16 y:CARD16 width:CARD16 height:CARD16 borderWidth:BOOL override", 
"destroyNotify", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window event:Window window", 
"enterLeave", "BYTE type:BYTE detail:CARD16 sequenceNumber:Time time:Window root:Window event:Window child:INT16 rootX:INT16 rootY:INT16 eventX:INT16 eventY:KeyButMask state:BYTE mode:BYTE flags", 
"expose", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window window:CARD16 x:CARD16 y:CARD16 width:CARD16 height:CARD16 count", 
"focus", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window window:BYTE mode", 
"graphicsExposure", "BYTE type:BYTE detail:CARD16 sequenceNumber:Drawable drawable:CARD16 x:CARD16 y:CARD16 width:CARD16 height:CARD16 minorEvent:CARD16 count:BYTE majorEvent", 
"gravity", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window event:Window window:INT16 x:INT16 y", 
"keyButtonPointer", "BYTE type:BYTE detail:CARD16 sequenceNumber:Time time:Window root:Window event:Window child:INT16 rootX:INT16 rootY:INT16 eventX:INT16 eventY:KeyButMask state:BOOL sameScreen", 
"l", "Atom type:INT32 longs0:INT32 longs1:INT32 longs2:INT32 longs3:INT32 longs4", 
"mapNotify", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window event:Window window:BOOL override", 
"mapRequest", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window parent:Window window", 
"mappingNotify", "BYTE type:BYTE detail:CARD16 sequenceNumber:CARD8 request:KeyCode firstKeyCode:CARD8 count", 
"noExposure", "BYTE type:BYTE detail:CARD16 sequenceNumber:Drawable drawable:CARD16 minorEvent:BYTE majorEvent", 
"property", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window window:Atom atom:Time time:BYTE state", 
"reparent", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window event:Window window:Window parent:INT16 x:INT16 y:BOOL override", 
"resizeRequest", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window window:CARD16 width:CARD16 height", 
"s", "Atom type:INT16 shorts0:INT16 shorts1:INT16 shorts2:INT16 shorts3:INT16 shorts4:INT16 shorts5:INT16 shorts6:INT16 shorts7:INT16 shorts8:INT16 shorts9", 
"selectionClear", "BYTE type:BYTE detail:CARD16 sequenceNumber:Time time:Window window:Atom atom", 
"selectionNotify", "BYTE type:BYTE detail:CARD16 sequenceNumber:Time time:Window requestor:Atom selection:Atom target:Atom property", 
"selectionRequest", "BYTE type:BYTE detail:CARD16 sequenceNumber:Time time:Window owner:Window requestor:Atom selection:Atom target:Atom property", 
"u", "BYTE type:BYTE detail:CARD16 sequenceNumber", 
"unmapNotify", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window event:Window window:BOOL fromConfigure", 
"visibility", "BYTE type:BYTE detail:CARD16 sequenceNumber:Window window:CARD8 state", 
"xAllocColorCellsReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 nPixels:CARD16 nMasks", 
"xAllocColorCellsReq", "CARD8 reqType:BOOL contiguous:CARD16 length:Colormap cmap:CARD16 colors:CARD16 planes", 
"xAllocColorPlanesReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 nPixels:CARD32 redMask:CARD32 greenMask:CARD32 blueMask", 
"xAllocColorPlanesReq", "CARD8 reqType:BOOL contiguous:CARD16 length:Colormap cmap:CARD16 colors:CARD16 red:CARD16 green:CARD16 blue", 
"xAllocColorReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 red:CARD16 green:CARD16 blue:CARD32 pixel", 
"xAllocColorReq", "CARD8 reqType:CARD16 length:Colormap cmap:CARD16 red:CARD16 green:CARD16 blue", 
"xAllocNamedColorReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD32 pixel:CARD16 exactRed:CARD16 exactGreen:CARD16 exactBlue:CARD16 screenRed:CARD16 screenGreen:CARD16 screenBlue", 
"xAllocNamedColorReq", "CARD8 reqType:CARD16 length:Colormap cmap:CARD16 nbytes", 
"xAllowEventsReq", "CARD8 reqType:CARD8 mode:CARD16 length:Time time", 
"xArc", "INT16 x:INT16 y:CARD16 width:CARD16 height:INT16 angle1:INT16 angle2", 
"xBellReq", "CARD8 reqType:INT8 percent:CARD16 length", 
"xChangeActivePointerGrabReq", "CARD8 reqType:CARD16 length:Cursor cursor:Time time:CARD16 eventMask", 
"xChangeGCReq", "CARD8 reqType:CARD16 length:GContext gc:CARD32 mask", 
"xChangeHostsReq", "CARD8 reqType:BYTE mode:CARD16 length:CARD8 hostFamily:CARD16 hostLength", 
"xChangeKeyboardControlReq", "CARD8 reqType:CARD16 length:CARD32 mask", 
"xChangeKeyboardMappingReq", "CARD8 reqType:CARD8 keyCodes:CARD16 length:KeyCode firstKeyCode:CARD8 keySymsPerKeyCode", 
"xChangeModeReq", "CARD8 reqType:BYTE mode:CARD16 length", 
"xChangePointerControlReq", "CARD8 reqType:CARD16 length:INT16 accelNum:INT16 accelDenum:INT16 threshold:BOOL doAccel:BOOL doThresh", 
"xChangePropertyReq", "CARD8 reqType:CARD8 mode:CARD16 length:Window window:Atom property:Atom type:CARD8 format:CARD32 nUnits", 
"xChangeSaveSetReq", "CARD8 reqType:BYTE mode:CARD16 length:Window window", 
"xChangeWindowAttributesReq", "CARD8 reqType:CARD16 length:Window window:CARD32 valueMask", 
"xCharInfo", "INT16 leftSideBearing:INT16 rightSideBearing:INT16 characterWidth:INT16 ascent:INT16 descent:CARD16 attributes", 
"xCirculateWindowReq", "CARD8 reqType:CARD8 direction:CARD16 length:Window window", 
"xClearAreaReq", "CARD8 reqType:BOOL exposures:CARD16 length:Window window:INT16 x:INT16 y:CARD16 width:CARD16 height", 
"xCloseFontReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xColorItem", "CARD32 pixel:CARD16 red:CARD16 green:CARD16 blue:CARD8 flags", 
"xConfigureWindowReq", "CARD8 reqType:CARD16 length:Window window:CARD16 mask", 
"xConnClientPrefix", "CARD8 byteOrder:CARD16 majorVersion:CARD16 minorVersion:CARD16 nbytesAuthProto:CARD16 nbytesAuthString", 
"xConnSetup", "CARD32 release:CARD32 ridBase:CARD32 ridMask:CARD32 motionBufferSize:CARD16 nbytesVendor:CARD16 maxRequestSize:CARD8 numRoots:CARD8 numFormats:CARD8 imageByteOrder:CARD8 bitmapBitOrder:CARD8 bitmapScanlineUnit:CARD8 bitmapScanlinePad:KeyCode minKeyCode:KeyCode maxKeyCode", 
"xConnSetupPrefix", "BOOL success:BYTE lengthReason:CARD16 majorVersion:CARD16 minorVersion:CARD16 length", 
"xConvertSelectionReq", "CARD8 reqType:CARD16 length:Window requestor:Atom selection:Atom target:Atom property:Time time", 
"xCopyAreaReq", "CARD8 reqType:CARD16 length:Drawable srcDrawable:Drawable dstDrawable:GContext gc:INT16 srcX:INT16 srcY:INT16 dstX:INT16 dstY:CARD16 width:CARD16 height", 
"xCopyColormapAndFreeReq", "CARD8 reqType:CARD16 length:Colormap mid:Colormap srcCmap", 
"xCopyGCReq", "CARD8 reqType:CARD16 length:GContext srcGC:GContext dstGC:CARD32 mask", 
"xCopyPlaneReq", "CARD8 reqType:CARD16 length:Drawable srcDrawable:Drawable dstDrawable:GContext gc:INT16 srcX:INT16 srcY:INT16 dstX:INT16 dstY:CARD16 width:CARD16 height:CARD32 bitPlane", 
"xCreateColormapReq", "CARD8 reqType:BYTE alloc:CARD16 length:Colormap mid:Window window:VisualID visual", 
"xCreateCursorReq", "CARD8 reqType:CARD16 length:Cursor cid:Pixmap source:Pixmap mask:CARD16 foreRed:CARD16 foreGreen:CARD16 foreBlue:CARD16 backRed:CARD16 backGreen:CARD16 backBlue:CARD16 x:CARD16 y", 
"xCreateGCReq", "CARD8 reqType:CARD16 length:GContext gc:Drawable drawable:CARD32 mask", 
"xCreateGlyphCursorReq", "CARD8 reqType:CARD16 length:Cursor cid:Font source:Font mask:CARD16 sourceChar:CARD16 maskChar:CARD16 foreRed:CARD16 foreGreen:CARD16 foreBlue:CARD16 backRed:CARD16 backGreen:CARD16 backBlue", 
"xCreatePixmapReq", "CARD8 reqType:CARD8 depth:CARD16 length:Pixmap pid:Drawable drawable:CARD16 width:CARD16 height", 
"xCreateWindowReq", "CARD8 reqType:CARD8 depth:CARD16 length:Window wid:Window parent:INT16 x:INT16 y:CARD16 width:CARD16 height:CARD16 borderWidth:CARD16 class:VisualID visual:CARD32 mask", 
"xDeletePropertyReq", "CARD8 reqType:CARD16 length:Window window:Atom property", 
"xDepth", "CARD8 depth:CARD16 nVisuals", 
"xDestroySubwindowsReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xDestroyWindowReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xError", "BYTE type:BYTE errorCode:CARD16 sequenceNumber:CARD32 resourceID:CARD16 minorCode:CARD8 majorCode", 
"xEvent", "BYTE raw_bits 32", 
"xFillPolyReq", "CARD8 reqType:CARD16 length:Drawable drawable:GContext gc:BYTE shape:BYTE coordMode", 
"xFontProp", "Atom name:CARD32 value", 
"xForceScreenSaverReq", "CARD8 reqType:BYTE mode:CARD16 length", 
"xFreeColormapReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xFreeColorsReq", "CARD8 reqType:CARD16 length:Colormap cmap:CARD32 planeMask", 
"xFreeCursorReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xFreeGCReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xFreePixmapReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xGenericReply", "BYTE type:BYTE data1:CARD16 sequenceNumber:CARD32 length:CARD32 data00:CARD32 data01:CARD32 data02:CARD32 data03:CARD32 data04:CARD32 data05", 
"xGetAtomNameReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 nameLength", 
"xGetAtomNameReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xGetFontPathReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 nPaths", 
"xGetGeometryReply", "BYTE type:CARD8 depth:CARD16 sequenceNumber:CARD32 length:Window root:INT16 x:INT16 y:CARD16 width:CARD16 height:CARD16 borderWidth", 
"xGetGeometryReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xGetImageReply", "BYTE type:CARD8 depth:CARD16 sequenceNumber:CARD32 length:VisualID visual", 
"xGetImageReq", "CARD8 reqType:CARD8 format:CARD16 length:Drawable drawable:INT16 x:INT16 y:CARD16 width:CARD16 height:CARD32 planeMask", 
"xGetInputFocusReply", "BYTE type:CARD8 revertTo:CARD16 sequenceNumber:CARD32 length:Window focus", 
"xGetKeyboardControlReply", "BYTE type:BOOL globalAutoRepeat:CARD16 sequenceNumber:CARD32 length:CARD32 ledMask:CARD8 keyClickPercent:CARD8 bellPercent:CARD16 bellPitch:CARD16 bellDuration:BYTE map 32", 
"xGetKeyboardMappingReply", "BYTE type:CARD8 keySymsPerKeyCode:CARD16 sequenceNumber:CARD32 length", 
"xGetKeyboardMappingReq", "CARD8 reqType:CARD16 length:KeyCode firstKeyCode:CARD8 count", 
"xGetModifierMappingReply", "BYTE type:CARD8 numKeyPerModifier:CARD16 sequenceNumber:CARD32 length", 
"xGetMotionEventsReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD32 nEvents", 
"xGetMotionEventsReq", "CARD8 reqType:CARD16 length:Window window:Time start:Time stop", 
"xGetPointerControlReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 accelNumerator:CARD16 accelDenominator:CARD16 threshold", 
"xGetPointerMappingReply", "BYTE type:CARD8 nElts:CARD16 sequenceNumber:CARD32 length", 
"xGetPropertyReply", "BYTE type:CARD8 format:CARD16 sequenceNumber:CARD32 length:Atom propertyType:CARD32 bytesAfter:CARD32 nItems", 
"xGetPropertyReq", "CARD8 reqType:BOOL delete:CARD16 length:Window window:Atom property:Atom type:CARD32 longOffset:CARD32 longLength", 
"xGetScreenSaverReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 timeout:CARD16 interval:BOOL preferBlanking:BOOL allowExposures", 
"xGetSelectionOwnerReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:Window owner", 
"xGetSelectionOwnerReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xGetWindowAttributesReply", "BYTE type:CARD8 backingStore:CARD16 sequenceNumber:CARD32 length:VisualID visualID:CARD16 class:CARD8 bitGravity:CARD8 winGravity:CARD32 backingBitPlanes:CARD32 backingPixel:BOOL saveUnder:BOOL mapInstalled:CARD8 mapState:BOOL override:Colormap colormap:CARD32 allEventMasks:CARD32 yourEventMask:CARD16 doNotPropagateMask", 
"xGetWindowAttributesReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xGrabButtonReq", "CARD8 reqType:BOOL ownerEvents:CARD16 length:Window grabWindow:CARD16 eventMask:BYTE pointerMode:BYTE keyboardMode:Window confineTo:Cursor cursor:CARD8 button:CARD16 modifiers", 
"xGrabKeyReq", "CARD8 reqType:BOOL ownerEvents:CARD16 length:Window grabWindow:CARD16 modifiers:CARD8 key:BYTE pointerMode:BYTE keyboardMode", 
"xGrabKeyboardReply", "BYTE type:BYTE status:CARD16 sequenceNumber:CARD32 length", 
"xGrabKeyboardReq", "CARD8 reqType:BOOL ownerEvents:CARD16 length:Window grabWindow:Time time:BYTE pointerMode:BYTE keyboardMode", 
"xGrabPointerReply", "BYTE type:BYTE status:CARD16 sequenceNumber:CARD32 length", 
"xGrabPointerReq", "CARD8 reqType:BOOL ownerEvents:CARD16 length:Window grabWindow:CARD16 eventMask:BYTE pointerMode:BYTE keyboardMode:Window confineTo:Cursor cursor:Time time", 
"xHostEntry", "CARD8 family:CARD16 length", 
"xImageText16Req", "CARD8 reqType:BYTE nChars:CARD16 length:Drawable drawable:GContext gc:INT16 x:INT16 y", 
"xImageText8Req", "CARD8 reqType:BYTE nChars:CARD16 length:Drawable drawable:GContext gc:INT16 x:INT16 y", 
"xImageTextReq", "CARD8 reqType:BYTE nChars:CARD16 length:Drawable drawable:GContext gc:INT16 x:INT16 y", 
"xInstallColormapReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xInternAtomReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:Atom atom", 
"xInternAtomReq", "CARD8 reqType:BOOL onlyIfExists:CARD16 length:CARD16 nbytes", 
"xKeymapEvent", "BYTE type:BYTE map 31", 
"xKillClientReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xListExtensionsReply", "BYTE type:CARD8 nExtensions:CARD16 sequenceNumber:CARD32 length", 
"xListFontsReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 nFonts", 
"xListFontsReq", "CARD8 reqType:CARD16 length:CARD16 maxNames:CARD16 nbytes", 
"xListFontsWithInfoReply", "BYTE type:CARD8 nameLength:CARD16 sequenceNumber:CARD32 length:xCharInfo minBounds:CARD32 walign1:xCharInfo maxBounds:CARD32 walign2:CARD16 minCharOrByte2:CARD16 maxCharOrByte2:CARD16 defaultChar:CARD16 nFontProps:CARD8 drawDirection:CARD8 minByte1:CARD8 maxByte1:BOOL allCharsExist:INT16 fontAscent:INT16 fontDescent:CARD32 nReplies", 
"xListFontsWithInfoReq", "CARD8 reqType:CARD16 length:CARD16 maxNames:CARD16 nbytes", 
"xListHostsReply", "BYTE type:BOOL enabled:CARD16 sequenceNumber:CARD32 length:CARD16 nHosts", 
"xListHostsReq", "CARD8 reqType:CARD16 length", 
"xListInstalledColormapsReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 nColormaps", 
"xListInstalledColormapsReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xListPropertiesReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 nProperties", 
"xListPropertiesReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xLookupColorReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 exactRed:CARD16 exactGreen:CARD16 exactBlue:CARD16 screenRed:CARD16 screenGreen:CARD16 screenBlue", 
"xLookupColorReq", "CARD8 reqType:CARD16 length:Colormap cmap:CARD16 nbytes", 
"xMapSubwindowsReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xMapWindowReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xOpenFontReq", "CARD8 reqType:CARD16 length:Font fid:CARD16 nbytes", 
"xPixmapFormat", "CARD8 depth:CARD8 bitsPerPixel:CARD8 scanLinePad", 
"xPoint", "INT16 x:INT16 y", 
"xPolyArcReq", "CARD8 reqType:CARD16 length:Drawable drawable:GContext gc", 
"xPolyFillArcReq", "CARD8 reqType:CARD16 length:Drawable drawable:GContext gc", 
"xPolyFillRectangleReq", "CARD8 reqType:CARD16 length:Drawable drawable:GContext gc", 
"xPolyLineReq", "CARD8 reqType:BYTE coordMode:CARD16 length:Drawable drawable:GContext gc", 
"xPolyPointReq", "CARD8 reqType:BYTE coordMode:CARD16 length:Drawable drawable:GContext gc", 
"xPolyRectangleReq", "CARD8 reqType:CARD16 length:Drawable drawable:GContext gc", 
"xPolySegmentReq", "CARD8 reqType:CARD16 length:Drawable drawable:GContext gc", 
"xPolyText16Req", "CARD8 reqType:CARD16 length:Drawable drawable:GContext gc:INT16 x:INT16 y", 
"xPolyText8Req", "CARD8 reqType:CARD16 length:Drawable drawable:GContext gc:INT16 x:INT16 y", 
"xPolyTextReq", "CARD8 reqType:CARD16 length:Drawable drawable:GContext gc:INT16 x:INT16 y", 
"xPutImageReq", "CARD8 reqType:CARD8 format:CARD16 length:Drawable drawable:GContext gc:CARD16 width:CARD16 height:INT16 dstX:INT16 dstY:CARD8 leftPad:CARD8 depth", 
"xQueryBestSizeReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 width:CARD16 height", 
"xQueryBestSizeReq", "CARD8 reqType:CARD8 class:CARD16 length:Drawable drawable:CARD16 width:CARD16 height", 
"xQueryColorsReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:CARD16 nColors", 
"xQueryColorsReq", "CARD8 reqType:CARD16 length:Colormap cmap", 
"xQueryExtensionReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:BOOL present:CARD8 major_opcode:CARD8 first_event:CARD8 first_error", 
"xQueryExtensionReq", "CARD8 reqType:CARD16 length:CARD16 nbytes", 
"xQueryFontReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:xCharInfo minBounds:CARD32 walign1:xCharInfo maxBounds:CARD32 walign2:CARD16 minCharOrByte2:CARD16 maxCharOrByte2:CARD16 defaultChar:CARD16 nFontProps:CARD8 drawDirection:CARD8 minByte1:CARD8 maxByte1:BOOL allCharsExist:INT16 fontAscent:INT16 fontDescent:CARD32 nCharInfos", 
"xQueryFontReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xQueryKeymapReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:BYTE map 32", 
"xQueryPointerReply", "BYTE type:BOOL sameScreen:CARD16 sequenceNumber:CARD32 length:Window root:Window child:INT16 rootX:INT16 rootY:INT16 winX:INT16 winY:CARD16 mask", 
"xQueryPointerReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xQueryTextExtentsReply", "BYTE type:CARD8 drawDirection:CARD16 sequenceNumber:CARD32 length:INT16 fontAscent:INT16 fontDescent:INT16 overallAscent:INT16 overallDescent:INT32 overallWidth:INT32 overallLeft:INT32 overallRight", 
"xQueryTextExtentsReq", "CARD8 reqType:BOOL oddLength:CARD16 length:Font fid", 
"xQueryTreeReply", "BYTE type:CARD16 sequenceNumber:CARD32 length:Window root:Window parent:CARD16 nChildren", 
"xQueryTreeReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xRecolorCursorReq", "CARD8 reqType:CARD16 length:Cursor cursor:CARD16 foreRed:CARD16 foreGreen:CARD16 foreBlue:CARD16 backRed:CARD16 backGreen:CARD16 backBlue", 
"xRectangle", "INT16 x:INT16 y:CARD16 width:CARD16 height", 
"xReparentWindowReq", "CARD8 reqType:CARD16 length:Window window:Window parent:INT16 x:INT16 y", 
"xReq", "CARD8 reqType:CARD8 data:CARD16 length", 
"xResourceReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xRotatePropertiesReq", "CARD8 reqType:CARD16 length:Window window:CARD16 nAtoms:INT16 nPositions", 
"xSegment", "INT16 x1:INT16 y1:INT16 x2:INT16 y2", 
"xSendEventReq", "CARD8 reqType:BOOL propagate:CARD16 length:Window destination:CARD32 eventMask:xEvent event", 
"xSetAccessControlReq", "CARD8 reqType:BYTE mode:CARD16 length", 
"xSetClipRectanglesReq", "CARD8 reqType:BYTE ordering:CARD16 length:GContext gc:INT16 xOrigin:INT16 yOrigin", 
"xSetCloseDownModeReq", "CARD8 reqType:BYTE mode:CARD16 length", 
"xSetDashesReq", "CARD8 reqType:CARD16 length:GContext gc:CARD16 dashOffset:CARD16 nDashes", 
"xSetFontPathReq", "CARD8 reqType:CARD16 length:CARD16 nFonts", 
"xSetInputFocusReq", "CARD8 reqType:CARD8 revertTo:CARD16 length:Window focus:Time time", 
"xSetMappingReply", "BYTE type:CARD8 success:CARD16 sequenceNumber:CARD32 length", 
"xSetModifierMappingReply", "BYTE type:CARD8 success:CARD16 sequenceNumber:CARD32 length", 
"xSetModifierMappingReq", "CARD8 reqType:CARD8 numKeyPerModifier:CARD16 length", 
"xSetPointerMappingReply", "BYTE type:CARD8 success:CARD16 sequenceNumber:CARD32 length", 
"xSetPointerMappingReq", "CARD8 reqType:CARD8 nElts:CARD16 length", 
"xSetScreenSaverReq", "CARD8 reqType:CARD16 length:INT16 timeout:INT16 interval:BYTE preferBlank:BYTE allowExpose", 
"xSetSelectionOwnerReq", "CARD8 reqType:CARD16 length:Window window:Atom selection:Time time", 
"xStoreColorsReq", "CARD8 reqType:CARD16 length:Colormap cmap", 
"xStoreNamedColorReq", "CARD8 reqType:CARD8 flags:CARD16 length:Colormap cmap:CARD32 pixel:CARD16 nbytes", 
"xTextElt", "CARD8 len:INT8 delta", 
"xTimecoord", "CARD32 time:INT16 x:INT16 y", 
"xTranslateCoordsReply", "BYTE type:BOOL sameScreen:CARD16 sequenceNumber:CARD32 length:Window child:INT16 dstX:INT16 dstY", 
"xTranslateCoordsReq", "CARD8 reqType:CARD16 length:Window srcWid:Window dstWid:INT16 srcX:INT16 srcY", 
"xUngrabButtonReq", "CARD8 reqType:CARD8 button:CARD16 length:Window grabWindow:CARD16 modifiers", 
"xUngrabKeyReq", "CARD8 reqType:CARD8 key:CARD16 length:Window grabWindow:CARD16 modifiers", 
"xUngrabKeyboardReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xUngrabPointerReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xUninstallColormapReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xUnmapSubwindowsReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xUnmapWindowReq", "CARD8 reqType:CARD16 length:CARD32 id", 
"xVisualType", "VisualID visualID:CARD8 class:CARD8 bitsPerRGB:CARD16 colormapEntries:CARD32 redMask:CARD32 greenMask:CARD32 blueMask", 
"xWarpPointerReq", "CARD8 reqType:CARD16 length:Window srcWid:Window dstWid:INT16 srcX:INT16 srcY:CARD16 srcWidth:CARD16 srcHeight:INT16 dstX:INT16 dstY", 
"xWindowRoot", "Window windowId:Colormap defaultColormap:CARD32 whitePixel:CARD32 blackPixel:CARD32 currentInputMask:CARD16 pixWidth:CARD16 pixHeight:CARD16 mmWidth:CARD16 mmHeight:CARD16 minInstalledMaps:CARD16 maxInstalledMaps:VisualID rootVisualID:CARD8 backingStore:BOOL saveUnders:CARD8 rootDepth:CARD8 nDepths", 
"xrgb", "CARD16 red:CARD16 green:CARD16 blue", 
); # xtypeof
%xsizeof = (
"Atom", 4,
"Colormap", 4,
"Cursor", 4,
"Drawable", 4,
"Font", 4,
"GContext", 4,
"GetFontPath", 4,
"GetInputFocus", 4,
"GetKeyboardControl", 4,
"GetModifierMapping", 4,
"GetPointerControl", 4,
"GetPointerMapping", 4,
"GetScreenSaver", 4,
"GrabServer", 4,
"KEYCODE", 1,
"KeyButMask", 2,
"KeyCode", 1,
"KeySym", 4,
"ListExtensions", 4,
"Mask", 4,
"NoOperation", 4,
"Pixmap", 4,
"QueryKeymap", 4,
"Time", 4,
"UngrabServer", 4,
"VisualID", 4,
"Window", 4,
"XID", 4,
"XWDFileHeader", 100,
"b", 24,
"circulate", 20,
"clientMessage", 8,
"colormap", 16,
"configureNotify", 28,
"configureRequest", 32,
"createNotify", 24,
"destroyNotify", 12,
"enterLeave", 32,
"expose", 20,
"focus", 12,
"graphicsExposure", 24,
"gravity", 32,
"keyButtonPointer", 32,
"l", 24,
"mapNotify", 16,
"mapRequest", 12,
"mappingNotify", 8,
"noExposure", 12,
"property", 20,
"reparent", 24,
"resizeRequest", 12,
"s", 24,
"selectionClear", 16,
"selectionNotify", 24,
"selectionRequest", 28,
"u", 4,
"unmapNotify", 16,
"visibility", 12,
"xAllocColorCellsReply", 32,
"xAllocColorCellsReq", 12,
"xAllocColorPlanesReply", 32,
"xAllocColorPlanesReq", 16,
"xAllocColorReply", 32,
"xAllocColorReq", 16,
"xAllocNamedColorReply", 32,
"xAllocNamedColorReq", 12,
"xAllowEventsReq", 8,
"xArc", 12,
"xBellReq", 4,
"xChangeActivePointerGrabReq", 16,
"xChangeGCReq", 12,
"xChangeHostsReq", 8,
"xChangeKeyboardControlReq", 8,
"xChangeKeyboardMappingReq", 8,
"xChangeModeReq", 4,
"xChangePointerControlReq", 12,
"xChangePropertyReq", 24,
"xChangeSaveSetReq", 8,
"xChangeWindowAttributesReq", 12,
"xCharInfo", 12,
"xCirculateWindowReq", 8,
"xClearAreaReq", 16,
"xCloseFontReq", 8,
"xColorItem", 12,
"xConfigureWindowReq", 12,
"xConnClientPrefix", 12,
"xConnSetup", 32,
"xConnSetupPrefix", 8,
"xConvertSelectionReq", 24,
"xCopyAreaReq", 28,
"xCopyColormapAndFreeReq", 12,
"xCopyGCReq", 16,
"xCopyPlaneReq", 32,
"xCreateColormapReq", 16,
"xCreateCursorReq", 32,
"xCreateGCReq", 16,
"xCreateGlyphCursorReq", 32,
"xCreatePixmapReq", 16,
"xCreateWindowReq", 32,
"xDeletePropertyReq", 12,
"xDepth", 8,
"xDestroySubwindowsReq", 8,
"xDestroyWindowReq", 8,
"xError", 32,
"xEvent", 32,
"xFillPolyReq", 16,
"xFontProp", 8,
"xForceScreenSaverReq", 4,
"xFreeColormapReq", 8,
"xFreeColorsReq", 12,
"xFreeCursorReq", 8,
"xFreeGCReq", 8,
"xFreePixmapReq", 8,
"xGenericReply", 32,
"xGetAtomNameReply", 32,
"xGetAtomNameReq", 8,
"xGetFontPathReply", 32,
"xGetGeometryReply", 32,
"xGetGeometryReq", 8,
"xGetImageReply", 32,
"xGetImageReq", 20,
"xGetInputFocusReply", 32,
"xGetKeyboardControlReply", 52,
"xGetKeyboardMappingReply", 32,
"xGetKeyboardMappingReq", 8,
"xGetModifierMappingReply", 32,
"xGetMotionEventsReply", 32,
"xGetMotionEventsReq", 16,
"xGetPointerControlReply", 32,
"xGetPointerMappingReply", 32,
"xGetPropertyReply", 32,
"xGetPropertyReq", 24,
"xGetScreenSaverReply", 32,
"xGetSelectionOwnerReply", 32,
"xGetSelectionOwnerReq", 8,
"xGetWindowAttributesReply", 44,
"xGetWindowAttributesReq", 8,
"xGrabButtonReq", 24,
"xGrabKeyReq", 16,
"xGrabKeyboardReply", 32,
"xGrabKeyboardReq", 16,
"xGrabPointerReply", 32,
"xGrabPointerReq", 24,
"xHostEntry", 4,
"xImageText16Req", 16,
"xImageText8Req", 16,
"xImageTextReq", 16,
"xInstallColormapReq", 8,
"xInternAtomReply", 32,
"xInternAtomReq", 8,
"xKeymapEvent", 32,
"xKillClientReq", 8,
"xListExtensionsReply", 32,
"xListFontsReply", 32,
"xListFontsReq", 8,
"xListFontsWithInfoReply", 60,
"xListFontsWithInfoReq", 8,
"xListHostsReply", 32,
"xListHostsReq", 4,
"xListInstalledColormapsReply", 32,
"xListInstalledColormapsReq", 8,
"xListPropertiesReply", 32,
"xListPropertiesReq", 8,
"xLookupColorReply", 32,
"xLookupColorReq", 12,
"xMapSubwindowsReq", 8,
"xMapWindowReq", 8,
"xOpenFontReq", 12,
"xPixmapFormat", 8,
"xPoint", 4,
"xPolyArcReq", 12,
"xPolyFillArcReq", 12,
"xPolyFillRectangleReq", 12,
"xPolyLineReq", 12,
"xPolyPointReq", 12,
"xPolyRectangleReq", 12,
"xPolySegmentReq", 12,
"xPolyText16Req", 16,
"xPolyText8Req", 16,
"xPolyTextReq", 16,
"xPutImageReq", 24,
"xQueryBestSizeReply", 32,
"xQueryBestSizeReq", 12,
"xQueryColorsReply", 32,
"xQueryColorsReq", 8,
"xQueryExtensionReply", 32,
"xQueryExtensionReq", 8,
"xQueryFontReply", 60,
"xQueryFontReq", 8,
"xQueryKeymapReply", 40,
"xQueryPointerReply", 32,
"xQueryPointerReq", 8,
"xQueryTextExtentsReply", 32,
"xQueryTextExtentsReq", 8,
"xQueryTreeReply", 32,
"xQueryTreeReq", 8,
"xRecolorCursorReq", 20,
"xRectangle", 8,
"xReparentWindowReq", 16,
"xReq", 4,
"xResourceReq", 8,
"xRotatePropertiesReq", 12,
"xSegment", 8,
"xSendEventReq", 44,
"xSetAccessControlReq", 4,
"xSetClipRectanglesReq", 12,
"xSetCloseDownModeReq", 4,
"xSetDashesReq", 12,
"xSetFontPathReq", 8,
"xSetInputFocusReq", 12,
"xSetMappingReply", 32,
"xSetModifierMappingReply", 32,
"xSetModifierMappingReq", 4,
"xSetPointerMappingReply", 32,
"xSetPointerMappingReq", 4,
"xSetScreenSaverReq", 12,
"xSetSelectionOwnerReq", 16,
"xStoreColorsReq", 8,
"xStoreNamedColorReq", 16,
"xTextElt", 2,
"xTimecoord", 8,
"xTranslateCoordsReply", 32,
"xTranslateCoordsReq", 16,
"xUngrabButtonReq", 12,
"xUngrabKeyReq", 12,
"xUngrabKeyboardReq", 8,
"xUngrabPointerReq", 8,
"xUninstallColormapReq", 8,
"xUnmapSubwindowsReq", 8,
"xUnmapWindowReq", 8,
"xVisualType", 24,
"xWarpPointerReq", 24,
"xWindowRoot", 40,
"xrgb", 8,
); # xsizeof
%defines = (
"Above", 0,
"AllTemporary", 0,
"AllocAll", 1,
"AllocNone", 0,
"AllowExposures", 1,
"AlreadyGrabbed", 1,
"Always", 2,
"AnyButton", 0,
"AnyKey", 0,
"AnyModifier", 32768,
"AnyPropertyType", 0,
"ArcChord", 0,
"ArcPieSlice", 1,
"AsyncBoth", 6,
"AsyncKeyboard", 3,
"AsyncPointer", 0,
"AutoRepeatModeDefault", 2,
"AutoRepeatModeOff", 0,
"AutoRepeatModeOn", 1,
"BadAccess", 10,
"BadAlloc", 11,
"BadAtom", 5,
"BadColor", 12,
"BadCursor", 6,
"BadDrawable", 9,
"BadFont", 7,
"BadGC", 13,
"BadIDChoice", 14,
"BadImplementation", 17,
"BadLength", 16,
"BadMatch", 8,
"BadName", 15,
"BadPixmap", 4,
"BadRequest", 1,
"BadValue", 2,
"BadWindow", 3,
"Below", 1,
"BottomIf", 3,
"Button1", 1,
"Button1Mask", 256,
"Button1MotionMask", 256,
"Button2", 2,
"Button2Mask", 512,
"Button2MotionMask", 512,
"Button3", 3,
"Button3Mask", 1024,
"Button3MotionMask", 1024,
"Button4", 4,
"Button4Mask", 2048,
"Button4MotionMask", 2048,
"Button5", 5,
"Button5Mask", 4096,
"Button5MotionMask", 4096,
"ButtonMotionMask", 8192,
"ButtonPress", 4,
"ButtonPressMask", 4,
"ButtonRelease", 5,
"ButtonReleaseMask", 8,
"CWBackPixel", 2,
"CWBackPixmap", 1,
"CWBackingPixel", 256,
"CWBackingPlanes", 128,
"CWBackingStore", 64,
"CWBitGravity", 16,
"CWBorderPixel", 8,
"CWBorderPixmap", 4,
"CWBorderWidth", 16,
"CWColormap", 8192,
"CWCursor", 16384,
"CWDontPropagate", 4096,
"CWEventMask", 2048,
"CWHeight", 8,
"CWOverrideRedirect", 512,
"CWSaveUnder", 1024,
"CWSibling", 32,
"CWStackMode", 64,
"CWWidth", 4,
"CWWinGravity", 32,
"CWX", 1,
"CWY", 2,
"CapButt", 1,
"CapNotLast", 0,
"CapProjecting", 3,
"CapRound", 2,
"CenterGravity", 5,
"CirculateNotify", 26,
"CirculateRequest", 27,
"ClientMessage", 33,
"ClipByChildren", 0,
"ColormapChangeMask", 8388608,
"ColormapInstalled", 1,
"ColormapNotify", 32,
"ColormapUninstalled", 0,
"Complex", 0,
"ConfigureNotify", 22,
"ConfigureRequest", 23,
"ControlMapIndex", 2,
"ControlMask", 4,
"Convex", 2,
"CoordModeOrigin", 0,
"CoordModePrevious", 1,
"CopyFromParent", 0,
"CreateNotify", 16,
"CurrentTime", 0,
"CursorShape", 0,
"DefaultBlanking", 2,
"DefaultExposures", 2,
"DestroyAll", 0,
"DestroyNotify", 17,
"DirectColor", 5,
"DisableAccess", 0,
"DisableScreenInterval", 0,
"DisableScreenSaver", 0,
"DoBlue", 4,
"DoGreen", 2,
"DoRed", 1,
"DontAllowExposures", 0,
"DontPreferBlanking", 0,
"EastGravity", 6,
"EnableAccess", 1,
"EnterNotify", 7,
"EnterWindowMask", 16,
"EvenOddRule", 0,
"Expose", 12,
"ExposureMask", 32768,
"FamilyChaos", 2,
"FamilyDECnet", 1,
"FamilyInternet", 0,
"FillOpaqueStippled", 3,
"FillSolid", 0,
"FillStippled", 2,
"FillTiled", 1,
"FirstExtensionError", 128,
"FocusChangeMask", 2097152,
"FocusIn", 9,
"FocusOut", 10,
"FontChange", 255,
"FontLeftToRight", 0,
"FontRightToLeft", 1,
"ForgetGravity", 0,
"GCArcMode", 4194304,
"GCBackground", 8,
"GCCapStyle", 64,
"GCClipMask", 524288,
"GCClipXOrigin", 131072,
"GCClipYOrigin", 262144,
"GCDashList", 2097152,
"GCDashOffset", 1048576,
"GCFillRule", 512,
"GCFillStyle", 256,
"GCFont", 16384,
"GCForeground", 4,
"GCFunction", 1,
"GCGraphicsExposures", 65536,
"GCJoinStyle", 128,
"GCLastBit", 22,
"GCLineStyle", 32,
"GCLineWidth", 16,
"GCPlaneMask", 2,
"GCStipple", 2048,
"GCSubwindowMode", 32768,
"GCTile", 1024,
"GCTileStipXOrigin", 4096,
"GCTileStipYOrigin", 8192,
"GXand", 1,
"GXandInverted", 4,
"GXandReverse", 2,
"GXcopy", 3,
"GXcopyInverted", 12,
"GXequiv", 9,
"GXinvert", 10,
"GXnand", 14,
"GXnor", 8,
"GXor", 7,
"GXorInverted", 13,
"GXorReverse", 11,
"GXset", 15,
"GXxor", 6,
"GrabFrozen", 4,
"GrabInvalidTime", 2,
"GrabModeAsync", 1,
"GrabModeSync", 0,
"GrabNotViewable", 3,
"GrabSuccess", 0,
"GraphicsExpose", 13,
"GravityNotify", 24,
"GrayScale", 1,
"HostDelete", 1,
"HostInsert", 0,
"IncludeInferiors", 1,
"InputFocus", 1,
"InputOnly", 2,
"InputOutput", 1,
"IsUnmapped", 0,
"IsUnviewable", 1,
"IsViewable", 2,
"JoinBevel", 2,
"JoinMiter", 0,
"JoinRound", 1,
"KBAutoRepeatMode", 128,
"KBBellDuration", 8,
"KBBellPercent", 2,
"KBBellPitch", 4,
"KBKey", 64,
"KBKeyClickPercent", 1,
"KBLed", 16,
"KBLedMode", 32,
"KeyPress", 2,
"KeyPressMask", 1,
"KeyRelease", 3,
"KeyReleaseMask", 2,
"KeymapNotify", 11,
"KeymapStateMask", 16384,
"LASTEvent", 35,
"LSBFirst", 0,
"LastExtensionError", 255,
"LeaveNotify", 8,
"LeaveWindowMask", 32,
"LedModeOff", 0,
"LedModeOn", 1,
"LineDoubleDash", 2,
"LineOnOffDash", 1,
"LineSolid", 0,
"LockMapIndex", 1,
"LockMask", 2,
"LowerHighest", 1,
"MSBFirst", 1,
"MapNotify", 19,
"MapRequest", 20,
"MappingBusy", 1,
"MappingFailed", 2,
"MappingKeyboard", 1,
"MappingModifier", 0,
"MappingNotify", 34,
"MappingPointer", 2,
"MappingSuccess", 0,
"Mod1MapIndex", 3,
"Mod1Mask", 8,
"Mod2MapIndex", 4,
"Mod2Mask", 16,
"Mod3MapIndex", 5,
"Mod3Mask", 32,
"Mod4MapIndex", 6,
"Mod4Mask", 64,
"Mod5MapIndex", 7,
"Mod5Mask", 128,
"MotionNotify", 6,
"NoEventMask", 0,
"NoExpose", 14,
"NoSymbol", 0,
"Nonconvex", 1,
"None", 0,
"NorthEastGravity", 3,
"NorthGravity", 2,
"NorthWestGravity", 1,
"NotUseful", 0,
"NotifyAncestor", 0,
"NotifyDetailNone", 7,
"NotifyGrab", 1,
"NotifyHint", 1,
"NotifyInferior", 2,
"NotifyNonlinear", 3,
"NotifyNonlinearVirtual", 4,
"NotifyNormal", 0,
"NotifyPointer", 5,
"NotifyPointerRoot", 6,
"NotifyUngrab", 2,
"NotifyVirtual", 1,
"NotifyWhileGrabbed", 3,
"Opposite", 4,
"OwnerGrabButtonMask", 16777216,
"ParentRelative", 1,
"PlaceOnBottom", 1,
"PlaceOnTop", 0,
"PointerMotionHintMask", 128,
"PointerMotionMask", 64,
"PointerRoot", 1,
"PointerWindow", 0,
"PreferBlanking", 1,
"PropModeAppend", 2,
"PropModePrepend", 1,
"PropModeReplace", 0,
"PropertyChangeMask", 4194304,
"PropertyDelete", 1,
"PropertyNewValue", 0,
"PropertyNotify", 28,
"PseudoColor", 3,
"RaiseLowest", 0,
"ReparentNotify", 21,
"ReplayKeyboard", 5,
"ReplayPointer", 2,
"ResizeRedirectMask", 262144,
"ResizeRequest", 25,
"RetainPermanent", 1,
"RetainTemporary", 2,
"RevertToParent", 2,
"ScreenSaverActive", 1,
"ScreenSaverReset", 0,
"SelectionClear", 29,
"SelectionNotify", 31,
"SelectionRequest", 30,
"SetModeDelete", 1,
"SetModeInsert", 0,
"ShiftMapIndex", 0,
"ShiftMask", 1,
"SouthEastGravity", 9,
"SouthGravity", 8,
"SouthWestGravity", 7,
"StaticColor", 2,
"StaticGravity", 10,
"StaticGray", 0,
"StippleShape", 2,
"StructureNotifyMask", 131072,
"SubstructureNotifyMask", 524288,
"SubstructureRedirectMask", 1048576,
"Success", 0,
"SyncBoth", 7,
"SyncKeyboard", 4,
"SyncPointer", 1,
"TileShape", 1,
"TopIf", 2,
"TrueColor", 4,
"UnmapGravity", 0,
"UnmapNotify", 18,
"Unsorted", 0,
"VisibilityChangeMask", 65536,
"VisibilityFullyObscured", 2,
"VisibilityNotify", 15,
"VisibilityPartiallyObscured", 1,
"VisibilityUnobscured", 0,
"WestGravity", 4,
"WhenMapped", 1,
"WindingRule", 1,
"XC_X_cursor", 0,
"XC_arrow", 2,
"XC_based_arrow_down", 4,
"XC_based_arrow_up", 6,
"XC_boat", 8,
"XC_bogosity", 10,
"XC_bottom_left_corner", 12,
"XC_bottom_right_corner", 14,
"XC_bottom_side", 16,
"XC_bottom_tee", 18,
"XC_box_spiral", 20,
"XC_center_ptr", 22,
"XC_circle", 24,
"XC_clock", 26,
"XC_coffee_mug", 28,
"XC_cross", 30,
"XC_cross_reverse", 32,
"XC_crosshair", 34,
"XC_diamond_cross", 36,
"XC_dot", 38,
"XC_dotbox", 40,
"XC_double_arrow", 42,
"XC_draft_large", 44,
"XC_draft_small", 46,
"XC_draped_box", 48,
"XC_exchange", 50,
"XC_fleur", 52,
"XC_gobbler", 54,
"XC_gumby", 56,
"XC_hand1", 58,
"XC_hand2", 60,
"XC_heart", 62,
"XC_icon", 64,
"XC_iron_cross", 66,
"XC_left_ptr", 68,
"XC_left_side", 70,
"XC_left_tee", 72,
"XC_leftbutton", 74,
"XC_ll_angle", 76,
"XC_lr_angle", 78,
"XC_man", 80,
"XC_middlebutton", 82,
"XC_mouse", 84,
"XC_num_glyphs", 154,
"XC_pencil", 86,
"XC_pirate", 88,
"XC_plus", 90,
"XC_question_arrow", 92,
"XC_right_ptr", 94,
"XC_right_side", 96,
"XC_right_tee", 98,
"XC_rightbutton", 100,
"XC_rtl_logo", 102,
"XC_sailboat", 104,
"XC_sb_down_arrow", 106,
"XC_sb_h_double_arrow", 108,
"XC_sb_left_arrow", 110,
"XC_sb_right_arrow", 112,
"XC_sb_up_arrow", 114,
"XC_sb_v_double_arrow", 116,
"XC_shuttle", 118,
"XC_sizing", 120,
"XC_spider", 122,
"XC_spraycan", 124,
"XC_star", 126,
"XC_target", 128,
"XC_tcross", 130,
"XC_top_left_arrow", 132,
"XC_top_left_corner", 134,
"XC_top_right_corner", 136,
"XC_top_side", 138,
"XC_top_tee", 140,
"XC_trek", 142,
"XC_ul_angle", 144,
"XC_umbrella", 146,
"XC_ur_angle", 148,
"XC_watch", 150,
"XC_xterm", 152,
"XPROTOSTRUCTS_H", 1,
"XPROTO_H", 1,
"XWD_FILE_VERSION", 7,
"XYBitmap", 0,
"XYPixmap", 1,
"X_AllocColor", 84,
"X_AllocColorCells", 86,
"X_AllocColorPlanes", 87,
"X_AllocNamedColor", 85,
"X_AllowEvents", 35,
"X_Bell", 104,
"X_ChangeActivePointerGrab", 30,
"X_ChangeGC", 56,
"X_ChangeHosts", 109,
"X_ChangeKeyboardControl", 102,
"X_ChangeKeyboardMapping", 100,
"X_ChangePointerControl", 105,
"X_ChangeProperty", 18,
"X_ChangeSaveSet", 6,
"X_ChangeWindowAttributes", 2,
"X_CirculateWindow", 13,
"X_ClearArea", 61,
"X_CloseFont", 46,
"X_ConfigureWindow", 12,
"X_ConvertSelection", 24,
"X_CopyArea", 62,
"X_CopyColormapAndFree", 80,
"X_CopyGC", 57,
"X_CopyPlane", 63,
"X_CreateColormap", 78,
"X_CreateCursor", 93,
"X_CreateGC", 55,
"X_CreateGlyphCursor", 94,
"X_CreatePixmap", 53,
"X_CreateWindow", 1,
"X_DeleteProperty", 19,
"X_DestroySubwindows", 5,
"X_DestroyWindow", 4,
"X_Error", 0,
"X_FillPoly", 69,
"X_ForceScreenSaver", 115,
"X_FreeColormap", 79,
"X_FreeColors", 88,
"X_FreeCursor", 95,
"X_FreeGC", 60,
"X_FreePixmap", 54,
"X_GetAtomName", 17,
"X_GetFontPath", 52,
"X_GetGeometry", 14,
"X_GetImage", 73,
"X_GetInputFocus", 43,
"X_GetKeyboardControl", 103,
"X_GetKeyboardMapping", 101,
"X_GetModifierMapping", 119,
"X_GetMotionEvents", 39,
"X_GetPointerControl", 106,
"X_GetPointerMapping", 117,
"X_GetProperty", 20,
"X_GetScreenSaver", 108,
"X_GetSelectionOwner", 23,
"X_GetWindowAttributes", 3,
"X_GrabButton", 28,
"X_GrabKey", 33,
"X_GrabKeyboard", 31,
"X_GrabPointer", 26,
"X_GrabServer", 36,
"X_H", 1,
"X_ImageText16", 77,
"X_ImageText8", 76,
"X_InstallColormap", 81,
"X_InternAtom", 16,
"X_KillClient", 113,
"X_ListExtensions", 99,
"X_ListFonts", 49,
"X_ListFontsWithInfo", 50,
"X_ListHosts", 110,
"X_ListInstalledColormaps", 83,
"X_ListProperties", 21,
"X_LookupColor", 92,
"X_MapSubwindows", 9,
"X_MapWindow", 8,
"X_NoOperation", 127,
"X_OpenFont", 45,
"X_PROTOCOL", 11,
"X_PROTOCOL_REVISION", 0,
"X_PolyArc", 68,
"X_PolyFillArc", 71,
"X_PolyFillRectangle", 70,
"X_PolyLine", 65,
"X_PolyPoint", 64,
"X_PolyRectangle", 67,
"X_PolySegment", 66,
"X_PolyText16", 75,
"X_PolyText8", 74,
"X_PutImage", 72,
"X_QueryBestSize", 97,
"X_QueryColors", 91,
"X_QueryExtension", 98,
"X_QueryFont", 47,
"X_QueryKeymap", 44,
"X_QueryPointer", 38,
"X_QueryTextExtents", 48,
"X_QueryTree", 15,
"X_RecolorCursor", 96,
"X_ReparentWindow", 7,
"X_Reply", 1,
"X_RotateProperties", 114,
"X_SendEvent", 25,
"X_SetAccessControl", 111,
"X_SetClipRectangles", 59,
"X_SetCloseDownMode", 112,
"X_SetDashes", 58,
"X_SetFontPath", 51,
"X_SetInputFocus", 42,
"X_SetModifierMapping", 118,
"X_SetPointerMapping", 116,
"X_SetScreenSaver", 107,
"X_SetSelectionOwner", 22,
"X_StoreColors", 89,
"X_StoreNamedColor", 90,
"X_TCP_PORT", 6000,
"X_TranslateCoords", 40,
"X_UngrabButton", 29,
"X_UngrabKey", 34,
"X_UngrabKeyboard", 32,
"X_UngrabPointer", 27,
"X_UngrabServer", 37,
"X_UninstallColormap", 82,
"X_UnmapSubwindows", 11,
"X_UnmapWindow", 10,
"X_WarpPointer", 41,
"YSorted", 1,
"YXBanded", 3,
"YXSorted", 2,
"ZPixmap", 2,
"xFalse", 0,
"xTrue", 1,
); # defines
sub pad4 { (($_[0]+3)>>2)<<2; }
sub xpad { my $l=pad4($_[0])-$_[0]; "\0" x $l; }
$AllocColor = 84;
sub xAllocColorReq { pack($xpackof{"xAllocColorReq"},$AllocColor,@_);}
sub newxAllocColorReq { my $l=$_[4]; my $x=xAllocColorReq(0,@_).$l; xAllocColorReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$AllocColorCells = 86;
sub xAllocColorCellsReq { pack($xpackof{"xAllocColorCellsReq"},$AllocColorCells,$_[1],$_[0],@_[2..$#_]);}
sub newxAllocColorCellsReq { my $l=$_[4]; my $x=xAllocColorCellsReq(0,@_).$l; xAllocColorCellsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$AllocColorPlanes = 87;
sub xAllocColorPlanesReq { pack($xpackof{"xAllocColorPlanesReq"},$AllocColorPlanes,$_[1],$_[0],@_[2..$#_]);}
sub newxAllocColorPlanesReq { my $l=$_[6]; my $x=xAllocColorPlanesReq(0,@_).$l; xAllocColorPlanesReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$AllocNamedColor = 85;
sub xAllocNamedColorReq { pack($xpackof{"xAllocNamedColorReq"},$AllocNamedColor,@_);}
sub newxAllocNamedColorReq { my $l=$_[2]; my $x=xAllocNamedColorReq(0,@_).$l; xAllocNamedColorReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$AllowEvents = 35;
sub xAllowEventsReq { pack($xpackof{"xAllowEventsReq"},$AllowEvents,$_[1],$_[0],@_[2..$#_]);}
sub newxAllowEventsReq { my $l=$_[2]; my $x=xAllowEventsReq(0,@_).$l; xAllowEventsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$Bell = 104;
sub xBellReq { pack($xpackof{"xBellReq"},$Bell,$_[1],$_[0],@_[2..$#_]);}
sub newxBellReq { my $l=$_[1]; my $x=xBellReq(0,@_).$l; xBellReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ChangeActivePointerGrab = 30;
sub xChangeActivePointerGrabReq { pack($xpackof{"xChangeActivePointerGrabReq"},$ChangeActivePointerGrab,@_);}
sub newxChangeActivePointerGrabReq { my $l=$_[3]; my $x=xChangeActivePointerGrabReq(0,@_).$l; xChangeActivePointerGrabReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ChangeGC = 56;
sub xChangeGCReq { pack($xpackof{"xChangeGCReq"},$ChangeGC,@_);}
sub newxChangeGCReq { my $l=$_[2]; my $x=xChangeGCReq(0,@_).$l; xChangeGCReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ChangeHosts = 109;
sub xChangeHostsReq { pack($xpackof{"xChangeHostsReq"},$ChangeHosts,$_[1],$_[0],@_[2..$#_]);}
sub newxChangeHostsReq { my $l=$_[3]; my $x=xChangeHostsReq(0,@_).$l; xChangeHostsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ChangeKeyboardControl = 102;
sub xChangeKeyboardControlReq { pack($xpackof{"xChangeKeyboardControlReq"},$ChangeKeyboardControl,@_);}
sub newxChangeKeyboardControlReq { my $l=$_[1]; my $x=xChangeKeyboardControlReq(0,@_).$l; xChangeKeyboardControlReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ChangeKeyboardMapping = 100;
sub xChangeKeyboardMappingReq { pack($xpackof{"xChangeKeyboardMappingReq"},$ChangeKeyboardMapping,$_[1],$_[0],@_[2..$#_]);}
sub newxChangeKeyboardMappingReq { my $l=$_[3]; my $x=xChangeKeyboardMappingReq(0,@_).$l; xChangeKeyboardMappingReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ChangePointerControl = 105;
sub xChangePointerControlReq { pack($xpackof{"xChangePointerControlReq"},$ChangePointerControl,@_);}
sub newxChangePointerControlReq { my $l=$_[5]; my $x=xChangePointerControlReq(0,@_).$l; xChangePointerControlReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ChangeProperty = 18;
sub xChangePropertyReq { pack($xpackof{"xChangePropertyReq"},$ChangeProperty,$_[1],$_[0],@_[2..$#_]);}
sub newxChangePropertyReq { my $l=$_[6]; my $x=xChangePropertyReq(0,@_).$l; xChangePropertyReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ChangeSaveSet = 6;
sub xChangeSaveSetReq { pack($xpackof{"xChangeSaveSetReq"},$ChangeSaveSet,$_[1],$_[0],@_[2..$#_]);}
sub newxChangeSaveSetReq { my $l=$_[2]; my $x=xChangeSaveSetReq(0,@_).$l; xChangeSaveSetReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ChangeWindowAttributes = 2;
sub xChangeWindowAttributesReq { pack($xpackof{"xChangeWindowAttributesReq"},$ChangeWindowAttributes,@_);}
sub newxChangeWindowAttributesReq { my $l=$_[2]; my $x=xChangeWindowAttributesReq(0,@_).$l; xChangeWindowAttributesReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CirculateWindow = 13;
sub xCirculateWindowReq { pack($xpackof{"xCirculateWindowReq"},$CirculateWindow,$_[1],$_[0],@_[2..$#_]);}
sub newxCirculateWindowReq { my $l=$_[2]; my $x=xCirculateWindowReq(0,@_).$l; xCirculateWindowReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ClearArea = 61;
sub xClearAreaReq { pack($xpackof{"xClearAreaReq"},$ClearArea,$_[1],$_[0],@_[2..$#_]);}
sub newxClearAreaReq { my $l=$_[6]; my $x=xClearAreaReq(0,@_).$l; xClearAreaReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CloseFont = 46;
sub xCloseFontReq { pack($xpackof{"xCloseFontReq"},$CloseFont,2,@_);}
sub newxCloseFontReq { xCloseFontReq(@_); }
$ConfigureWindow = 12;
sub xConfigureWindowReq { pack($xpackof{"xConfigureWindowReq"},$ConfigureWindow,@_);}
sub newxConfigureWindowReq { my $l=$_[2]; my $x=xConfigureWindowReq(0,@_).$l; xConfigureWindowReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ConvertSelection = 24;
sub xConvertSelectionReq { pack($xpackof{"xConvertSelectionReq"},$ConvertSelection,@_);}
sub newxConvertSelectionReq { my $l=$_[5]; my $x=xConvertSelectionReq(0,@_).$l; xConvertSelectionReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CopyArea = 62;
sub xCopyAreaReq { pack($xpackof{"xCopyAreaReq"},$CopyArea,@_);}
sub newxCopyAreaReq { my $l=$_[9]; my $x=xCopyAreaReq(0,@_).$l; xCopyAreaReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CopyColormapAndFree = 80;
sub xCopyColormapAndFreeReq { pack($xpackof{"xCopyColormapAndFreeReq"},$CopyColormapAndFree,@_);}
sub newxCopyColormapAndFreeReq { my $l=$_[2]; my $x=xCopyColormapAndFreeReq(0,@_).$l; xCopyColormapAndFreeReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CopyGC = 57;
sub xCopyGCReq { pack($xpackof{"xCopyGCReq"},$CopyGC,@_);}
sub newxCopyGCReq { my $l=$_[3]; my $x=xCopyGCReq(0,@_).$l; xCopyGCReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CopyPlane = 63;
sub xCopyPlaneReq { pack($xpackof{"xCopyPlaneReq"},$CopyPlane,@_);}
sub newxCopyPlaneReq { my $l=$_[10]; my $x=xCopyPlaneReq(0,@_).$l; xCopyPlaneReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CreateColormap = 78;
sub xCreateColormapReq { pack($xpackof{"xCreateColormapReq"},$CreateColormap,$_[1],$_[0],@_[2..$#_]);}
sub newxCreateColormapReq { my $l=$_[4]; my $x=xCreateColormapReq(0,@_).$l; xCreateColormapReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CreateCursor = 93;
sub xCreateCursorReq { pack($xpackof{"xCreateCursorReq"},$CreateCursor,@_);}
sub newxCreateCursorReq { my $l=$_[11]; my $x=xCreateCursorReq(0,@_).$l; xCreateCursorReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CreateGC = 55;
sub xCreateGCReq { pack($xpackof{"xCreateGCReq"},$CreateGC,@_);}
sub newxCreateGCReq { my $l=$_[3]; my $x=xCreateGCReq(0,@_).$l; xCreateGCReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CreateGlyphCursor = 94;
sub xCreateGlyphCursorReq { pack($xpackof{"xCreateGlyphCursorReq"},$CreateGlyphCursor,@_);}
sub newxCreateGlyphCursorReq { my $l=$_[11]; my $x=xCreateGlyphCursorReq(0,@_).$l; xCreateGlyphCursorReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CreatePixmap = 53;
sub xCreatePixmapReq { pack($xpackof{"xCreatePixmapReq"},$CreatePixmap,$_[1],$_[0],@_[2..$#_]);}
sub newxCreatePixmapReq { my $l=$_[5]; my $x=xCreatePixmapReq(0,@_).$l; xCreatePixmapReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$CreateWindow = 1;
sub xCreateWindowReq { pack($xpackof{"xCreateWindowReq"},$CreateWindow,$_[1],$_[0],@_[2..$#_]);}
sub newxCreateWindowReq { my $l=$_[11]; my $x=xCreateWindowReq(0,@_).$l; xCreateWindowReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$DeleteProperty = 19;
sub xDeletePropertyReq { pack($xpackof{"xDeletePropertyReq"},$DeleteProperty,@_);}
sub newxDeletePropertyReq { my $l=$_[2]; my $x=xDeletePropertyReq(0,@_).$l; xDeletePropertyReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$DestroySubwindows = 5;
sub xDestroySubwindowsReq { pack($xpackof{"xDestroySubwindowsReq"},$DestroySubwindows,2,@_);}
sub newxDestroySubwindowsReq { xDestroySubwindowsReq(@_); }
$DestroyWindow = 4;
sub xDestroyWindowReq { pack($xpackof{"xDestroyWindowReq"},$DestroyWindow,2,@_);}
sub newxDestroyWindowReq { xDestroyWindowReq(@_); }
$Error = 0;
$FillPoly = 69;
sub xFillPolyReq { pack($xpackof{"xFillPolyReq"},$FillPoly,@_);}
sub newxFillPolyReq { my $l=$_[4]; my $x=xFillPolyReq(0,@_).$l; xFillPolyReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ForceScreenSaver = 115;
sub xForceScreenSaverReq { pack($xpackof{"xForceScreenSaverReq"},$ForceScreenSaver,$_[1],$_[0],@_[2..$#_]);}
sub newxForceScreenSaverReq { my $l=$_[1]; my $x=xForceScreenSaverReq(0,@_).$l; xForceScreenSaverReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$FreeColormap = 79;
sub xFreeColormapReq { pack($xpackof{"xFreeColormapReq"},$FreeColormap,2,@_);}
sub newxFreeColormapReq { xFreeColormapReq(@_); }
$FreeColors = 88;
sub xFreeColorsReq { pack($xpackof{"xFreeColorsReq"},$FreeColors,@_);}
sub newxFreeColorsReq { my $l=$_[2]; my $x=xFreeColorsReq(0,@_).$l; xFreeColorsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$FreeCursor = 95;
sub xFreeCursorReq { pack($xpackof{"xFreeCursorReq"},$FreeCursor,2,@_);}
sub newxFreeCursorReq { xFreeCursorReq(@_); }
$FreeGC = 60;
sub xFreeGCReq { pack($xpackof{"xFreeGCReq"},$FreeGC,2,@_);}
sub newxFreeGCReq { xFreeGCReq(@_); }
$FreePixmap = 54;
sub xFreePixmapReq { pack($xpackof{"xFreePixmapReq"},$FreePixmap,2,@_);}
sub newxFreePixmapReq { xFreePixmapReq(@_); }
$GetAtomName = 17;
sub xGetAtomNameReq { pack($xpackof{"xGetAtomNameReq"},$GetAtomName,2,@_);}
sub newxGetAtomNameReq { xGetAtomNameReq(@_); }
$GetFontPath = 52;
sub xGetFontPathReq { pack($xpackof{"GetFontPath"},$GetFontPath,0,1);}
sub newxGetFontPathReq { xGetFontPathReq(@_); }
$GetGeometry = 14;
sub xGetGeometryReq { pack($xpackof{"xGetGeometryReq"},$GetGeometry,2,@_);}
sub newxGetGeometryReq { xGetGeometryReq(@_); }
$GetImage = 73;
sub xGetImageReq { pack($xpackof{"xGetImageReq"},$GetImage,$_[1],$_[0],@_[2..$#_]);}
sub newxGetImageReq { my $l=$_[7]; my $x=xGetImageReq(0,@_).$l; xGetImageReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$GetInputFocus = 43;
sub xGetInputFocusReq { pack($xpackof{"GetInputFocus"},$GetInputFocus,0,1);}
sub newxGetInputFocusReq { xGetInputFocusReq(@_); }
$GetKeyboardControl = 103;
sub xGetKeyboardControlReq { pack($xpackof{"GetKeyboardControl"},$GetKeyboardControl,0,1);}
sub newxGetKeyboardControlReq { xGetKeyboardControlReq(@_); }
$GetKeyboardMapping = 101;
sub xGetKeyboardMappingReq { pack($xpackof{"xGetKeyboardMappingReq"},$GetKeyboardMapping,@_);}
sub newxGetKeyboardMappingReq { my $l=$_[2]; my $x=xGetKeyboardMappingReq(0,@_).$l; xGetKeyboardMappingReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$GetModifierMapping = 119;
sub xGetModifierMappingReq { pack($xpackof{"GetModifierMapping"},$GetModifierMapping,0,1);}
sub newxGetModifierMappingReq { xGetModifierMappingReq(@_); }
$GetMotionEvents = 39;
sub xGetMotionEventsReq { pack($xpackof{"xGetMotionEventsReq"},$GetMotionEvents,@_);}
sub newxGetMotionEventsReq { my $l=$_[3]; my $x=xGetMotionEventsReq(0,@_).$l; xGetMotionEventsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$GetPointerControl = 106;
sub xGetPointerControlReq { pack($xpackof{"GetPointerControl"},$GetPointerControl,0,1);}
sub newxGetPointerControlReq { xGetPointerControlReq(@_); }
$GetPointerMapping = 117;
sub xGetPointerMappingReq { pack($xpackof{"GetPointerMapping"},$GetPointerMapping,0,1);}
sub newxGetPointerMappingReq { xGetPointerMappingReq(@_); }
$GetProperty = 20;
sub xGetPropertyReq { pack($xpackof{"xGetPropertyReq"},$GetProperty,$_[1],$_[0],@_[2..$#_]);}
sub newxGetPropertyReq { my $l=$_[6]; my $x=xGetPropertyReq(0,@_).$l; xGetPropertyReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$GetScreenSaver = 108;
sub xGetScreenSaverReq { pack($xpackof{"GetScreenSaver"},$GetScreenSaver,0,1);}
sub newxGetScreenSaverReq { xGetScreenSaverReq(@_); }
$GetSelectionOwner = 23;
sub xGetSelectionOwnerReq { pack($xpackof{"xGetSelectionOwnerReq"},$GetSelectionOwner,2,@_);}
sub newxGetSelectionOwnerReq { xGetSelectionOwnerReq(@_); }
$GetWindowAttributes = 3;
sub xGetWindowAttributesReq { pack($xpackof{"xGetWindowAttributesReq"},$GetWindowAttributes,2,@_);}
sub newxGetWindowAttributesReq { xGetWindowAttributesReq(@_); }
$GrabButton = 28;
sub xGrabButtonReq { pack($xpackof{"xGrabButtonReq"},$GrabButton,$_[1],$_[0],@_[2..$#_]);}
sub newxGrabButtonReq { my $l=$_[9]; my $x=xGrabButtonReq(0,@_).$l; xGrabButtonReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$GrabKey = 33;
sub xGrabKeyReq { pack($xpackof{"xGrabKeyReq"},$GrabKey,$_[1],$_[0],@_[2..$#_]);}
sub newxGrabKeyReq { my $l=$_[6]; my $x=xGrabKeyReq(0,@_).$l; xGrabKeyReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$GrabKeyboard = 31;
sub xGrabKeyboardReq { pack($xpackof{"xGrabKeyboardReq"},$GrabKeyboard,$_[1],$_[0],@_[2..$#_]);}
sub newxGrabKeyboardReq { my $l=$_[5]; my $x=xGrabKeyboardReq(0,@_).$l; xGrabKeyboardReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$GrabPointer = 26;
sub xGrabPointerReq { pack($xpackof{"xGrabPointerReq"},$GrabPointer,$_[1],$_[0],@_[2..$#_]);}
sub newxGrabPointerReq { my $l=$_[8]; my $x=xGrabPointerReq(0,@_).$l; xGrabPointerReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$GrabServer = 36;
sub xGrabServerReq { pack($xpackof{"GrabServer"},$GrabServer,0,1);}
sub newxGrabServerReq { xGrabServerReq(@_); }
$H = 1;
$ImageText16 = 77;
sub xImageText16Req { pack($xpackof{"xImageText16Req"},$ImageText16,$_[1],$_[0],@_[2..$#_]);}
sub newxImageText16Req { my $l=$_[5]; my $x=xImageText16Req(0,@_).$l; xImageText16Req(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ImageText8 = 76;
sub xImageText8Req { pack($xpackof{"xImageText8Req"},$ImageText8,$_[1],$_[0],@_[2..$#_]);}
sub newxImageText8Req { my $l=$_[5]; my $x=xImageText8Req(0,@_).$l; xImageText8Req(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$InstallColormap = 81;
sub xInstallColormapReq { pack($xpackof{"xInstallColormapReq"},$InstallColormap,2,@_);}
sub newxInstallColormapReq { xInstallColormapReq(@_); }
$InternAtom = 16;
sub xInternAtomReq { pack($xpackof{"xInternAtomReq"},$InternAtom,$_[1],$_[0],@_[2..$#_]);}
sub newxInternAtomReq { my $l=$_[2]; my $x=xInternAtomReq(0,@_).$l; xInternAtomReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$KillClient = 113;
sub xKillClientReq { pack($xpackof{"xKillClientReq"},$KillClient,2,@_);}
sub newxKillClientReq { xKillClientReq(@_); }
$ListExtensions = 99;
sub xListExtensionsReq { pack($xpackof{"ListExtensions"},$ListExtensions,0,1);}
sub newxListExtensionsReq { xListExtensionsReq(@_); }
$ListFonts = 49;
sub xListFontsReq { pack($xpackof{"xListFontsReq"},$ListFonts,@_);}
sub newxListFontsReq { my $l=$_[2]; my $x=xListFontsReq(0,@_).$l; xListFontsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ListFontsWithInfo = 50;
sub xListFontsWithInfoReq { pack($xpackof{"xListFontsWithInfoReq"},$ListFontsWithInfo,@_);}
sub newxListFontsWithInfoReq { my $l=$_[2]; my $x=xListFontsWithInfoReq(0,@_).$l; xListFontsWithInfoReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ListHosts = 110;
sub xListHostsReq { pack($xpackof{"xListHostsReq"},$ListHosts,@_);}
sub newxListHostsReq { my $l=$_[0]; my $x=xListHostsReq(0,@_).$l; xListHostsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ListInstalledColormaps = 83;
sub xListInstalledColormapsReq { pack($xpackof{"xListInstalledColormapsReq"},$ListInstalledColormaps,2,@_);}
sub newxListInstalledColormapsReq { xListInstalledColormapsReq(@_); }
$ListProperties = 21;
sub xListPropertiesReq { pack($xpackof{"xListPropertiesReq"},$ListProperties,2,@_);}
sub newxListPropertiesReq { xListPropertiesReq(@_); }
$LookupColor = 92;
sub xLookupColorReq { pack($xpackof{"xLookupColorReq"},$LookupColor,@_);}
sub newxLookupColorReq { my $l=$_[2]; my $x=xLookupColorReq(0,@_).$l; xLookupColorReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$MapSubwindows = 9;
sub xMapSubwindowsReq { pack($xpackof{"xMapSubwindowsReq"},$MapSubwindows,2,@_);}
sub newxMapSubwindowsReq { xMapSubwindowsReq(@_); }
$MapWindow = 8;
sub xMapWindowReq { pack($xpackof{"xMapWindowReq"},$MapWindow,2,@_);}
sub newxMapWindowReq { xMapWindowReq(@_); }
$NoOperation = 127;
sub xNoOperationReq { pack($xpackof{"NoOperation"},$NoOperation,0,1);}
sub newxNoOperationReq { xNoOperationReq(@_); }
$OpenFont = 45;
sub xOpenFontReq { pack($xpackof{"xOpenFontReq"},$OpenFont,@_);}
sub newxOpenFontReq { my $l=$_[2]; my $x=xOpenFontReq(0,@_).$l; xOpenFontReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PROTOCOL = 11;
$PROTOCOL_REVISION = 0;
$PolyArc = 68;
sub xPolyArcReq { pack($xpackof{"xPolyArcReq"},$PolyArc,@_);}
sub newxPolyArcReq { my $l=$_[2]; my $x=xPolyArcReq(0,@_).$l; xPolyArcReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PolyFillArc = 71;
sub xPolyFillArcReq { pack($xpackof{"xPolyFillArcReq"},$PolyFillArc,@_);}
sub newxPolyFillArcReq { my $l=$_[2]; my $x=xPolyFillArcReq(0,@_).$l; xPolyFillArcReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PolyFillRectangle = 70;
sub xPolyFillRectangleReq { pack($xpackof{"xPolyFillRectangleReq"},$PolyFillRectangle,@_);}
sub newxPolyFillRectangleReq { my $l=$_[2]; my $x=xPolyFillRectangleReq(0,@_).$l; xPolyFillRectangleReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PolyLine = 65;
sub xPolyLineReq { pack($xpackof{"xPolyLineReq"},$PolyLine,$_[1],$_[0],@_[2..$#_]);}
sub newxPolyLineReq { my $l=$_[3]; my $x=xPolyLineReq(0,@_).$l; xPolyLineReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PolyPoint = 64;
sub xPolyPointReq { pack($xpackof{"xPolyPointReq"},$PolyPoint,$_[1],$_[0],@_[2..$#_]);}
sub newxPolyPointReq { my $l=$_[3]; my $x=xPolyPointReq(0,@_).$l; xPolyPointReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PolyRectangle = 67;
sub xPolyRectangleReq { pack($xpackof{"xPolyRectangleReq"},$PolyRectangle,@_);}
sub newxPolyRectangleReq { my $l=$_[2]; my $x=xPolyRectangleReq(0,@_).$l; xPolyRectangleReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PolySegment = 66;
sub xPolySegmentReq { pack($xpackof{"xPolySegmentReq"},$PolySegment,@_);}
sub newxPolySegmentReq { my $l=$_[2]; my $x=xPolySegmentReq(0,@_).$l; xPolySegmentReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PolyText16 = 75;
sub xPolyText16Req { pack($xpackof{"xPolyText16Req"},$PolyText16,@_);}
sub newxPolyText16Req { my $l=$_[4]; my $x=xPolyText16Req(0,@_).$l; xPolyText16Req(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PolyText8 = 74;
sub xPolyText8Req { pack($xpackof{"xPolyText8Req"},$PolyText8,@_);}
sub newxPolyText8Req { my $l=$_[4]; my $x=xPolyText8Req(0,@_).$l; xPolyText8Req(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$PutImage = 72;
sub xPutImageReq { pack($xpackof{"xPutImageReq"},$PutImage,$_[1],$_[0],@_[2..$#_]);}
sub newxPutImageReq { my $l=$_[9]; my $x=xPutImageReq(0,@_).$l; xPutImageReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$QueryBestSize = 97;
sub xQueryBestSizeReq { pack($xpackof{"xQueryBestSizeReq"},$QueryBestSize,$_[1],$_[0],@_[2..$#_]);}
sub newxQueryBestSizeReq { my $l=$_[4]; my $x=xQueryBestSizeReq(0,@_).$l; xQueryBestSizeReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$QueryColors = 91;
sub xQueryColorsReq { pack($xpackof{"xQueryColorsReq"},$QueryColors,@_);}
sub newxQueryColorsReq { my $l=$_[1]; my $x=xQueryColorsReq(0,@_).$l; xQueryColorsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$QueryExtension = 98;
sub xQueryExtensionReq { pack($xpackof{"xQueryExtensionReq"},$QueryExtension,@_);}
sub newxQueryExtensionReq { my $l=$_[1]; my $x=xQueryExtensionReq(0,@_).$l; xQueryExtensionReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$QueryFont = 47;
sub xQueryFontReq { pack($xpackof{"xQueryFontReq"},$QueryFont,2,@_);}
sub newxQueryFontReq { xQueryFontReq(@_); }
$QueryKeymap = 44;
sub xQueryKeymapReq { pack($xpackof{"QueryKeymap"},$QueryKeymap,0,1);}
sub newxQueryKeymapReq { xQueryKeymapReq(@_); }
$QueryPointer = 38;
sub xQueryPointerReq { pack($xpackof{"xQueryPointerReq"},$QueryPointer,2,@_);}
sub newxQueryPointerReq { xQueryPointerReq(@_); }
$QueryTextExtents = 48;
sub xQueryTextExtentsReq { pack($xpackof{"xQueryTextExtentsReq"},$QueryTextExtents,$_[1],$_[0],@_[2..$#_]);}
sub newxQueryTextExtentsReq { my $l=$_[2]; my $x=xQueryTextExtentsReq(0,@_).$l; xQueryTextExtentsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$QueryTree = 15;
sub xQueryTreeReq { pack($xpackof{"xQueryTreeReq"},$QueryTree,2,@_);}
sub newxQueryTreeReq { xQueryTreeReq(@_); }
$RecolorCursor = 96;
sub xRecolorCursorReq { pack($xpackof{"xRecolorCursorReq"},$RecolorCursor,@_);}
sub newxRecolorCursorReq { my $l=$_[7]; my $x=xRecolorCursorReq(0,@_).$l; xRecolorCursorReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$ReparentWindow = 7;
sub xReparentWindowReq { pack($xpackof{"xReparentWindowReq"},$ReparentWindow,@_);}
sub newxReparentWindowReq { my $l=$_[4]; my $x=xReparentWindowReq(0,@_).$l; xReparentWindowReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$Reply = 1;
$RotateProperties = 114;
sub xRotatePropertiesReq { pack($xpackof{"xRotatePropertiesReq"},$RotateProperties,@_);}
sub newxRotatePropertiesReq { my $l=$_[3]; my $x=xRotatePropertiesReq(0,@_).$l; xRotatePropertiesReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SendEvent = 25;
sub xSendEventReq { pack($xpackof{"xSendEventReq"},$SendEvent,$_[1],$_[0],@_[2..$#_]);}
sub newxSendEventReq { my $l=$_[4]; my $x=xSendEventReq(0,@_).$l; xSendEventReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetAccessControl = 111;
sub xSetAccessControlReq { pack($xpackof{"xSetAccessControlReq"},$SetAccessControl,$_[1],$_[0],@_[2..$#_]);}
sub newxSetAccessControlReq { my $l=$_[1]; my $x=xSetAccessControlReq(0,@_).$l; xSetAccessControlReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetClipRectangles = 59;
sub xSetClipRectanglesReq { pack($xpackof{"xSetClipRectanglesReq"},$SetClipRectangles,$_[1],$_[0],@_[2..$#_]);}
sub newxSetClipRectanglesReq { my $l=$_[4]; my $x=xSetClipRectanglesReq(0,@_).$l; xSetClipRectanglesReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetCloseDownMode = 112;
sub xSetCloseDownModeReq { pack($xpackof{"xSetCloseDownModeReq"},$SetCloseDownMode,$_[1],$_[0],@_[2..$#_]);}
sub newxSetCloseDownModeReq { my $l=$_[1]; my $x=xSetCloseDownModeReq(0,@_).$l; xSetCloseDownModeReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetDashes = 58;
sub xSetDashesReq { pack($xpackof{"xSetDashesReq"},$SetDashes,@_);}
sub newxSetDashesReq { my $l=$_[3]; my $x=xSetDashesReq(0,@_).$l; xSetDashesReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetFontPath = 51;
sub xSetFontPathReq { pack($xpackof{"xSetFontPathReq"},$SetFontPath,@_);}
sub newxSetFontPathReq { my $l=$_[1]; my $x=xSetFontPathReq(0,@_).$l; xSetFontPathReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetInputFocus = 42;
sub xSetInputFocusReq { pack($xpackof{"xSetInputFocusReq"},$SetInputFocus,$_[1],$_[0],@_[2..$#_]);}
sub newxSetInputFocusReq { my $l=$_[3]; my $x=xSetInputFocusReq(0,@_).$l; xSetInputFocusReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetModifierMapping = 118;
sub xSetModifierMappingReq { pack($xpackof{"xSetModifierMappingReq"},$SetModifierMapping,$_[1],$_[0],@_[2..$#_]);}
sub newxSetModifierMappingReq { my $l=$_[1]; my $x=xSetModifierMappingReq(0,@_).$l; xSetModifierMappingReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetPointerMapping = 116;
sub xSetPointerMappingReq { pack($xpackof{"xSetPointerMappingReq"},$SetPointerMapping,$_[1],$_[0],@_[2..$#_]);}
sub newxSetPointerMappingReq { my $l=$_[1]; my $x=xSetPointerMappingReq(0,@_).$l; xSetPointerMappingReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetScreenSaver = 107;
sub xSetScreenSaverReq { pack($xpackof{"xSetScreenSaverReq"},$SetScreenSaver,@_);}
sub newxSetScreenSaverReq { my $l=$_[4]; my $x=xSetScreenSaverReq(0,@_).$l; xSetScreenSaverReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$SetSelectionOwner = 22;
sub xSetSelectionOwnerReq { pack($xpackof{"xSetSelectionOwnerReq"},$SetSelectionOwner,@_);}
sub newxSetSelectionOwnerReq { my $l=$_[3]; my $x=xSetSelectionOwnerReq(0,@_).$l; xSetSelectionOwnerReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$StoreColors = 89;
sub xStoreColorsReq { pack($xpackof{"xStoreColorsReq"},$StoreColors,@_);}
sub newxStoreColorsReq { my $l=$_[1]; my $x=xStoreColorsReq(0,@_).$l; xStoreColorsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$StoreNamedColor = 90;
sub xStoreNamedColorReq { pack($xpackof{"xStoreNamedColorReq"},$StoreNamedColor,$_[1],$_[0],@_[2..$#_]);}
sub newxStoreNamedColorReq { my $l=$_[4]; my $x=xStoreNamedColorReq(0,@_).$l; xStoreNamedColorReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$TCP_PORT = 6000;
$TranslateCoords = 40;
sub xTranslateCoordsReq { pack($xpackof{"xTranslateCoordsReq"},$TranslateCoords,@_);}
sub newxTranslateCoordsReq { my $l=$_[4]; my $x=xTranslateCoordsReq(0,@_).$l; xTranslateCoordsReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$UngrabButton = 29;
sub xUngrabButtonReq { pack($xpackof{"xUngrabButtonReq"},$UngrabButton,$_[1],$_[0],@_[2..$#_]);}
sub newxUngrabButtonReq { my $l=$_[3]; my $x=xUngrabButtonReq(0,@_).$l; xUngrabButtonReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$UngrabKey = 34;
sub xUngrabKeyReq { pack($xpackof{"xUngrabKeyReq"},$UngrabKey,$_[1],$_[0],@_[2..$#_]);}
sub newxUngrabKeyReq { my $l=$_[3]; my $x=xUngrabKeyReq(0,@_).$l; xUngrabKeyReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
$UngrabKeyboard = 32;
sub xUngrabKeyboardReq { pack($xpackof{"xUngrabKeyboardReq"},$UngrabKeyboard,2,@_);}
sub newxUngrabKeyboardReq { xUngrabKeyboardReq(@_); }
$UngrabPointer = 27;
sub xUngrabPointerReq { pack($xpackof{"xUngrabPointerReq"},$UngrabPointer,2,@_);}
sub newxUngrabPointerReq { xUngrabPointerReq(@_); }
$UngrabServer = 37;
sub xUngrabServerReq { pack($xpackof{"UngrabServer"},$UngrabServer,0,1);}
sub newxUngrabServerReq { xUngrabServerReq(@_); }
$UninstallColormap = 82;
sub xUninstallColormapReq { pack($xpackof{"xUninstallColormapReq"},$UninstallColormap,2,@_);}
sub newxUninstallColormapReq { xUninstallColormapReq(@_); }
$UnmapSubwindows = 11;
sub xUnmapSubwindowsReq { pack($xpackof{"xUnmapSubwindowsReq"},$UnmapSubwindows,2,@_);}
sub newxUnmapSubwindowsReq { xUnmapSubwindowsReq(@_); }
$UnmapWindow = 10;
sub xUnmapWindowReq { pack($xpackof{"xUnmapWindowReq"},$UnmapWindow,2,@_);}
sub newxUnmapWindowReq { xUnmapWindowReq(@_); }
$WarpPointer = 41;
sub xWarpPointerReq { pack($xpackof{"xWarpPointerReq"},$WarpPointer,@_);}
sub newxWarpPointerReq { my $l=$_[8]; my $x=xWarpPointerReq(0,@_).$l; xWarpPointerReq(pad4(length($x))/4,@_).$l.xpad(length($x)); }
%xsubs = (
"newxAllocColorCellsReq" => \&newxAllocColorCellsReq,
"newxAllocColorPlanesReq" => \&newxAllocColorPlanesReq,
"newxAllocColorReq" => \&newxAllocColorReq,
"newxAllocNamedColorReq" => \&newxAllocNamedColorReq,
"newxAllowEventsReq" => \&newxAllowEventsReq,
"newxBellReq" => \&newxBellReq,
"newxChangeActivePointerGrabReq" => \&newxChangeActivePointerGrabReq,
"newxChangeGCReq" => \&newxChangeGCReq,
"newxChangeHostsReq" => \&newxChangeHostsReq,
"newxChangeKeyboardControlReq" => \&newxChangeKeyboardControlReq,
"newxChangeKeyboardMappingReq" => \&newxChangeKeyboardMappingReq,
"newxChangePointerControlReq" => \&newxChangePointerControlReq,
"newxChangePropertyReq" => \&newxChangePropertyReq,
"newxChangeSaveSetReq" => \&newxChangeSaveSetReq,
"newxChangeWindowAttributesReq" => \&newxChangeWindowAttributesReq,
"newxCirculateWindowReq" => \&newxCirculateWindowReq,
"newxClearAreaReq" => \&newxClearAreaReq,
"newxCloseFontReq" => \&newxCloseFontReq,
"newxConfigureWindowReq" => \&newxConfigureWindowReq,
"newxConvertSelectionReq" => \&newxConvertSelectionReq,
"newxCopyAreaReq" => \&newxCopyAreaReq,
"newxCopyColormapAndFreeReq" => \&newxCopyColormapAndFreeReq,
"newxCopyGCReq" => \&newxCopyGCReq,
"newxCopyPlaneReq" => \&newxCopyPlaneReq,
"newxCreateColormapReq" => \&newxCreateColormapReq,
"newxCreateCursorReq" => \&newxCreateCursorReq,
"newxCreateGCReq" => \&newxCreateGCReq,
"newxCreateGlyphCursorReq" => \&newxCreateGlyphCursorReq,
"newxCreatePixmapReq" => \&newxCreatePixmapReq,
"newxCreateWindowReq" => \&newxCreateWindowReq,
"newxDeletePropertyReq" => \&newxDeletePropertyReq,
"newxDestroySubwindowsReq" => \&newxDestroySubwindowsReq,
"newxDestroyWindowReq" => \&newxDestroyWindowReq,
"newxFillPolyReq" => \&newxFillPolyReq,
"newxForceScreenSaverReq" => \&newxForceScreenSaverReq,
"newxFreeColormapReq" => \&newxFreeColormapReq,
"newxFreeColorsReq" => \&newxFreeColorsReq,
"newxFreeCursorReq" => \&newxFreeCursorReq,
"newxFreeGCReq" => \&newxFreeGCReq,
"newxFreePixmapReq" => \&newxFreePixmapReq,
"newxGetAtomNameReq" => \&newxGetAtomNameReq,
"newxGetFontPathReq" => \&newxGetFontPathReq,
"newxGetGeometryReq" => \&newxGetGeometryReq,
"newxGetImageReq" => \&newxGetImageReq,
"newxGetInputFocusReq" => \&newxGetInputFocusReq,
"newxGetKeyboardControlReq" => \&newxGetKeyboardControlReq,
"newxGetKeyboardMappingReq" => \&newxGetKeyboardMappingReq,
"newxGetModifierMappingReq" => \&newxGetModifierMappingReq,
"newxGetMotionEventsReq" => \&newxGetMotionEventsReq,
"newxGetPointerControlReq" => \&newxGetPointerControlReq,
"newxGetPointerMappingReq" => \&newxGetPointerMappingReq,
"newxGetPropertyReq" => \&newxGetPropertyReq,
"newxGetScreenSaverReq" => \&newxGetScreenSaverReq,
"newxGetSelectionOwnerReq" => \&newxGetSelectionOwnerReq,
"newxGetWindowAttributesReq" => \&newxGetWindowAttributesReq,
"newxGrabButtonReq" => \&newxGrabButtonReq,
"newxGrabKeyReq" => \&newxGrabKeyReq,
"newxGrabKeyboardReq" => \&newxGrabKeyboardReq,
"newxGrabPointerReq" => \&newxGrabPointerReq,
"newxGrabServerReq" => \&newxGrabServerReq,
"newxImageText16Req" => \&newxImageText16Req,
"newxImageText8Req" => \&newxImageText8Req,
"newxInstallColormapReq" => \&newxInstallColormapReq,
"newxInternAtomReq" => \&newxInternAtomReq,
"newxKillClientReq" => \&newxKillClientReq,
"newxListExtensionsReq" => \&newxListExtensionsReq,
"newxListFontsReq" => \&newxListFontsReq,
"newxListFontsWithInfoReq" => \&newxListFontsWithInfoReq,
"newxListHostsReq" => \&newxListHostsReq,
"newxListInstalledColormapsReq" => \&newxListInstalledColormapsReq,
"newxListPropertiesReq" => \&newxListPropertiesReq,
"newxLookupColorReq" => \&newxLookupColorReq,
"newxMapSubwindowsReq" => \&newxMapSubwindowsReq,
"newxMapWindowReq" => \&newxMapWindowReq,
"newxNoOperationReq" => \&newxNoOperationReq,
"newxOpenFontReq" => \&newxOpenFontReq,
"newxPolyArcReq" => \&newxPolyArcReq,
"newxPolyFillArcReq" => \&newxPolyFillArcReq,
"newxPolyFillRectangleReq" => \&newxPolyFillRectangleReq,
"newxPolyLineReq" => \&newxPolyLineReq,
"newxPolyPointReq" => \&newxPolyPointReq,
"newxPolyRectangleReq" => \&newxPolyRectangleReq,
"newxPolySegmentReq" => \&newxPolySegmentReq,
"newxPolyText16Req" => \&newxPolyText16Req,
"newxPolyText8Req" => \&newxPolyText8Req,
"newxPutImageReq" => \&newxPutImageReq,
"newxQueryBestSizeReq" => \&newxQueryBestSizeReq,
"newxQueryColorsReq" => \&newxQueryColorsReq,
"newxQueryExtensionReq" => \&newxQueryExtensionReq,
"newxQueryFontReq" => \&newxQueryFontReq,
"newxQueryKeymapReq" => \&newxQueryKeymapReq,
"newxQueryPointerReq" => \&newxQueryPointerReq,
"newxQueryTextExtentsReq" => \&newxQueryTextExtentsReq,
"newxQueryTreeReq" => \&newxQueryTreeReq,
"newxRecolorCursorReq" => \&newxRecolorCursorReq,
"newxReparentWindowReq" => \&newxReparentWindowReq,
"newxRotatePropertiesReq" => \&newxRotatePropertiesReq,
"newxSendEventReq" => \&newxSendEventReq,
"newxSetAccessControlReq" => \&newxSetAccessControlReq,
"newxSetClipRectanglesReq" => \&newxSetClipRectanglesReq,
"newxSetCloseDownModeReq" => \&newxSetCloseDownModeReq,
"newxSetDashesReq" => \&newxSetDashesReq,
"newxSetFontPathReq" => \&newxSetFontPathReq,
"newxSetInputFocusReq" => \&newxSetInputFocusReq,
"newxSetModifierMappingReq" => \&newxSetModifierMappingReq,
"newxSetPointerMappingReq" => \&newxSetPointerMappingReq,
"newxSetScreenSaverReq" => \&newxSetScreenSaverReq,
"newxSetSelectionOwnerReq" => \&newxSetSelectionOwnerReq,
"newxStoreColorsReq" => \&newxStoreColorsReq,
"newxStoreNamedColorReq" => \&newxStoreNamedColorReq,
"newxTranslateCoordsReq" => \&newxTranslateCoordsReq,
"newxUngrabButtonReq" => \&newxUngrabButtonReq,
"newxUngrabKeyReq" => \&newxUngrabKeyReq,
"newxUngrabKeyboardReq" => \&newxUngrabKeyboardReq,
"newxUngrabPointerReq" => \&newxUngrabPointerReq,
"newxUngrabServerReq" => \&newxUngrabServerReq,
"newxUninstallColormapReq" => \&newxUninstallColormapReq,
"newxUnmapSubwindowsReq" => \&newxUnmapSubwindowsReq,
"newxUnmapWindowReq" => \&newxUnmapWindowReq,
"newxWarpPointerReq" => \&newxWarpPointerReq,
"xAllocColorCellsReq" => \&xAllocColorCellsReq,
"xAllocColorPlanesReq" => \&xAllocColorPlanesReq,
"xAllocColorReq" => \&xAllocColorReq,
"xAllocNamedColorReq" => \&xAllocNamedColorReq,
"xAllowEventsReq" => \&xAllowEventsReq,
"xBellReq" => \&xBellReq,
"xChangeActivePointerGrabReq" => \&xChangeActivePointerGrabReq,
"xChangeGCReq" => \&xChangeGCReq,
"xChangeHostsReq" => \&xChangeHostsReq,
"xChangeKeyboardControlReq" => \&xChangeKeyboardControlReq,
"xChangeKeyboardMappingReq" => \&xChangeKeyboardMappingReq,
"xChangePointerControlReq" => \&xChangePointerControlReq,
"xChangePropertyReq" => \&xChangePropertyReq,
"xChangeSaveSetReq" => \&xChangeSaveSetReq,
"xChangeWindowAttributesReq" => \&xChangeWindowAttributesReq,
"xCirculateWindowReq" => \&xCirculateWindowReq,
"xClearAreaReq" => \&xClearAreaReq,
"xCloseFontReq" => \&xCloseFontReq,
"xConfigureWindowReq" => \&xConfigureWindowReq,
"xConvertSelectionReq" => \&xConvertSelectionReq,
"xCopyAreaReq" => \&xCopyAreaReq,
"xCopyColormapAndFreeReq" => \&xCopyColormapAndFreeReq,
"xCopyGCReq" => \&xCopyGCReq,
"xCopyPlaneReq" => \&xCopyPlaneReq,
"xCreateColormapReq" => \&xCreateColormapReq,
"xCreateCursorReq" => \&xCreateCursorReq,
"xCreateGCReq" => \&xCreateGCReq,
"xCreateGlyphCursorReq" => \&xCreateGlyphCursorReq,
"xCreatePixmapReq" => \&xCreatePixmapReq,
"xCreateWindowReq" => \&xCreateWindowReq,
"xDeletePropertyReq" => \&xDeletePropertyReq,
"xDestroySubwindowsReq" => \&xDestroySubwindowsReq,
"xDestroyWindowReq" => \&xDestroyWindowReq,
"xFillPolyReq" => \&xFillPolyReq,
"xForceScreenSaverReq" => \&xForceScreenSaverReq,
"xFreeColormapReq" => \&xFreeColormapReq,
"xFreeColorsReq" => \&xFreeColorsReq,
"xFreeCursorReq" => \&xFreeCursorReq,
"xFreeGCReq" => \&xFreeGCReq,
"xFreePixmapReq" => \&xFreePixmapReq,
"xGetAtomNameReq" => \&xGetAtomNameReq,
"xGetFontPathReq" => \&xGetFontPathReq,
"xGetGeometryReq" => \&xGetGeometryReq,
"xGetImageReq" => \&xGetImageReq,
"xGetInputFocusReq" => \&xGetInputFocusReq,
"xGetKeyboardControlReq" => \&xGetKeyboardControlReq,
"xGetKeyboardMappingReq" => \&xGetKeyboardMappingReq,
"xGetModifierMappingReq" => \&xGetModifierMappingReq,
"xGetMotionEventsReq" => \&xGetMotionEventsReq,
"xGetPointerControlReq" => \&xGetPointerControlReq,
"xGetPointerMappingReq" => \&xGetPointerMappingReq,
"xGetPropertyReq" => \&xGetPropertyReq,
"xGetScreenSaverReq" => \&xGetScreenSaverReq,
"xGetSelectionOwnerReq" => \&xGetSelectionOwnerReq,
"xGetWindowAttributesReq" => \&xGetWindowAttributesReq,
"xGrabButtonReq" => \&xGrabButtonReq,
"xGrabKeyReq" => \&xGrabKeyReq,
"xGrabKeyboardReq" => \&xGrabKeyboardReq,
"xGrabPointerReq" => \&xGrabPointerReq,
"xGrabServerReq" => \&xGrabServerReq,
"xImageText16Req" => \&xImageText16Req,
"xImageText8Req" => \&xImageText8Req,
"xInstallColormapReq" => \&xInstallColormapReq,
"xInternAtomReq" => \&xInternAtomReq,
"xKillClientReq" => \&xKillClientReq,
"xListExtensionsReq" => \&xListExtensionsReq,
"xListFontsReq" => \&xListFontsReq,
"xListFontsWithInfoReq" => \&xListFontsWithInfoReq,
"xListHostsReq" => \&xListHostsReq,
"xListInstalledColormapsReq" => \&xListInstalledColormapsReq,
"xListPropertiesReq" => \&xListPropertiesReq,
"xLookupColorReq" => \&xLookupColorReq,
"xMapSubwindowsReq" => \&xMapSubwindowsReq,
"xMapWindowReq" => \&xMapWindowReq,
"xNoOperationReq" => \&xNoOperationReq,
"xOpenFontReq" => \&xOpenFontReq,
"xPolyArcReq" => \&xPolyArcReq,
"xPolyFillArcReq" => \&xPolyFillArcReq,
"xPolyFillRectangleReq" => \&xPolyFillRectangleReq,
"xPolyLineReq" => \&xPolyLineReq,
"xPolyPointReq" => \&xPolyPointReq,
"xPolyRectangleReq" => \&xPolyRectangleReq,
"xPolySegmentReq" => \&xPolySegmentReq,
"xPolyText16Req" => \&xPolyText16Req,
"xPolyText8Req" => \&xPolyText8Req,
"xPutImageReq" => \&xPutImageReq,
"xQueryBestSizeReq" => \&xQueryBestSizeReq,
"xQueryColorsReq" => \&xQueryColorsReq,
"xQueryExtensionReq" => \&xQueryExtensionReq,
"xQueryFontReq" => \&xQueryFontReq,
"xQueryKeymapReq" => \&xQueryKeymapReq,
"xQueryPointerReq" => \&xQueryPointerReq,
"xQueryTextExtentsReq" => \&xQueryTextExtentsReq,
"xQueryTreeReq" => \&xQueryTreeReq,
"xRecolorCursorReq" => \&xRecolorCursorReq,
"xReparentWindowReq" => \&xReparentWindowReq,
"xRotatePropertiesReq" => \&xRotatePropertiesReq,
"xSendEventReq" => \&xSendEventReq,
"xSetAccessControlReq" => \&xSetAccessControlReq,
"xSetClipRectanglesReq" => \&xSetClipRectanglesReq,
"xSetCloseDownModeReq" => \&xSetCloseDownModeReq,
"xSetDashesReq" => \&xSetDashesReq,
"xSetFontPathReq" => \&xSetFontPathReq,
"xSetInputFocusReq" => \&xSetInputFocusReq,
"xSetModifierMappingReq" => \&xSetModifierMappingReq,
"xSetPointerMappingReq" => \&xSetPointerMappingReq,
"xSetScreenSaverReq" => \&xSetScreenSaverReq,
"xSetSelectionOwnerReq" => \&xSetSelectionOwnerReq,
"xStoreColorsReq" => \&xStoreColorsReq,
"xStoreNamedColorReq" => \&xStoreNamedColorReq,
"xTranslateCoordsReq" => \&xTranslateCoordsReq,
"xUngrabButtonReq" => \&xUngrabButtonReq,
"xUngrabKeyReq" => \&xUngrabKeyReq,
"xUngrabKeyboardReq" => \&xUngrabKeyboardReq,
"xUngrabPointerReq" => \&xUngrabPointerReq,
"xUngrabServerReq" => \&xUngrabServerReq,
"xUninstallColormapReq" => \&xUninstallColormapReq,
"xUnmapSubwindowsReq" => \&xUnmapSubwindowsReq,
"xUnmapWindowReq" => \&xUnmapWindowReq,
"xWarpPointerReq" => \&xWarpPointerReq,
);
$requests[84] = "X_AllocColor";
$requests[86] = "X_AllocColorCells";
$requests[87] = "X_AllocColorPlanes";
$requests[85] = "X_AllocNamedColor";
$requests[35] = "X_AllowEvents";
$requests[104] = "X_Bell";
$requests[30] = "X_ChangeActivePointerGrab";
$requests[56] = "X_ChangeGC";
$requests[109] = "X_ChangeHosts";
$requests[102] = "X_ChangeKeyboardControl";
$requests[100] = "X_ChangeKeyboardMapping";
$requests[105] = "X_ChangePointerControl";
$requests[18] = "X_ChangeProperty";
$requests[6] = "X_ChangeSaveSet";
$requests[2] = "X_ChangeWindowAttributes";
$requests[13] = "X_CirculateWindow";
$requests[61] = "X_ClearArea";
$requests[46] = "X_CloseFont";
$requests[12] = "X_ConfigureWindow";
$requests[24] = "X_ConvertSelection";
$requests[62] = "X_CopyArea";
$requests[80] = "X_CopyColormapAndFree";
$requests[57] = "X_CopyGC";
$requests[63] = "X_CopyPlane";
$requests[78] = "X_CreateColormap";
$requests[93] = "X_CreateCursor";
$requests[55] = "X_CreateGC";
$requests[94] = "X_CreateGlyphCursor";
$requests[53] = "X_CreatePixmap";
$requests[1] = "X_CreateWindow";
$requests[19] = "X_DeleteProperty";
$requests[5] = "X_DestroySubwindows";
$requests[4] = "X_DestroyWindow";
$requests[0] = "X_Error";
$requests[69] = "X_FillPoly";
$requests[115] = "X_ForceScreenSaver";
$requests[79] = "X_FreeColormap";
$requests[88] = "X_FreeColors";
$requests[95] = "X_FreeCursor";
$requests[60] = "X_FreeGC";
$requests[54] = "X_FreePixmap";
$requests[17] = "X_GetAtomName";
$requests[52] = "X_GetFontPath";
$requests[14] = "X_GetGeometry";
$requests[73] = "X_GetImage";
$requests[43] = "X_GetInputFocus";
$requests[103] = "X_GetKeyboardControl";
$requests[101] = "X_GetKeyboardMapping";
$requests[119] = "X_GetModifierMapping";
$requests[39] = "X_GetMotionEvents";
$requests[106] = "X_GetPointerControl";
$requests[117] = "X_GetPointerMapping";
$requests[20] = "X_GetProperty";
$requests[108] = "X_GetScreenSaver";
$requests[23] = "X_GetSelectionOwner";
$requests[3] = "X_GetWindowAttributes";
$requests[28] = "X_GrabButton";
$requests[33] = "X_GrabKey";
$requests[31] = "X_GrabKeyboard";
$requests[26] = "X_GrabPointer";
$requests[36] = "X_GrabServer";
$requests[1] = "X_H";
$requests[77] = "X_ImageText16";
$requests[76] = "X_ImageText8";
$requests[81] = "X_InstallColormap";
$requests[16] = "X_InternAtom";
$requests[113] = "X_KillClient";
$requests[99] = "X_ListExtensions";
$requests[49] = "X_ListFonts";
$requests[50] = "X_ListFontsWithInfo";
$requests[110] = "X_ListHosts";
$requests[83] = "X_ListInstalledColormaps";
$requests[21] = "X_ListProperties";
$requests[92] = "X_LookupColor";
$requests[9] = "X_MapSubwindows";
$requests[8] = "X_MapWindow";
$requests[127] = "X_NoOperation";
$requests[45] = "X_OpenFont";
$requests[11] = "X_PROTOCOL";
$requests[0] = "X_PROTOCOL_REVISION";
$requests[68] = "X_PolyArc";
$requests[71] = "X_PolyFillArc";
$requests[70] = "X_PolyFillRectangle";
$requests[65] = "X_PolyLine";
$requests[64] = "X_PolyPoint";
$requests[67] = "X_PolyRectangle";
$requests[66] = "X_PolySegment";
$requests[75] = "X_PolyText16";
$requests[74] = "X_PolyText8";
$requests[72] = "X_PutImage";
$requests[97] = "X_QueryBestSize";
$requests[91] = "X_QueryColors";
$requests[98] = "X_QueryExtension";
$requests[47] = "X_QueryFont";
$requests[44] = "X_QueryKeymap";
$requests[38] = "X_QueryPointer";
$requests[48] = "X_QueryTextExtents";
$requests[15] = "X_QueryTree";
$requests[96] = "X_RecolorCursor";
$requests[7] = "X_ReparentWindow";
$requests[1] = "X_Reply";
$requests[114] = "X_RotateProperties";
$requests[25] = "X_SendEvent";
$requests[111] = "X_SetAccessControl";
$requests[59] = "X_SetClipRectangles";
$requests[112] = "X_SetCloseDownMode";
$requests[58] = "X_SetDashes";
$requests[51] = "X_SetFontPath";
$requests[42] = "X_SetInputFocus";
$requests[118] = "X_SetModifierMapping";
$requests[116] = "X_SetPointerMapping";
$requests[107] = "X_SetScreenSaver";
$requests[22] = "X_SetSelectionOwner";
$requests[89] = "X_StoreColors";
$requests[90] = "X_StoreNamedColor";
$requests[6000] = "X_TCP_PORT";
$requests[40] = "X_TranslateCoords";
$requests[29] = "X_UngrabButton";
$requests[34] = "X_UngrabKey";
$requests[32] = "X_UngrabKeyboard";
$requests[27] = "X_UngrabPointer";
$requests[37] = "X_UngrabServer";
$requests[82] = "X_UninstallColormap";
$requests[11] = "X_UnmapSubwindows";
$requests[10] = "X_UnmapWindow";
$requests[41] = "X_WarpPointer";
@xevents = ("","","KeyPress","KeyRelease","ButtonPress","ButtonRelease","MotionNotify","EnterNotify","LeaveNotify","FocusIn","FocusOut","KeymapNotify","Expose","GraphicsExpose","NoExpose","VisibilityNotify","CreateNotify","DestroyNotify","UnmapNotify","MapNotify","MapRequest","ReparentNotify","ConfigureNotify","ConfigureRequest","GravityNotify","ResizeRequest","CirculateNotify","CirculateRequest","PropertyNotify","SelectionClear","SelectionRequest","SelectionNotify","ColormapNotify","ClientMessage","MappingNotify"); # xevents
package XC;
$X_cursor = 0;
$arrow = 2;
$based_arrow_down = 4;
$based_arrow_up = 6;
$boat = 8;
$bogosity = 10;
$bottom_left_corner = 12;
$bottom_right_corner = 14;
$bottom_side = 16;
$bottom_tee = 18;
$box_spiral = 20;
$center_ptr = 22;
$circle = 24;
$clock = 26;
$coffee_mug = 28;
$cross = 30;
$cross_reverse = 32;
$crosshair = 34;
$diamond_cross = 36;
$dot = 38;
$dotbox = 40;
$double_arrow = 42;
$draft_large = 44;
$draft_small = 46;
$draped_box = 48;
$exchange = 50;
$fleur = 52;
$gobbler = 54;
$gumby = 56;
$hand1 = 58;
$hand2 = 60;
$heart = 62;
$icon = 64;
$iron_cross = 66;
$left_ptr = 68;
$left_side = 70;
$left_tee = 72;
$leftbutton = 74;
$ll_angle = 76;
$lr_angle = 78;
$man = 80;
$middlebutton = 82;
$mouse = 84;
$num_glyphs = 154;
$pencil = 86;
$pirate = 88;
$plus = 90;
$question_arrow = 92;
$right_ptr = 94;
$right_side = 96;
$right_tee = 98;
$rightbutton = 100;
$rtl_logo = 102;
$sailboat = 104;
$sb_down_arrow = 106;
$sb_h_double_arrow = 108;
$sb_left_arrow = 110;
$sb_right_arrow = 112;
$sb_up_arrow = 114;
$sb_v_double_arrow = 116;
$shuttle = 118;
$sizing = 120;
$spider = 122;
$spraycan = 124;
$star = 126;
$target = 128;
$tcross = 130;
$top_left_arrow = 132;
$top_left_corner = 134;
$top_right_corner = 136;
$top_side = 138;
$top_tee = 140;
$trek = 142;
$ul_angle = 144;
$umbrella = 146;
$ur_angle = 148;
$watch = 150;
$xterm = 152;
1;
