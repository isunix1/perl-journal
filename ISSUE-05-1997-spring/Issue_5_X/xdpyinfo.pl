#!perl

require "./xbase.pl";

$xopen = &xlib::x_open_display($ENV{"DISPLAY"});
die $xlib::status if not defined $xopen;

&xlib::xdebugdisplay($xopen);
print "remaining in queue: ",length($xopen->{"readqueue"}),"\n";
@le = &xlib::x_listextensions($xopen);
print "extensions: ", join("\n      name: ",@le), "\n";

close $xopen->{"XFD"};
