#/*
# * descriptions of errors in Section 4 of Protocol doc (pp. 350-351); more
# * verbose descriptions are given in the error database
# */
# static Const char * Const _XErrorList[] = {
@X::ErrorList = (
"no error",
"BadRequest",
"BadValue",
"BadWindow",
"BadPixmap",
"BadAtom",
"BadCursor",
"BadFont",
"BadMatch",
"BadDrawable",
"BadAccess",
"BadAlloc",
"BadColor",
"BadGC",
"BadIDChoice",
"BadName",
"BadLength",
"BadImplementation",
);
