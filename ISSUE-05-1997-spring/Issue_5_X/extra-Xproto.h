typedef xResourceReq xDestroySubwindowsReq; /*win*/
typedef xResourceReq xDestroyWindowReq; /* w*/
typedef xResourceReq xCloseFontReq; /* fs*/
typedef xResourceReq xQueryFontReq; /* fid*/
typedef xResourceReq xFreeColormapReq; /* cmap*/
typedef xResourceReq xFreeCursorReq; /* cursor*/
typedef xResourceReq xFreeGCReq; /* gc->gid*/
typedef xResourceReq xFreePixmapReq; /* pixmap*/
typedef xResourceReq xGetAtomNameReq; /* atom*/
typedef xResourceReq xGetGeometryReq; /* d*/
typedef xResourceReq xGetSelectionOwnerReq; /* selection*/
typedef xResourceReq xGetWindowAttributesReq; /* w*/
typedef xResourceReq xGetGeometryReq; /* w*/
typedef xResourceReq xInstallColormapReq; /* cmap*/
typedef xResourceReq xKillClientReq; /* resource*/
typedef xResourceReq xListInstalledColormapsReq; /* win*/
typedef xResourceReq xListPropertiesReq; /* window*/
typedef xResourceReq xMapSubwindowsReq; /* win*/
typedef xResourceReq xMapWindowReq; /* w*/
typedef xResourceReq xQueryPointerReq; /* w*/
typedef xResourceReq xQueryTreeReq; /* w*/
typedef xResourceReq xUngrabKeyboardReq; /* time*/
typedef xResourceReq xUngrabPointerReq; /* time*/
typedef xResourceReq xUninstallColormapReq; /* cmap*/
typedef xResourceReq xCloseFontReq; /* font*/
typedef xResourceReq xUnmapSubwindowsReq; /*win*/
typedef xResourceReq xUnmapWindowReq; /* w*/

typedef xReq GetFontPath;
typedef xReq GetInputFocus;
typedef xReq GetKeyboardControl;
typedef xReq GetPointerControl;
typedef xReq GetPointerMapping;
typedef xReq GetScreenSaver;
typedef xReq GrabServer;
typedef xReq ListExtensions;
typedef xReq NoOperation;
typedef xReq GetModifierMapping;
typedef xReq QueryKeymap;
typedef xReq GetInputFocus;
typedef xReq GetInputFocus;
typedef xReq UngrabServer;

#define XWD_FILE_VERSION 7

#define sz_XWDFileHeader 100
typedef struct _xwd_file_header {
	CARD32 header_size;	  /* Size of the entire file header (bytes). */
	CARD32 file_version;	  /* XWD_FILE_VERSION */
	CARD32 pixmap_format;	  /* Pixmap format */
	CARD32 pixmap_depth;	  /* Pixmap depth */
	CARD32 pixmap_width;	  /* Pixmap width */
	CARD32 pixmap_height;	  /* Pixmap height */
	CARD32 xoffset;           /* Bitmap x offset */
	CARD32 byte_order;        /* MSBFirst, LSBFirst */
	CARD32 bitmap_unit;       /* Bitmap unit */
	CARD32 bitmap_bit_order;  /* MSBFirst, LSBFirst */
	CARD32 bitmap_pad;	  /* Bitmap scanline pad */
	CARD32 bits_per_pixel;	  /* Bits per pixel */
	CARD32 bytes_per_line;	  /* Bytes per scanline */
	CARD32 visual_class;	  /* Class of colormap */
	CARD32 red_mask;	  /* Z red mask */
	CARD32 green_mask;	  /* Z green mask */
	CARD32 blue_mask;	  /* Z blue mask */
	CARD32 bits_per_rgb;	  /* Log base 2 of distinct color values */
	CARD32 colormap_entries;  /* Number of entries in colormap */
	CARD32 ncolors;		  /* Number of Color structures */
	CARD32 window_width;	  /* Window width */
	CARD32 window_height;	  /* Window height */
	INT32 window_x;		  /* Window upper left X coordinate */
	INT32 window_y;		  /* Window upper left Y coordinate */
	CARD32 window_bdrwidth;	  /* Window border width */
} XWDFileHeader;
