Back in late 1991, I wrote an X library for perl.  This wasn't a binding
to the C Xlib; this was a raw implementation of the X protocol.

It got some small amount of use around MIT; I never got around to
posting it widely, but got feedback from a few people.

perl5 has some clever new features, which should make it easier to
provide a clean "interface" than the old code had, and easier to
set up good abstractions internally.

Now that I've had time to read through the Perl5 edition of the Camel
Book, it's time to begin...  [eichin:19961101.0337EST]

discoveries:
* attempted to speed up the load of the converted Xproto.
perl *without* -w, user time:
  .cooked file: 0.5s [original mechanism]
  indiv. assign: 1.1s [first modification]
  flattened assign: 0.52s, but more variance(?) [optimized a bit]
Note, however, that this *appears* to be *slower* than 
 parsing the proto file.  Perhaps that shouldn't be a surprise...
So a win might be to store a binary format instead?
Problem is that these are all variable length strings; switching to a 
 split on ^@ gave about the same time range.

in perl5, we should be able to say:
 my($output) = &xlibconvert("xConnSetupPrefix",$reply)
 $success = $output->{"success"};
this would make $output blessed? or perhaps we say
 my($output) = Xlib->new(type=>"xConnSetupPrefix",data=>$reply);
all output needs to be, though, is a hash... ie. convert can 
do the unpack *and* stuff the fields into the hash (and perl4 could 
have probably handled this too, if &xlibconvert explicitly returned
a hash...) so xlibconvert just returns a hashref, which also happens
to contain the "packof" and "sizeof" fields.  However, this only helps
with the few cases where we read now, though it would make it easier
to write more of them (as we'd need to for event handling.) We need a
better syntax for generating protocol messages, if we can...

x_beep just does:
  print XFD pack($xpackof{"xBellReq"},$defines{"X_Bell"},$percentage,1);
we could put the $defines into a namespace directly; in fact, the X
defines are a useful set themselves, and could just become X::Bell and
the like. (maybe def:: for the others? or just make them raw values?)
so, step 1:
  print XFD pack($xpackof{"xBellReq"},X::Bell,$percentage,1);
we can also, at least, define xpack as
  sub xpack { my $typ=shift; pack($packof{$typ},@_); }
and then say
  print XFD &xpack("xBellReq",X::Bell,$percentage,1);
which, with a name space and actual functions, could instead become
  print XFD X::xBellReq(X::Bell,$percentage,1);
Then note that most of these include their *own* verb at the
beginning, there should be a clever way to instead say
  print XFD X::xBellReq($percentage,1);
and now we're getting somewhere.

[eichin:19961105.0458EST]
[done] All uses of xsizeof/sizeof are in bytes -- so drop the *8 /8 altogether.
Many use the $lll self-measuring trick, which could be generalized?

[eichin:19961106.1304EST]
[done] auth is way simple - perhaps we can just implement it next?
should fix "mismatch" code so it doesn't complain as much gratuitously
add more synonyms
[done] implement something like GetResReq/GetEmptyReq (for one arg/no args)
  instead of forcing stuff into extra-Xproto.h?

[eichin:19961108.0113EST]
[done] finish off "xdpyinfo" features -- store xopendisplay output, and then
  parse it seperately...
perhaps a master data queue? or at least *some* way to handle events? [done]
  (maybe just a hook that prints them at first...)

[eichin:19961109.1630EST]
milestones:
	hello world (createwindow, gc, font) [done]
	query-tree (start on wmm!) [done]

[eichin:19961117.1841EST]
request list: determine full protocol coverage based on request names
(up to 1/3 by morning...)

[eichin:19961118.1349EST]
idea from ken: calculate the length in Req subs directly? we *can*...[done]

[eichin:19961119.0351EST]
what about elisp, anyway? :-)

[eichin:19961120.0102EST]
how about a tie'd hash for atom name lookups? [done]

[eichin:19961120.1221EST]
now do the *reverse* as well -- what's a perl "isnumber"? that way we
can use the same data to go both ways... [done]

[eichin:19970227.0155EST]

Renamed .perl files to .pl since emacs handles that extension consistently
now.  Leave .perl for generated files for now.  Add Makefile for release.
