#!perl
# more organized interfaces to xbase functions
require "./xbase.pl";

package xlib;
package xlib::default;
	sub root { xlib::defaultroot(@_); }
	sub rootdepth { xlib::defaultrootdepth(@_); }
package xlib::window;
	sub destroy { xlib::x_destroywindow(@_); }
	sub destroysubs { xlib::x_destroysubwindows(@_); }
	sub map { xlib::x_mapwindow(@_); }
	sub mapsubs { xlib::x_mapsubwindows(@_); }
	sub unmap { xlib::x_unmapwindow(@_); }
	sub unmapsubs { xlib::x_unmapsubwindows(@_); }
	sub configure { xlib::x_configurewindow(@_); }
	sub getgeometry { xlib::x_get_geometry(@_); }
	sub reparent { xlib::x_reparentwindow(@_); }
	sub circulate { xlib::x_circulatewindow(@_); }
	sub create { xlib::x_create_window(@_); }
	sub list { xlib::x_querytree(@_); }
	sub set_background_pixmap { xlib::x_set_window_background_pixmap(@_); }
	sub clear { xlib::x_clear_area(@_); }
	sub saveset { xlib::x_changesaveset(@_); }
	sub translate { xlib::TranslateCoords__PENDING(@_); }
package xlib::window::attributes;
	sub get { xlib::x_get_window_attributes(@_); }
	sub change { xlib::x_change_window_attributes(@_); }
package xlib::pixmap;
	sub create { xlib::x_create_pixmap(@_); }
	sub copyarea { xlib::CopyArea__PENDING(@_); }
	sub copyplane { xlib::CopyPlane__PENDING(@_); }
package xlib::image;
	sub put { xlib::x_put_image(@_); }
	sub get { xlib::GetImage__PENDING(@_); }
	sub free { xlib::FreePixmap__PENDING(@_); }
package xlib::poly;
	sub arcs { xlib::xpolyarcs(@_); }
	sub fillarcs { xlib::xpolyfillarcs(@_); }
	sub rects { xlib::xpolyrects(@_); }
	sub fillrects { xlib::xpolyfillrects(@_); }
	sub segments { xlib::xpolysegments(@_); }
	sub points { xlib::xpolypoints(@_); }
	sub lines { xlib::xpolylines(@_); }
	sub fill { xlib::FillPoly__PENDING(@_); }
package xlib::selection;
	sub setowner { xlib::xsetselectionowner(@_); }
	sub getowner { xlib::xgetselectionowner(@_); }
	sub convert { xlib::xconvertselection(@_); }
package xlib::property;
	sub change { xlib::x_changeproperty(@_); }
	sub delete { xlib::x_deleteproperty(@_); }
	sub get { xlib::x_getproperty(@_); }
	sub list { xlib::x_listproperties(@_); }
	sub rotate { xlib::RotateProperties__PENDING(@_); }
package xlib::font;
	sub getpath { xlib::x_getfontpath(@_); }
	sub setpath { xlib::SetFontPath__PENDING(@_); }
	sub list { xlib::x_listfonts(@_); }
	sub close { xlib::CloseFont__PENDING(@_); }
	sub query { xlib::QueryFont__PENDING(@_); }
	sub queryextents { xlib::QueryTextExtents__PENDING(@_); }
	sub open { xlib::x_openfont(@_); }
package xlib::text;
	sub poly8 { xlib::PolyText8__PENDING(@_); }
	sub poly16 { xlib::PolyText16__PENDING(@_); }
	sub image8 { xlib::x_imagetext8(@_); }
	sub image16 { xlib::ImageText16__PENDING(@_); }
package xlib::atom;
	sub getname { xlib::x_getatomname(@_); }
	sub intern { xlib::x_internatom(@_); }
package xlib::pointer;
	sub grab { xlib::xgrabpointer(@_); }
	sub ungrab { xlib::xungrabpointer(@_); }
	sub changegrab { xlib::xchangeactivepointergrab(@_); }
	sub query { xlib::xquerypointer(@_); }
	sub getmapping { xlib::x_get_pointer_mapping(@_); }
	sub setmapping { xlib::SetPointerMapping__PENDING(@_); }
	sub warp { xlib::WarpPointer__PENDING(@_); }
	sub getcontrol { xlib::GetPointerControl__PENDING(@_); }
	sub changecontrol { xlib::ChangePointerControl__PENDING(@_); }
package xlib::GC;
	sub free { xlib::FreeGC__PENDING(@_); }
	sub change { xlib::ChangeGC__PENDING(@_); }
	sub copy { xlib::CopyGC__PENDING(@_); }
	sub create { xlib::x_create_gc(@_); }
	sub setdashes { xlib::SetDashes__PENDING(@_); }
	sub setcliprects { xlib::SetClipRectangles__PENDING(@_); }
package xlib::colormap;
	sub create { xlib::CreateColormap__PENDING(@_); }
	sub free { xlib::FreeColormap__PENDING(@_); }
	sub copyandfree { xlib::CopyColormapAndFree__PENDING(@_); }
	sub install { xlib::InstallColormap__PENDING(@_); }
	sub uninstall { xlib::UninstallColormap__PENDING(@_); }
	sub listinstalled { xlib::ListInstalledColormaps__PENDING(@_); }
package xlib::color;
# may use a hash, or at least combine allocs
	sub alloc { xlib::AllocColor__PENDING(@_); }
	sub allocnamed { xlib::AllocNamedColor__PENDING(@_); }
	sub alloccells { xlib::AllocColorCells__PENDING(@_); }
	sub allocplanes { xlib::AllocColorPlanes__PENDING(@_); }
	sub free { xlib::FreeColors__PENDING(@_); }
	sub store { xlib::StoreColors__PENDING(@_); }
	sub storenamed { xlib::StoreNamedColor__PENDING(@_); }
	sub query { xlib::QueryColors__PENDING(@_); }
	sub lookup { xlib::LookupColor__PENDING(@_); }
package xlib::cursor;
	sub create { xlib::CreateCursor__PENDING(@_); }
	sub createglyph { xlib::CreateGlyphCursor__PENDING(@_); }
	sub free { xlib::FreeCursor__PENDING(@_); }
	sub recolor { xlib::RecolorCursor__PENDING(@_); }
	sub bestize { xlib::QueryBestSize__PENDING(@_); }
package xlib::access;
	sub change { xlib::ChangeHosts__PENDING(@_); }
	sub list { xlib::ListHosts__PENDING(@_); }
	sub set { xlib::SetAccessControl__PENDING(@_); }
package xlib::screensaver;
	sub set { xlib::SetScreenSaver__PENDING(@_); }
	sub get { xlib::GetScreenSaver__PENDING(@_); }
	sub force { xlib::ForceScreenSaver__PENDING(@_); }
package xlib::key;
	sub grabkeyboard { xlib::xgrabkeyboard(@_); }
	sub grab { xlib::xgrabkey(@_); }
	sub grabbutton { xlib::xgrabbutton(@_); }
	sub ungrabkeyboard { xlib::xungrabkeyboard(@_); }
	sub ungrabbutton { xlib::xungrabbutton(@_); }
	sub ungrab { xlib::xungrabkey(@_); }
	sub getmapping { xlib::x_get_keyboard_mapping(@_); }
	sub changemapping { xlib::ChangeKeyboardMapping__PENDING(@_); }
	sub changecontrol { xlib::ChangeKeyboardControl__PENDING(@_); }
	sub getmodmapping { xlib::x_get_modifier_mapping(@_); }
	sub setmodmapping { xlib::SetModifierMapping__PENDING(@_); }
	sub getcontrol { xlib::x_get_keyboard_control(@_); }
	sub getfocus { xlib::x_get_input_focus(@_); }
	sub setfocus { xlib::SetInputFocus__PENDING(@_); }
	sub getmotion { xlib::GetMotionEvents__PENDING(@_); }
	sub query { xlib::QueryKeymap__PENDING(@_); }
	sub beep { xlib::x_beep(@_); }
package xlib::connection;
	sub setmode { xlib::SetCloseDownMode__PENDING(@_); }
	sub kill { xlib::KillClient__PENDING(@_); }
	sub open { xlib::x_open_display(@_); }
	sub close { xlib::x_closedisplay(@_); }
	sub grab { xlib::xgrabserver(@_); }
	sub ungrab { xlib::xungrabserver(@_); }
package xlib::extensions;
	sub list { xlib::x_listextensions(@_); }
	sub query { xlib::QueryExtension__PENDING(@_); }
package xlib::event;
	sub send { xlib::xsendevent(@_); }
	sub allow { xlib::xallowevents(@_); }
	sub handle { xlib::handle_event(@_); }
