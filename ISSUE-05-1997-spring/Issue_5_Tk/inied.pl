
    # This file initializes all Evaluate Parameters data for 
    # monitor_disk_space and all embedded command processors.

    ### `monitor_disk_space' ###

    package main;

    $PDT = <<'end_of_PDT';
PDT monitor_disk_space, monds
    hosts, ho: list of string = 'Turkey.CC.Lehigh.EDU'
    poll_interval, pi: integer = 60000
    exclude_filesystem_configuration_file, efcf: file = /dev/null
PDTEND no_file_list
end_of_PDT
    $MM = <<'end_of_MM';
monitor_disk_space, monds

        A TCP client/server to monitor disk space and alert operators
        when a machine is running low.

        Each machine runs the server monitor_disk_space_daemon which
        transmits a `df' output when requested by this client.

        Example:

          monds -host dandy -host dillon -host turkey
.hosts
	A list of hosts whose disk space you want monitored. 
	Mutliple -hosts parameters can be specified.
.poll_interval
        Millisecond interval between `df' polls.
.exclude_filesystem_configuration_file
	The pathname of the configuration file containing
	EXCLUDE_FILESYTEMS commands which describe those
	machine/filesystem pairs you do not want monitored.
	(This file can be updated on-the-fly.)

end_of_MM

    @PDT = split( /\n/, $PDT );
    @MM = split( /\n/, $MM );

    ### `exclude_filesystems' ###

    package exclude_filesystems_pkg;

    $PDT = <<'end_of_PDT';
PDT exclude_filesystems, excf
    host, ho: string = $required
    filesystem, f: list of file
PDTEND no_file_list
end_of_PDT
    $MM = <<'end_of_MM';
exclude_filesystems, excf

	Specify filesystems we don't want to monitor.
.host
	The IP name of a host.  If the special host name
	ALL_HOSTS is specified then the list of excluded
	filesystems applies to all monitored hosts.
.filesystem
	Specifies a list of filesystems to ignore.
end_of_MM
    @PDT=split(/\n/, $PDT);
    @MM=split(/\n/, $MM);

    package main;

1;
