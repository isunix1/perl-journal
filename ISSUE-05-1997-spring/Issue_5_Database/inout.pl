#!/usr/bin/perl -w
#
# (c)1996 Alligator Descartes <descarte@hermetica.com>
#
# inout.pl: Connects and disconnects from a database called 'test' on a 
#           local mSQL server

use DBI;

if ( $#ARGV < 0 ) {
    die "Usage: inout.pl <Database String> <Database Vendor>\n";
  }

# Create new database handle. If we can't connect, die()
$dbh = DBI->connect( '', $ARGV[0], '', $ARGV[1] );
if ( !defined $dbh ) {
    die "Cannot connect to mSQL server: $DBI::errstr\n";
  }

# Disconnect from the database
$dbh->disconnect;

exit;
