#!/usr/bin/perl -w
#
# (c)1996 Alligator Descartes <descarte@hermetica.com>
#
# execute.pl: Connects to a database called 'test' on a 
#             given database, then EXECUTEs an update
#             statement. This is non-cursorial, so do()
#             is used.

use DBI;

if ( $#ARGV < 0 ) {
    die "Usage: execute.pl <Database String> <Database Vendor>\n";
  }

# Create new database handle. If we can't connect, die()
$dbh = DBI->connect( '', $ARGV[0], '', $ARGV[1] );
if ( !defined $dbh ) {
    die "Cannot connect to mSQL server: $DBI::errstr\n";
  }

# Prepare the statement for immediate execution
$rv =
    $dbh->do( "
        UPDATE table
        SET name = 'Alligator Descartes'
        WHERE id = 1
      " );
if ( !defined $rv ) {
    die "Statement execution failed: $DBI::errstr\n";
  }
            
# Disconnect from the database
$dbh->disconnect;

exit;
