#!/usr/bin/perl -w
#
# (c)1996 Alligator Descartes <descarte@hermetica.com>
#
# select.pl: Connects to a database called 'test' on a 
#            given database, then SELECTs some basic data
#            out in array and scalar forms

use DBI;

if ( $#ARGV < 0 ) {
    die "Usage: inout.pl <Database String> <Database Vendor>\n";
  }

# Create new database handle. If we can't connect, die()
$dbh = DBI->connect( '', $ARGV[0], '', $ARGV[1] );
if ( !defined $dbh ) {
    die "Cannot connect to mSQL server: $DBI::errstr\n";
  }

# Prepare the statement for execution
$sth =
    $dbh->prepare( "
        SELECT id, name
        FROM table
      " );
if ( !defined $sth ) {
    die "Cannot prepare statement: $DBI::errstr\n";
  }
            
# Execute the statement at the database level
$sth->execute;

# Fetch the rows back from the SELECT statement
while ( @row = $sth->fetchrow() ) {
    print "Row returned: @row\n";
  }

# Re-execute the statement to bring the rows back again
$sth->execute;

# Fetch the data back into separate variables this time
while ( ( $id, $name ) = $sth->fetchrow() ) {
    print "ID: $id\tName: $name\n";
  }

# Release the statement handle resources
$sth->finish;

# Disconnect from the database
$dbh->disconnect;

exit;
