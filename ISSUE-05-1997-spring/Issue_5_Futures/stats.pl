<html>
<head>
<title>stats.pl</title>
</head>
<body>
<h1>stats.pl</h1>
<pre>

# stats.pl -- basic statistics on trade logs
# M. Edward Borasky -- 27-DEC-96

$commslip = 100; # deduction for commission and slippage

# dollar table -- price*table value is contract value
%dollars = (); # allocate dollar table
$dollars{"c_"} = 50; # corn
$dollars{"dm"} = 1250; # Deutschemark
$dollars{"ed"} = 2500; # Eurodollars
$dollars{"fb"} = 1000; # five-year Treasuries
$dollars{"gc"} = 100; # COMEX gold
$dollars{"jy"} = 1250; # Japanese yen
$dollars{"s_"} = 50; # soybeans
$dollars{"si"} = 50; # COMEX silver
$dollars{"ty"} = 1000; # ten-year Treasuries
$dollars{"us"} = 1000; # US T-bonds {30-year}

$count = $sum = $sumsq = 0; # all lengths, all contracts
%ccount = %csum = %csumsq = (); # all lengths, each contract
%dcount = %dsum = %dsumsq = (); # each length, all contracts
%cdcount = %cdsum = %cdsumsq = (); # each length/contract combo

# open P/L record file
open (OUTPUT, "> record.out");

# loop over trade logs
foreach $file (`dir /o /b /l *.prn`) {
	chop ($file);
	open (INPUT, $file);
	print "Loading $file\n";
	&dostats; # load the data
	close (INPUT);
}	

close (OUTPUT);

# print statistics for contract / length combinations
foreach $key (sort(keys(%cdcount))) {
	$mean = $cdsum{$key}/$cdcount{$key};
	$stdev = sqrt($cdsumsq{$key}/$cdcount{$key} - $mean**2);
	$stderr = $stdev/sqrt($cdcount{$key});
	printf ("%s %10g %10g %10g %10.2f %10.2f %10.2f\n",
	  $key, $cdcount{$key}, $cdsum{$key}, $cdsumsq{$key},
	  $mean, $stdev, $stderr);
}

# print statistics for each length
foreach $key (sort(keys(%dcount))) {
	$mean = $dsum{$key}/$dcount{$key};
	$stdev = sqrt($dsumsq{$key}/$dcount{$key} - $mean**2);
	$stderr = $stdev/sqrt($dcount{$key});
	printf ("%s %10g %10g %10g %10.2f %10.2f %10.2f\n",
	  $key, $dcount{$key}, $dsum{$key}, $dsumsq{$key},
	  $mean, $stdev, $stderr);
}

# print statistics for each contract
foreach $key (sort(keys(%ccount))) {
	$mean = $csum{$key}/$ccount{$key};
	$stdev = sqrt($csumsq{$key}/$ccount{$key} - $mean**2);
	$stderr = $stdev/sqrt($ccount{$key});
	printf ("%s %10g %10g %10g %10.2f %10.2f %10.2f\n",
	  $key, $ccount{$key}, $csum{$key}, $csumsq{$key},
	  $mean, $stdev, $stderr);
}

# print statistics for all trades
$mean = $sum/$count;
$stdev = sqrt($sumsq/$count - $mean**2);
$stderr = $stdev/sqrt($count);
$key = "Total";
printf ("%s %10g %10g %10g %10.2f %10.2f %10.2f\n",
  $key, $count, $sum, $sumsq,
  $mean, $stdev, $stderr);

sub dostats { # accumulate statistics
	while (<INPUT>) {
		chop ($_); # get rid of EOL
		@F = split (' ', $_);
		if ($F[2] eq "1" || $F[2] eq "-1") {
			$contract = $F[0];
			$days = $F[1];

			# compute profit from trade
			$profit = $dollars{$contract}*$F[2]*
			  ($F[6]-$F[4]) - $commslip;
			printf OUTPUT 
			  ("%s %12.2f\n",  $_, $profit);
			$key = "$contract $days";

			# compute counts
			$count++;
			$ccount{$contract}++;
			$dcount{$days}++; 
			$cdcount{$key}++;

			# compute sums
			$sum += $profit;
			$csum{$contract} += $profit;
			$dsum{$days} += $profit; 
			$cdsum{$key} += $profit;

			# compute sums of squares
			$sumsq += $profit**2;
			$csumsq{$contract} += $profit**2;
			$dsumsq{$days} += $profit**2; 
			$cdsumsq{$key} += $profit**2;
		}
	}
}	

</pre>
</body>
</html>
