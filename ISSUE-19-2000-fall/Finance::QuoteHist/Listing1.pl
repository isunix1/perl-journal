   use Finance::QuoteHist;
   $q = Finance::QuoteHist->new
      (
       symbols    => [qw(LNUX MSFT IBM)],
       start_date => '01/01/2000',
       end_date   => 'today',
      );

   print "Quotes:\n";
   foreach $row ($q->quotes()) {
     ($date, $open, $high, $low, $close, $volume) = @$row;
     print "$date $close\n";
   }

   @splits = $q->splits();
   if (@splits) {
      print "\nSplits\n";
      foreach $row ($q->splits()) {
         ($date, $new_shares, $old_shares) = @$row;
         print "$date $new_shares:$old_shares\n";
      }
   }

   @dividends = $q->dividends();
   if (@dividends) {
      print "\nDividends\n";
      foreach $row ($q->dividends()) {
         ($date, $amount) = @$row;
         print "$date $amount\n";
      }
   }
