    use Inline;
    Inline->import(C => <DATA>);
    $, = '/';     # Set print list separator
    
    ($scalar1, $scalar2, $scalar3) = ('paper', 'scissors', 42);
    print get_scalars(qw(main::scalar2 main::scalar4 
	                 main::scalar3 main::scalar1));
    print "\n";

    $scalar4 = "Inline";
    undef $scalar1;
    "$scalar3";   # Force scalar to string
    print get_scalars(qw(main::scalar2 main::scalar4 
	                 main::scalar3 main::scalar1));
    print "\n";
    
    __END__
    
    void get_scalars(SV* sv, ...) {
      Inline_Stack_Vars;
      int i;
      SV* name_sv; 
      SV* value_sv;
      char* name;
    
      Inline_Stack_Reset;
      for (i = 0; i < Inline_Stack_Items; i++) {
        name_sv = Inline_Stack_Item(i);
        name = SvPVX(name_sv);
        value_sv = perl_get_sv(name, FALSE);
        if (value_sv && SvPOK(value_sv))
          Inline_Stack_Push(value_sv);
      }
    
      Inline_Stack_Done;
    }
