    # rithmatick.pl
    print "9 + 16 = ", add(9, 16), "\n";
    print "9 - 16 = ", subtract(9, 16), "\n";
 
    use Inline C => <<'END_OF_C_CODE';
    
    int add(int x, int y) {
      return x + y;
    }
 
    int subtract(int x, int y) {
      return x - y;
    }
   
    END_OF_C_CODE
