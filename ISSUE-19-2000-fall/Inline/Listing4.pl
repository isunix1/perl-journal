    use Inline C => <<'END_OF_C_CODE';
    
    void hash_values(SV* hash_ref) {
      Inline_Stack_Vars;
      HV* hash;
      HE* hash_entry;
      int num_keys, i;
      SV* buffer;
      SV* sv_val;
    
      if (! (SvROK(hash_ref) && SvTYPE(SvRV(hash_ref)) == SVt_PVHV))
        croak("Error. Expected a hash reference");
      hash = (HV*)SvRV(hash_ref);
      num_keys = hv_iterinit(hash);
    
      buffer = NEWSV(0, 0);
      for (i = 0; i++ < num_keys;) {
        hash_entry = hv_iternext(hash);
        sv_val = hv_iterval(hash, hash_entry);
        sv_catpvf(buffer, "%s%s", SvPV(sv_val, PL_na),
                  (i < num_keys) ? "," : "");
      }
    
      Inline_Stack_Item(0) = sv_2mortal(buffer);
      Inline_Stack_Return(1);
    }
    
    END_OF_C_CODE
    
    %hash = (
	     Who  => 'Ingy',
	     What => 'Loves',
	     Whom => 'Perl',
	    );
    
    print hash_values(\%hash), "\n";
