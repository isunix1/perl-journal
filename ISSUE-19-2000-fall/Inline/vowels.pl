    use Inline;
    Inline->import(C => <DATA>);   # "eval" the C code at run time
    
    $filename = $ARGV[0];
    die "Usage: perl vowels.pl filename\n" unless -f $filename;
    
    $text = join '', <>;           # slurp input file
    $vp = vowel_scan($text);       # call our function
    $vp = sprintf("%03.1f", $vp * 100);  # format for printing
    print "The letters in $filename are $vp% vowels.\n";
    
    __END__

    /* Find percentage of vowels to letters */
    double vowel_scan(char* str) {
      int letters = 0;
      int vowels = 0;
      int i = 0;
      char c;
      char normalize = 'a' ^ 'A';  /* Assembly programmer trick :-) */
      /* normalize forces lower case in ASCII; upper in EBCDIC */
      char A = normalize | 'a';
      char E = normalize | 'e';
      char I = normalize | 'i';
      char O = normalize | 'o';
      char U = normalize | 'u';
      char Z = normalize | 'z';
    
      while(c = str[i++]) {
        c |= normalize; 
        if (c >= A && c <= Z) {
          letters++;
          if (c == A || c == E || c == I || c == O || c == U)
            vowels++;
        }
      }
    
      return letters ? ((double) vowels / letters) : 0.0;
    }
