    package Math::Simple;
    
    use strict;
    use vars qw($VERSION @ISA @EXPORT_OK);
    require Exporter;
    @ISA = qw(Exporter);
    @EXPORT_OK = qw(add subtract multiply divide);
    BEGIN {
        $VERSION = '1.23';
    }
    
    use Inline;
    Inline->import(C => <DATA>);
    
    1;
    
    __DATA__
    
    double add(double x, double y) {
      return x + y;
    }
    
    double subtract(double x, double y) {
      return x - y;
    }
    
    double multiply(double x, double y) {
      return x * y;
    }
    
    double divide(double x, double y) {
      if (! y)
        croak("Error! Attempt to divide by zero\n");
      return x / y;
    }
