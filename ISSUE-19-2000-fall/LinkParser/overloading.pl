# Overloading a constant for pattern-matching

use overload q("") => "new_as_string";

sub new_as_string
{
  my $linkage = shift;
  my $return = '';
  my $i = 0;
  foreach my $word ($linkage->words)
  {
    my ($before,$after) = '';
    foreach my $link ($word->links)
    {
      my $position = $link->linkposition;
      my $text     = $link->linkword;
      my $type     = $link->linklabel;
      if ($position < $i)
      {
        $before .= "$type:$position:$text ";
      }
      elsif ($position > $i)
      {
        $after.= "$type:$position:$text ";
      }
    }
    $return .= "(" . $before . " \"" . $word->text . "\" " .
         $after . ")" ;
    $i++;
  }
 "(" . $return . ")";
} 
