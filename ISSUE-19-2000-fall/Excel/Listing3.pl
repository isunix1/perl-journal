   #!/usr/bin/perl -w

   use strict;
   use Spreadsheet::WriteExcel;

   # Send the content type
   print "Content-type: application/vnd.ms-excel\n\n";

   # Redirect the output to STDOUT
   my $workbook  = Spreadsheet::WriteExcel->new("-");
   my $worksheet = $workbook->addworksheet();

   $worksheet->write(0, 0, "The Perl Journal");
