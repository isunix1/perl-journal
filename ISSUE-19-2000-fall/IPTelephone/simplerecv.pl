    #!/usr/bin/perl

    use strict;
    use IO::Socket;
    use IO::File;

    use constant DSP => '/dev/dsp';
    use constant BUFSIZE => 4000;
    $SIG{CHLD} = sub { wait };

    my $port = shift || 2007;
    my $listen = IO::Socket::INET->new(LocalPort=>$port,
                                       Listen=>5,
                                      Reuse =>1) || die "Can't listen: $!";


   while (1) {
     warn "waiting for a connection...\n";
     my $sock = $listen->accept;
     warn "accepting connection from ",$sock->peerhost,"\n";
     unless (my $dsp = IO::File->new(DSP,"r+")) {
       close $sock;
     } else {
       handle_connection($sock,$dsp);
     }
   }

   sub handle_connection {
     my ($sock,$dsp) = @_;
     my $child = fork();
     die "Can't fork: $!" unless defined $child;
     my $data;
     if ($child) { # parent process
       eval {
         local $SIG{INT} = sub {die};
         print $dsp $data while sysread($sock,$data,BUFSIZE);
       };
       close $sock;
       kill TERM=>$child;
     } else {
       close $listen;
       print $sock $data while sysread($dsp,$data,BUFSIZE);
       exit;
     }
   }
