    #!/usr/bin/perl
    # file mp3_send.pl

    use strict;
    use DSP;
    use IO::Socket;

    use constant BUFSIZE => 1024;
    $SIG{CHLD} = sub { exit 0 };

    my $dest = shift || 'prego';
    my $port = shift || 2007;
    my $sock = IO::Socket::INET->new("$dest:$port") || die "Can't connect: $!";

   my $dsp = new DSP;

   my $child = fork();
   die "Can't fork: $!" unless defined $child;

   my $data;
   if ($child) { # parent process
     my $uncompress = $dsp->uncompress;
     print $uncompress $data while sysread($sock,$data,BUFSIZE);
     kill TERM=>$child;
   } else {
     my $compress = $dsp->compress;
     print $sock $data while sysread($compress,$data,BUFSIZE);
   }
