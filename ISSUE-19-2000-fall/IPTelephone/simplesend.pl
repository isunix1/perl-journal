    #!/usr/bin/perl

    use strict;
    use IO::File;
    use IO::Socket;

    use constant BUFSIZE => 4000;
    use constant DSP     => '/dev/dsp';
    $SIG{CHLD} = sub { exit 0 };

    my $dest = shift || 'prego';
    my $port = shift || 2007;
    my $sock = IO::Socket::INET->new("$dest:$port") || die "Can't connect: $!";
   warn "Connected, go ahead...\n";

   my $dsp = IO::File->new(DSP,"r+") || die "Can't open DSP: $!";

   my $child = fork();
   die "Can't fork: $!" unless defined $child;

   my $data;
   if ($child) { # parent process
     print $dsp $data while sysread($sock,$data,BUFSIZE);
     kill TERM=>$child;
   } else {
     print $sock $data while sysread($dsp,$data,BUFSIZE);
   }
