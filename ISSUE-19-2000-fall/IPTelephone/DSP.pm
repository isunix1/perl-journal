    package DSP;

    use strict;
    use IO::File;
    use Carp 'croak';
    use vars '%sizeof','@ISA';
    @ISA = 'IO::Handle';

    BEGIN {
      use Config;
      %sizeof= ( int => $Config{intsize} );
      require "sys/soundcard.ph";
   }

   use constant DSP        => '/dev/dsp';
   use constant COMPRESS   => $Config{byteorder} eq '1234'
                              ? "lame -r -x -m m -b32 -s16 - - |"
                              : "lame -r    -m m -b32 -s16 - - |";
   use constant UNCOMPRESS => "| mpg123 -s -m -";

   sub new {
     my $class = shift;
     my $dsp = IO::File->new('/dev/dsp','r+') or croak "Can't open /dev/dsp: $!";
     croak "can't set samplesize: $!" unless ioctl $dsp,SNDCTL_DSP_SAMPLESIZE,pack("I",16);
     croak "can't set speed: $!"      unless ioctl $dsp,SNDCTL_DSP_SPEED,pack("I",16000);
     croak "can't set mono: $!"       unless ioctl $dsp,SNDCTL_DSP_STEREO,pack("I",0);
     return bless $dsp,$class;
   }

   # read from encode to get MP3 from microphone
   sub compress {
     my $self = shift;
     open(S,"<&STDIN");
     STDIN->fdopen($self,"r") or die "Can't reopen STDIN on DSP: $!";
     my $encode = IO::File->new(COMPRESS) or croak "Can't open lame: $!";
     open(STDIN,"<&S");
     return $encode;
   }

   # write to decode to get sound from MP3
   sub uncompress {
     my $self = shift;
     open(S,">&STDOUT");
     STDOUT->fdopen($self,"w") or die "Can't reopen STDOUT on DSP: $!";
     my $uncompress = IO::File->new(UNCOMPRESS) or die "Can't open mpg123: $!";
     $uncompress->autoflush(1);
     open(STDOUT,">&S");
     return $uncompress
   }

   1;
