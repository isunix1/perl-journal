    #!/usr/bin/perl
    # file: mp3_recv.pl

    use strict;
    use DSP;
    use IO::Socket;

    use constant BUFSIZE => 1024;
    $SIG{CHLD} = sub { wait; };

    my $dest = shift || 'prego';
    my $port = shift || 2007;
    my $listen = IO::Socket::INET->new(LocalPort=>$port,
                                      Listen=>5,
                                      Reuse =>1) || die "Can't listen: $!";

   while (1) {
     warn "waiting for a connection...\n";
     my $sock = $listen->accept;
     warn "accepting connection from ",$sock->peerhost,"\n";
     unless (my $dsp = new DSP) {
       warn "DSP unavailable.  Hanging up.\n";
       close $sock;
     } else {
       handle_connection($sock,$dsp);
     }
   }

   sub handle_connection {
     my ($sock,$dsp) = @_;
     my $child = fork();
     die "Can't fork: $!" unless defined $child;
     my $data;
     if ($child) { # parent process
       eval {
         local $SIG{INT} = sub {die};
         my $uncompress = $dsp->uncompress;
         print $uncompress $data while sysread($sock,$data,BUFSIZE);
       };
       close $sock;
       kill TERM=>$child;
     } else {
       close $listen;
       my $compress = $dsp->compress;
       print $sock $data while sysread($compress,$data,BUFSIZE);
       exit;
     }
   }

