#!/usr/bin/perl

use strict;

use CGI;
use DBI ();

## Create a CGI query object
my $q = CGI->new();
my $base_url = $q->url( -full => 1 );

## Map rating numbers to strings
my %ratings = (
	       1=> qq{I\'m dumber for having read it},
	       2=> qq{Useless},
	       3=> qq{Not very helpful},
	       4=> qq{Somewhat helpful},
	       5=> qq{Just what I needed},
	      );

## Define these variables here so they're visible to all parts of our
## script (including subroutines)
my( $prefix, $ttdb, $dbuser, $dbpass, $ttroot );

##
## Get Database parameters from %ENV
##
$prefix = $ENV{'TopTenPrefix'} || 'topten';
$ttdb = $ENV{'TopTenDB'} || 'tpj';
$dbuser = $ENV{'TopTenDBUser'} || 'ap_auth';
$dbpass = $ENV{'TopTenDBPass'} || '';
$ttroot = $ENV{'TopTenRoot'} || '/tmp';

## Print HTTP headers, and start of HTML page
print $q->header( 'text/html' ),
      $q->start_html( -title => 'TopTen Ratings',
		      -bgcolor => '#ffffff' 
		    ),
      "<h1>TopTen Ratings</h1>\n";

## Get rating parameter
my $rating = $q->param( 'rating' );

## See if they actually selected anything first
if( defined $rating ) {
  ## Figure out which document they're rating.
  my $referer = $q->referer;
  $referer =~ s/.*\/$ENV{TopTenPrefix}\///;

  ## Convert to path of file if a positional number was used.
  $referer = &position_to_path( $referer )
    if $referer =~ /\d+/;

  ## Open a connection to the database and retrieve the current rating
  ## and number of ratings
  my $dbh = DBI->connect( "dbi:Pg:dbname=$ttdb", 
			  $dbuser, $dbpass )
    or die "DBI Error: $DBI::errstr";

  my $sth = $dbh->prepare( qq{
    select rating, raters from documents where path = ?;
  })
    or die "DBI Error: " . $dbh->errstr;

  die "DBI Error: " . $dbh->errstr
    unless ($sth->execute( $referer ));

  my( $oldrating, $raters ) = $sth->fetchrow;

  ## Done with the statment handle
  $sth->finish;

  ## Compute the new average rating
  my $newrating = (($raters * $oldrating) + $rating) / ($raters+1);

  ## Update the values in the database
  $sth = $dbh->prepare( qq{
    update documents set rating =  ?, raters = raters + 1
               where path = ?;
  })
    or die "DBI Error: " . $dbh->errstr;

  die "DBI Error: " . $dbh->errstr
    unless ($sth->execute( $newrating, $referer ));

  ## Done with database 
  $sth->finish;
  $dbh->disconnect;

  ## 
  ## Give the user feedback and let them see the new rating
  ##
  print "Document: " . $q->referer . "<br>\n",
        "Your Rating:&nbsp;", $ratings{$rating}, "($rating)<br>\n",
        "New Average Rating:&nbsp;", 
        sprintf( "%-0.2f", $newrating ), "<p>\n",
        qq{
    Thank you for your feedback.<p>
    <a href="/$prefix/">Back to the Top Ten Documents</a><p>
    <a href="@{[$q->referer]}">Back to the document</a><p>
  };
} else {
  ## Didn't give us a rating, so gripe at them.
  print qq{
<p>Please use your browsers 'Back' button and select a rating before hitting
the 'Submit Rating' button.</p>
};
}
    
print $q->end_html, "\n";

##
## Subroutines
##
sub position_to_path {
  my $position = shift;

  ## Open connection to the database
  my $dbh = DBI->connect( "dbi:Pg:dbname=$ttdb", 
			  $dbuser, $dbpass );
    
  ## Prepare select statement to get information from db
  my $sth = $dbh->prepare( qq{
    select path, hits, rating from documents 
      order by rating desc, hits desc;
  } );

  ## Return a server error if the statement doesn't run
  die "DBI Error: " . $sth->errstr 
    unless $sth->execute;

  ## Fetch the $docnum'th item from the table
  my $row = undef;
  for( my $i = 0; $i < $position ; $i++ ) {
    $row = $sth->fetchrow_arrayref;
  }

  ## Done with database handles
  $sth->finish;
  $dbh->disconnect;

  return $row->[0];		# Return path
}
