<html>
<head>
<title>Top Ten Documents</title>
</head>

<body bgcolor="#ffffff">

<!-- 
Embperl uses square brackets to delimit its
commands from the surrounding HTML.  If you need
to insert a literal left square bracket, use two:
[[
-->

<!--
First off, an example of Embperl's conditionals.  The
[[$$] conditional delimeters allow you to (for
example) conditionally include or exclude text.
Depending on if $ENV{QUERY_STRING} is 'all' or
not, we'll change the heading of the document.

Conditional tags can also execute a while loop, or
build <hidden> fields in a form from hash
variables.  See the Embperl documentation for more
information on the [[$ while $] and [[$ hidden $]
commands.
-->
<h1>
[$ if $ENV{QUERY_STRING} eq 'all' $]
All
[$ else $]
Top Ten
[$ endif $]
Tracked Documents
</h1>

<!-- 
Next, define a simple subrotine to show the
use of [[!!]  sections.  Code within these
delimeters is executed only during the first
request.

We'll use this subroutine to alternate color some
of the cells of our example table.
-->
[!
sub colorsub {
  return shift() % 2 ? '#ffffff' : '#cccccc';
}
!]

<!-- 
Retrieve document list from database.  The
following statements (inside the [--]) are eval'd
by Embperl and any return value discarded.
-->
[-

## Connect to database 
use DBI;
my $dbh = DBI->connect( "dbi:Pg:dbname=tpj", "ap_auth" )
	    or die "Can't connect: $DBI::errstr\n";

## select statement to grab information
my $sth = $dbh->prepare( qq{
  select title, path, hits, rating from documents
	order by rating desc, hits desc;
});

## Execute it
$sth->execute or die "Can't execute: $DBI::errstr";

## Slurp first 10 results (or all results if
## $ENV{QUERY_STRING} is 'all') into arrayref and
## store that into $indexdata
$indexdata = $ENV{QUERY_STRING} eq 'all' ?
		$sth->fetchall_arrayref :
		[@{$sth->fetchall_arrayref}[0..9]];

$sth->finish;

## Close DB connection (NOOP if using Apache::DBI)
$dbh->disconnect;
-]

<!--
At this point, $indexdata is an array ref that
looks like:

   $indexdata = [
		 [ title, path, hits, rating ],
		 [ title, path, hits, rating ],
		 ...
                ]

The following <table> is parsed by Embperl. It
ignores the first row of table headings (<th>).
It repeats each row within the second <tr></tr>
tags, setting the variables $row and $col as
apropriate.  Embperl continues doing so as long as
the expressions which use these values have a
defined value.  Once $indexdata runs out, it
prints the closing </table> tag.

The [- $escmode = 0 -] line turns off Embperl's
automatic escaping of HTML entities inside the
contents of the anchor tag, and the corresponding
[- $escmode = 1 -] reenables it for the title
text.
-->

<table border="0" width="75%">
  <tr>
    <th>#</th><th>Title</th><th>Hits</th><th>Rating</th>
  </tr>
  <tr bgcolor="[+ colorsub( $row ) +]">
    <td>
	<a href="[+ $row + 1 +]">[+ $row + 1 +]</a>
    </td>
    <td width="50%">
        [- $escmode = 0; -]
	<a href="[+ "$indexdata->[$row]->[1]" +]">
        [- $escmode = 1; -]
	  [+ $indexdata->[$row]->[0] +]</a>
    </td>
    <td>[+ $indexdata->[$row]->[2] +]</td>
    <td>[+ sprintf "%-0.2f", $indexdata->[$row]->[3] +]</td>
  </tr>
</table>

<!-- 
And finally, put a link from the All version to
the Top Ten version (and vice versa).

This makes use of Embperl's providing an Apache object
in $req_req when running under mod_perl.
-->
[$ if $ENV{QUERY_STRING} eq 'all' $]
<a href="/[+ 
$req_rec->dir_config( 'TopTenPrefix')||'topten'
+]/">Top Ten Documents</a>
[$ else $]
<a href="/[+ 
  ($req_rec->dir_config( 'TopTenPrefix')||'topten')
  . '/?all'
+]">All Tracked Documents</a>
[$ endif $]
</body>
</html>