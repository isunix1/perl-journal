package Apache::NavBar;
# file Apache/NavBar.pm

use strict;
use Apache::Constants qw(:common HTTP_NOT_MODIFIED);
use Apache::File ();
use Apache::Util qw(parsedate ht_time);

my %BARS = ();
my $TABLEATTS   = 'WIDTH="100%" BORDER=1';
my $TABLECOLOR  = '#C8FFFF';
my $ACTIVECOLOR = '#FF0000';

sub handler {
    my $r = shift;
    
    $r->content_type eq 'text/html' || return DECLINED;
    my $bar   = read_configuration($r) || return DECLINED;
    my $table = make_bar($r, $bar);

    my $file_time = (stat($r->finfo))[9];
    if ($file_time) {
        my ($last_modified) = max($file_time,$bar->modified);
        $r->header_out('Last-Modified' => ht_time($last_modified));

        if ($r->method eq 'GET' && $r->header_in("If-Modified-Since")) {
	    my ($cache_time) = split(/;/,$r->header_in("If-Modified-Since"));
	    $cache_time = parsedate($cache_time);
	    return HTTP_NOT_MODIFIED if $cache_time >= $last_modified;
        }
    }

    my $fh = Apache::File->new($r->filename) || return DECLINED;
    
    $r->send_http_header;
    return OK if $r->header_only;
    
    while (<$fh>) {
      s:(<BODY.*?>):$1$table:oi;
      s:(</BODY>):$table$1:oi;
    } continue { 
 	$r->print($_); 
    }
    
    return OK;
}

sub make_bar {
    my($r, $bar) = @_;
    # create the navigation bar
    my $current_url = $r->uri;
    my @cells;
    foreach my $url ($bar->urls) {
 	my $label = $bar->label($url);
 	my $is_current = $current_url =~ /^$url/;
 	my $cell = $is_current ?
 	    qq(<FONT COLOR="$ACTIVECOLOR" CLASS="active">$label</FONT>)
 		: qq(<A HREF="$url" CLASS="inactive">$label</A>);
 	push @cells, 
 	qq(<TD CLASS="navbar" ALIGN=CENTER BGCOLOR="$TABLECOLOR">$cell</TD>\n);
    }
    return qq(<TABLE CLASS="navbar" $TABLEATTS><TR>@cells</TR></TABLE>\n);
}

# read the navigation bar configuration file and return it as a
# hash.
sub read_configuration {
    my $r = shift;
    return unless my $conf_file = $r->dir_config('NavConf');
    return unless -e ($conf_file = $r->server_root_relative($conf_file));
    my $mod_time = (stat _)[9];
    return $BARS{$conf_file} if $BARS{$conf_file} 
    && $BARS{$conf_file}->modified >= $mod_time;
    return $BARS{$conf_file} = NavBar->new($conf_file);
}

package NavBar;

# create a new NavBar object
sub new {
    my ($class,$conf_file) = @_;
    my (@c,%c);
    my $fh = Apache::File->new($conf_file) || return;
    while (<$fh>) {
 	chomp;
 	next if /^\s*\#/;  # skip comments
 	my($url, $label) = /^(\S+)\s+(.+)/;
 	push @c, $url;     # keep the url in an ordered array
 	$c{$url} = $label; # keep its label in a hash
    }
    return bless {'urls' => \@c,
 		  'labels' => \%c,
 		  'modified' => (stat $conf_file)[9]}, $class;
}

# return ordered list of all the URLs in the navigation bar
sub urls  { return @{shift->{'urls'}}; }

# return the label for a particular URL in the navigation bar
sub label { return $_[0]->{'labels'}->{$_[1]} || $_[1]; }

# return the modification date of the configuration file
sub modified { return $_[0]->{'modified'}; }

1;
__END__

