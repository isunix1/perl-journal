#!/usr/bin/perl
#
# test.pl  - test connection to Access database

use DBI;

$dsn  = 'TPJ';        # Configure $dsn, $user, and $pass in...
$user = 'perl';       # ...Control Panel/32bit ODBC
$pass = 'lerp';      
$dbd  = 'ODBC';       # A must if using ODBC.

# Connect to the database
$dbh = DBI->connect($dsn, $user, $pass, $dbd, {RaiseError => 1})
    or die "Database connection not made: $DBI::errstr";

# Prepare the statement for execution
# The table in the CSV file was named "Rolodex"
$stmt = $dbh->prepare(" SELECT * FROM Rolodex " );   

# Check if statement prepared correctly
die "ERROR: Cannot prepare statement: $DBI::errstr\n" unless (defined $stmt);

# Execute the statement at the database level
$stmt->execute;

# Print out first 5 records, each field on a line
for (1..5) {
    @record = $stmt->fetchrow();
    print "\n\n\nRECORD:\n";
    foreach $field (@record) {
	print "$field\n";
    }
}

# Clean up and go home
$stmt->finish;               # Release the statement handle resources
$dbh->disconnect;            # Close database handle

