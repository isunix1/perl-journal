#!/usr/bin/perl -w
use strict;

#
# This is what the code is actually doing.  In the real version
# it is operating upon itself rather than on an arbitrary input
# string.  We could just as easily replace the input string with:
#
# XXXXXXXXXXXXXXXXXXXX XXXXXXXX XXXXX XXXXXXXXXXXXXXXX XXXXX
# XXXXXXXXXXXXXXXXXX XXXXXXXXXXXX XXXXXXXXXX XXXXXXXXXXXXXXX
# XXXXXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXXXXXX XXXXXXXXXXXXXX X XXXXXXXXXXXX
#
# Which translates to (counting the X's):
#
# 	20  8  5 16  5 18 12 10 15 20 18 14  1 12
#
# Which is transformed into:
#
#        t  h  e  p  e  r  l  j  o  u  r  n  a  l
#
# Note that this is not the exact same algorithm as the code version;
# I replaced the s///gxe with the more natural split into words.  The
# search method was just uglier in the code.
#
# Tramm <hudson@swcp.com>
#
sub p
{
        chomp;
        for( split ) {
                my $char = chr(length($_) + ord('a') - 1);
                print $char;
        }
}
 
<DATA>; <DATA>;		# Ignore the first two lines

$_=<DATA>; &p;
$_=<DATA>; &p;
$_=<DATA>; &p;
print "\n";

__END__
#!/usr/bin/perl -w
use strict; local *D = *DATA; local     # Tramm Hudson (hudson@swcp.com)
$_=<D>;seek(D,9,0);$ {_}=<D>. <D>;; ;$_=<DATA>;&p;$_ =<D>;
&p;$_=<D>;&p();sub p{chomp$_;s{ ([^\s]+)}{ length$1}gxe&do
{for(split){print(chr ($_+ord('a')-1))}} };print("\n",) ; exit(0xDEAD)
                                        # Do not edit!  There be dragons
