I have submitted a one-line script that requires the LWP::Simple library.
To execute the script, just type:

		tpj

The script eval's a series of numbers that form the following line of perl
code:

use LWP::Simple;printf "%s\n",((get("http://www.tpj.com"))=~m#<TITLE>\s*(.*?)\s*</TITLE>#is);

It goes out to main web page of The Perl Journal and strips out the
TITLE, which happens to be "The Perl Journal", and prints it out.
The really nice thing about this line of code is that NO variables
are used in the process. Hope you enjoyed this submission.

Thanks,

Art Ramos