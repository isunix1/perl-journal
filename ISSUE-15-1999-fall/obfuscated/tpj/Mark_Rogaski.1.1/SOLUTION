
SOLUTION:

A black eval, gray codes, and a white lie ...

1) A 48 character array is created, the first half is some scrambled text
   (to call it encrypted would indicate employment at Microsoft), the second
   half is "Just another Perl hacker".

2) The first half of the array is XOR'd with the second half, and the resulting
   string is placed in a previously undefined variable, $w.  This $w variable
   has been tacked onto the end of the array argument to foreach, so in the
   last iteration of the loop, the /\D/ condition is used as a short circuit 
   to prevent $w from being used as an array index.
   
   Since $_ is now an alias for $w, the black eval (an eval using $_) the
   unscrambled string (which is '$a=0;"Th elrPelarn Juo";') is eval'd,
   assigning "Th elrPelarn Juo" to $o and $n.

3) This string will ultimately reveal "The Perl Journal" if we index it using
   gray codes.  For those unfamiliar with gray codes, they are a sequence of
   bit patterns that have some useful properties that I exploit later.  The
   mapping of unsigned integers to gray codes is:

     G[i] = B[i+1] XOR B[i]

   and the reverse mapping is:

     B[i] = G[i] XOR G[i+1] ... XOR G[n-1]

   These functions are g() and b(), respectively, and handle conversion of
   4 bit unsigned integers.

4) A 2D matrix of references to anonymous functions that map characters from
   $o to $n, using gray-to-binary, binary-to-gray, AND with 0xF, OR with 0xF,
   and XOR with 0xF.

5) The method that will be selected from the matrix for mapping depends upon
   the character's position in the $o string.  The d() function measures the
   number of bits that differ between two 4 bit unsigned integers.  We feed it
   the gray codes for the the position and it's immediate neighbor for the 
   first index to the matrix, and the gray codes for the position and the
   position that is n/2 away.  This is the white lie, since two useful
   properties of gray numbers are:

     a) X[i] and X[(i + 1) % n] only differ by one bit

     b) X[i] and X[(i + (n / 2)) % n] only differ by 2 bits

   In other words, except for $hell[1][2] and its corresponding anonymous
   sub, the rest of the matrix and subs are chaff, unused and merely meant to
   divert attention.

6) So, the string in $o is mapped to $n, using gray codes as an index, and the
   resulting $n is printed in the END block ... "The Perl Journal".

--
Mark Rogaski <wendigo@pobox.com>
... hoping his explanation wasn't more obfuscated than his entry.

