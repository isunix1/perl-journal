SOLUTION
--------

This script essentially decodes the string $mystery.
While the string is not null, &searchforclue gets the first character off the string,
gets its ASCII value and subtracts 65.  This result is stored in $hint and represents the number of character pairs that the script will look at.

&examinehint sets $bighint to 2.  All the trigonomety is math identity fluff that evaluates to 1.  In specific sin(theta)*sin(theta)+cos(theta)*cos(theta)=1.  A couple x**0=1 type things thrown in for the fun of it, and some strings to evaluate to 0.

&hypothesize does the dirty work.  It loops $hint number of times, each time using a pair of characters, starting with the first two. The bitwise exclusive or of the ASCII values of these two characters is taken.  $bighint is multiplied by the result.  The next time through the loop, charcters 2 and 3 are used, then 3 and 4, and so on.

&displayconclusion simply prints the character with the ASCII value of $bighint.

&keepthinking takes the characters just used off the front of $mystery.

What is essentially happening is that the ASCII values of the characters in the string
"THE PERL JoURNAL" are f ncalculaludthe m in  paly  tifharorizatlussult.  Txve operatlussaretwees in tletracter pairead of thely  tifharorsult.  through the firw
Whihe loo.  $bighint  bicerw
Whahinhoullieeult.  Tsy d, aw
Whihe lize does tsamtype th agets the firse &kehahicalls &tryagairown$myaused 

&displayconclusult&tryagairors judiviecod.  $bighied 2ieein eply prpenins its ASChe charac.---
en,hat the scrooponfst us4, aangestorbeyd, aallrty dterve mofirwy dtergluss up.