BEGIN { $^W=1; }

use strict;

sub exor( $ );

# Read the ASCII art
my @TPJ = <DATA>;

# One-line-ify
my $TPJ = join("", map { chomp; $_ } @TPJ);

# Turn runs of #'s into [+,-./] (ascii 43-47)
foreach my $length (reverse 1..5) {
  $TPJ =~ s{\w{$length}}{chr(42+$length)}eg;
}

# Turn runs of spaces into 1-9
foreach my $length (reverse 1..9) {
  $TPJ =~ s{ {$length}}{$length}eg;
}

# Look for common duples
my %Compression;

my $start = ord("9")+1;
my $compressto = chr($start);
while(ord($compressto) < 122) {
  my %Count;
  map { $Count{substr($TPJ, $_, 2)}++; } (0..length($TPJ)-2);

  last unless scalar(keys %Count);

  my $Common = (reverse sort { $Count{$a} <=> $Count{$b} } keys %Count)[0];

  print STDERR "Replacing $Common with $compressto -- $Count{$Common} changes\n";
  $TPJ =~ s{\Q$Common\E}{$compressto}g;

  $Compression{$compressto} = $Common;

  $compressto = chr(ord($compressto)+1); # Might be a digit
}

print STDERR "You're left with $TPJ\n";

my @Compressions = map { $Compression{$_} } sort { $a cmp $b } keys %Compression;

print STDERR "Creating program...\n";

my $m = "##"x$start .join("",@Compressions);

my @Decompresser = map { exor(" $_") }
  ("\$_='$TPJ';foreach \$i(reverse $start..".($start+$#Compressions)."){\$r=chr(\$i);",
   "s{\\Q\$r\\E}{('$m'",
   "=~ /(..)/g)[\$i]}ge}",
   "s/\\d/' 'x\$&/eg;",
   "s/([+.\\/,-])",
   "/'#'x(ord(\$1)-42)/gex;",
   "print join(\"\\n\", /(",
   ".{" . length($TPJ[0]) . "})/gx),\"\\n\";");

# Here's an xor-er

my @Decoder = map { " $_ " }
  (" \$/=\$\\;open(X,\$0);while(<X>){;s{.",
   " }{chr(~ord(\$&)&3|ord(\$&)&252)}gex;eval \$_;}");


# The last lines are special
my @LastLines = splice(@Decompresser, -@TPJ);

my $length=0;
map { $length=length($_) if length($_)>$length } @LastLines;
map { $_ .= " " x (2+$length-length($_)) . shift @TPJ } @LastLines;

# Alternate the decoder with what's left of the decompresser
while(@Decoder || @Decompresser) {
  print shift @Decoder, "\n" if @Decoder;
  print shift @Decompresser, "\n" if @Decompresser;
}
print join("\n", @LastLines), "\n";

sub exor ($) {
  my ($string) = @_;
  $string =~ s/./chr(~ord($&)&3|ord($&)&~3)/eg;
  $string;
}
  
__DATA__
XXXXX X  X XXXX    XXXX  XXXX XXX  X         X  XX  X  X XXX  X   X  XX  X   
  X   X  X X       X   X X    X  X X         X X  X X  X X  X XX  X X  X X   
  X   XXXX XXX     XXXX  XXX  XXX  X         X X  X X  X XXX  X X X XXXX X   
  X   X  X X       X     X    X X  X      X  X X  X X  X X X  X  XX X  X X   
  X   X  X XXXX    X     XXXX X  X XXXX   XXXX  XX   XX  X  X X   X X  X XXXX
