#include <stdlib.h>
#include <stdio.h>

#define MAIN \
        sub main {}

#define STRUCT \
        sub struct {}

#define SETSUBVER subver.number = 10 * SUBVER

struct eval
{
        int  number;
        char * name;
};


int
main (void)
#define SUBVER \
        .1;
{
        struct eval subver;
        SETSUBVER;
        printf ("This is \"Hello World\" version 1.%i ", subver.number);
        if (subver.number == 1)
        {
                printf ("(the C version):\n\n");
        }
        else
        {
                printf ("(the Perl version):\n\n");
        }
        printf ("Hello, World!\n");
        exit (EXIT_SUCCESS);
}

