package Date;

use Carp;

use overload ('fallback' => 1,
	      '0+'  => 'to_number',
	      'cmp' => 'compare',
	      '<=>' => 'compare',
	      '""'  => 'as_string',
	      '-'   => 'subtract',
	      '+'   => 'add');

# Constructor: get day, month, year, return object
sub new {
    my ($class, %args) = @_;	# Argument checking
    $args{'month'} -= 1;	# should be done here
    $args{'year'} -= 1900;	# and here

    my $ctime = timelocal(0,0,0, $args{'day'}, $args{'month'}, $args{'year'});
    my $this = { 'ctime' => $ctime };
    return bless $this, $class;
}

# Create a nice string for a date, like "Dec 31, 1999"
sub as_string {
    my ($this) = @_;
    
    my ($dd, $mm, $yy) = (localtime($this->{'ctime'}))[3,4,5];
    $mm = (qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec))[$mm];
    $yy += 1900;
    return "$mm $dd, $yy";
}


# Return in US format, e.g. 12-31-1999
sub us_fmt {
    my ($this) = @_;
    
    my ($dd, $mm, $yy) = (localtime($this->{'ctime'}))[3,4,5];
    $mm += 1;
    $yy += 1900;
    return "$mm-$dd-$yy";
}


# Return in European format, e.g. 31-12-1999
sub euro_fmt {
    my ($this) = @_;
    
    my ($dd, $mm, $yy) = (localtime($this->{'ctime'}))[3,4,5];
    $mm += 1;
    $yy += 1900;
    return "$dd-$mm-$yy";
}



# Add an integer number of days to a date
sub add {
    my ($this, $days) = @_;
    
    my $retval = $this->copy();
    $retval->{'ctime'} += $days * 24 * 60 * 60;
    
    return $retval;
}

# Subtract a number of days from a date or subtract two dates
sub subtract {
    my ($first, $second, $reverse) = @_;
    
    if (ref($second)) {		# Second parameter is a reference
	if (UNIVERSAL::isa($second, 'Date')) {
	    my $val = $first->{'ctime'} - $second->{'ctime'};
	    $val /= 24 * 60 * 60;
	    return $val;
	}
	confess "Cannot subtract non-date [$second] from [$first]";
    } else {			# Second parameter not a reference
	if ($reverse) {
	    confess "Cannot call [[$second - $first]";
	}
	my $retval = $first->copy();
	$retval->{'ctime'} -= $second * 24 * 60 * 60;
	return $retval;
    }
}


# Copy constructor
sub copy {
    my ($this) = @_;
    
    return bless { %$this }, ref($this);
}

# Compare two values by comparing their ctimes
sub compare {
    my ($first, $second) = @_;
    
    unless (UNIVERSAL::isa($second, 'Date')) {
	confess "Can only compare two Date objects, not $second";
    }
    return ($first->{'ctime'} <=> $second->{'ctime'});
}

# Silly numerical conversion operator
sub to_number {
    my ($this) = @_;
    
    return (localtime($this->{'ctime'}))[6];
}


package EuroDate;

use strict;
use Date;
use vars qw(@ISA);

@ISA = qw(Date);

use overload ('""'  => 'euro_fmt');

1;


