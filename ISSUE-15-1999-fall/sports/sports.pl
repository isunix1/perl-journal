#!/usr/bin/perl -s
# perlpoll.plx, pudge@pobox.com
use LWP::Simple;
$sport = uc(shift || 'mlb');
print "Polling web servers, please wait ...\n";
@page = split /\015?\012/, get 'http://pudge.net/pudge/teams.html';
$_ = shift @page until m/<H2>$sport</;
while (shift(@page) =~ m|^<LI>(?:<A HREF="(.+?)">)?(.+?)<|) {
    ($team, $turl, $score, $server) = ($2, $1, 0, (head $1)[-1]);
    for ($server) {
        /^Apache/ and $score += 10;  /^NCSA/ and $score += 7;
        /^Netscape/ and $score += 5; /^Microsoft/ and $score -= 5;
        /(php|unix|mysql|ssl)/i and $score += 2;
        /\bmac(?:| ?os|perl|intosh)\b/i and $score += 10;
        $score += 50 while /perl/ig; $score += 2 while /mod_/ig;
    }
    $teams{$team} = [$server, $score, $turl];
    print "\u$team ($turl) = $server ($score)\n" if $DEBUG;
}
@order = map  { $_->[0] } sort { $b->[2] <=> $a->[2] || $a->[1] cmp $b->[1] }
         map  { [ $_, @{$teams{$_}} ] } keys %teams;
@top = grep { $teams{$_}->[1] == $teams{$order[0]}->[1] } @order;
if (@top > 1) { @order[0..$#top] = tiebreak(@top) }
else { @top = grep { $teams{$_}->[1] == $teams{$order[1]}->[1] } @order;
@order[1..@top] = tiebreak(@top) if @top > 1 }
sub tiebreak {
    print "Executing tiebreaker ...\n";
    for $team (@_) {
        $tie{$team} = 100; local $_ = get $teams{$team}->[2];
        $tie{$team}--     while /<\s*(IMG|SCRIPT|EMBED)\b/gi;
        $tie{$team} -= 10 while /<\s*APPLET\b/gi;
        $tie{$team} -= 3  while /<\s*FRAME/gi;
        $tie{$team} -= 50 if /<\s*FRAME/i && ! /<\s*NOFRAME/i;
        $tie{$team} -= 50 if /require[\w ]+frame|frame[\w ]+require/i;
        print "  $team: $tie{$team}\n" if $DEBUG;
    }
    return sort { $tie{$b} <=> $tie{$a} } keys %tie;
}
printf <<EOT, map ucfirst, @order[0, 1], $sport;

THE PERL SPORTS POLL
====================
Unless some team upgrades, the %s will defeat
the %s in the next %s championship.

EOT
for (@order) {
    printf "%2.2d. %s ($teams{$_}->[1])\n", ++$count, ucfirst $_;
}
