* 1 rule files to read
* Reading from braille.tab
"25|"|0 closed quotes" duplicates existing rule; possible ordering paradox
"25|"|,8 open inner quotes?" duplicates existing rule; possible ordering paradox
"25|"|0, closed inner quotes?" duplicates existing rule; possible ordering paradox
"34|COME|-" duplicates existing rule; possible ordering paradox
"34|TOWARDS|"6" duplicates existing rule; possible ordering paradox
* 1269 rule statements read.

Rule frames:
 # 1 : "%s" 489 subrules (prec 950)
 # 2 : "\b%s\b" 130 subrules (prec 500)
 # 3 : "\b%s" 155 subrules (prec 600)
 # 4 : "\B%s\B" 9 subrules (prec 200)
 # 6 : "?" 3 subrules (prec ?)
   * No frame for rule number 6; skipping
 # 7 : "\B%s" 43 subrules (prec 700)
 # 10 : "\B%s\b" 1 subrules (prec 300)
 # 11 : "\b%s\B" 3 subrules (prec 400)
 # 19 : "?" 10 subrules (prec ?)
   * No frame for rule number 19; skipping
 # 21 : "?" 31 subrules (prec ?)
   * No frame for rule number 21; skipping
 # 22 : "?" 1 subrules (prec ?)
   * No frame for rule number 22; skipping
 # 24 : "?" 1 subrules (prec ?)
   * No frame for rule number 24; skipping
 # 25 : "?" 1 subrules (prec ?)
   * No frame for rule number 25; skipping
 # 27 : "?" 1 subrules (prec ?)
   * No frame for rule number 27; skipping
 # 28 : "?" 1 subrules (prec ?)
   * No frame for rule number 28; skipping
 # 33 : "?" 171 subrules (prec ?)
   * No frame for rule number 33; skipping
 # 34 : "?" 65 subrules (prec ?)
   * No frame for rule number 34; skipping
 # 35 : "?" 100 subrules (prec ?)
   * No frame for rule number 35; skipping
 # 36 : "?" 7 subrules (prec ?)
   * No frame for rule number 36; skipping
 # 39 : "?" 21 subrules (prec ?)
   * No frame for rule number 39; skipping
 # 42 : "?" 21 subrules (prec ?)
   * No frame for rule number 42; skipping
830 bits
Encoding...


Diff: �bestial� => �2/IAL�, ne real �BE/IAL�

Diff: �bestir� => �2/IR�, ne real �BE/IR�

Diff: �bestirring� => �2/IRR+�, ne real �BE/IRR+�

Diff: �bestseller� => �2/SELL]�, ne real �BE/SELL]�

Diff: �bestselling� => �2/SELL+�, ne real �BE/SELL+�

Diff: �bestubble� => �2/UB#�, ne real �BE/UB#�

Diff: �by� => �BY�, ne real �0�

Diff: �firsthand� => �FIR/H&�, ne real �F/H&�

Diff: �greatcoat� => �GR1TCOAT�, ne real �GRTCOAT�

Diff: �greater� => �GR1T]�, ne real �GRT]�

Diff: �into� => �9TO�, ne real �96�

Diff: �to� => �TO�, ne real �6�


--
24766 items = 24754 correct + 12 incorrect.  99.95% coverage
37s runtime.  Starfish version 0.31
