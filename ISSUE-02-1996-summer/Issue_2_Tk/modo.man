.TH "modometer" 1 "Mar 24, 1996"
.SH NAME
modometer
.SH SYNOPSIS
modo [ -h -d -dwm -dhm -dwp -dhp -psf -bg -fg -fn -g -i -mit -o -of -oat -t]
.SH DESCRIPTION

Track the total distance your pointing device and cursor
travel.  The distance can be displayed in various units.

modometer displays total distance and "trip" distance
since the application started (or since you clicked on
a trip reset button).

modometer requires certain information to ensure
accurate distance tracking.  Refer to the following
sections to learn about calibrating modometer, and to
view a list of tested configurations.

modometer is typically started from the .xinitrc file.
Use the left button for selections.  Use the middle
button to reset both trip odometers simultaneously.

For further help try modo -full_help.

Examples:

  modo -bg wheat1 -fg blue -g -0-0

  modo -fn rom6 -o cursor -dwm 300 -dhm 234 


CALIBRATION

modometer requires the display dimensions in both pixels
and millimeters in order to correctly compute distances.
Look for this information in the appropriate hardware 
reference manual for your display.  If you cannot find
this information it's still easy to determine.  For the
display dimensions in pixels simply run modometer, jam
the cursor in the bottom-right corner and note the X/Y
coordinates displayed at the bottom of the window - add
one to get the actual pixel count.  For the display
dimensions in millimeters just grab a tape measure and
measure your screen - if inches multiply by 25.4 and if
centimeters multiply by 10.  Select "Help/Verify" to
verify that modometer is calibrated properly by using a
ruler to measure the calibration scale.  The default
values for these dimensions are suitable for an IBM
RS/6000	machine with a 6091 19" color monitor.  Refer to
the next section for values of other tested
configurations.

Assuming that the display dimension data is correct the
cursor distance can be accurately tracked.  The actual
distance that your pointing device, typically a mouse,
travels is INFERRED by accleration information provided
by the X server and pointer scaling information that
you must provide.  The default scale factor is 3.4,
meaning that the cursor travels 3.4 times as far as the
pointing device moves. This value is appropriate for an
IBM RS/6000 machine with a 6091 19" color monitor.
Refer to the next section for values of other tested
configurations.

If you cannot find the correct scale factor for your
mouse then you must determine it by measuring.  It is
rather easy to do this:  first enter "xset m 1 1" to
set the X threshold and	acceleration to 1, then enter
"modo -psf 1.0" to set modometer's pointer scale factor
also to 1.  Once modo is running pull-down the Units
menu and select "inches".  Then, using a ruler, place
the pointing device against one edge, click the second
button to reset the trip odometers, and then trace a
known distance, say, one inch.  The distance recorded
by the pointer's trip odometer is the proper scaling
factor.  Repeat the measurement several times for
accuracy.

At the bottom of the modometer window is a status line
that displays the current distance Units and the X/Y
cordinates of the cursor.  In the "Help/Verify" window
the pointer Scale factor, and the X Threshold and
Acceleration are displayed.

All the calibration information you supply is either
passed on the command line or stored in environment
variables.  The applicable environment variables are:

  D_MODO_DWM	display_width_millimeters
  D_MODO_DHM	display_height_millimeters
  D_MODO_DWP	display_width_pixels
  D_MODO_DHP	display_height_pixels
  D_MODO_PSF	pointer_scale_factor


TESTED CONFIGURATIONS	

For each machine, Operating System/window manager and
display configuration, a sample modometer command line
is given:

- Pentium-100, Linux 1.2.13/fvwm, 15" color
  modo -dwm 286 -dhm 203 -dwp 1024 -dhp 768  -psf 3.0

- IBM RS/6000, AIX 3.2.3/mwm, 16" color
  modo -dwm 300 -dhm 234 -dwp 1280 -dhp 1024 -psf 3.0

- IBM RS/6000, AIX 3.2.5/mwm, 19" color
  modo -dwm 356 -dhm 284 -dwp 1280 -dhp 1024 -psf 3.4

- IBM RS/6000, AIX 3.2.3/mwm, 23" color
  modo -dwm 430 -dhm 340 -dwp 1280 -dhp 1024 -psf 4.0

- Sun SPARC 1+, SunOS 4.1.1/twm, 17" monochrome
  modo -dwm 292 -dhm 232 -dwp 1152 -dhp  900 -psf 2.0	  

- Sun SPARC 1+, SunOS 4.1.1/twm, 19" color
  modo -dwm 358 -dhm 274 -dwp 1152 -dhp  900 -psf 4.0	  

.SH OPTIONS

-help, h, usage_help, full_help: Display Command Information

  
  Display information about this command, which includes
  a command description with examples, plus a synopsis of
  the command line parameters.  If you specify -full_help
  rather than -help complete parameter help is displayed
  if it's available.
  

-display, d: string = DISPLAY, ":0.0"

  
  The X display name; default is the DISPLAY variable.
  

-display_width_millimeters, dwm: integer = D_MODO_DWM, 286

  
  The width in millimeters of the X display.  The default
  is appropriate for an IBM 6091 19" color monitor.
  

-display_height_millimeters, dhm: integer = D_MODO_DHM, 203

  
  The height in millimeters of the X display.  The default
  is appropriate for an IBM 6091 19" color monitor.
  

-display_width_pixels, dwp: integer = D_MODO_DWP, 1024

  
  The width of the X display in pixels.  The default
  is appropriate for an IBM 6091 19" color monitor.
  

-display_height_pixels, dhp: integer = D_MODO_DHP, 768

  
  The height of the X display in pixels.  The default
  is appropriate for an IBM 6091 19" color monitor.
  

-pointer_scale_factor, psf: real = D_MODO_PSF, 3.4

  
  The scale factor to convert cursor movement to pointer
  movement.  A scale factor of 2.0 means that for every D
  units of distance the cursor moves, the pointing device
  moves D/2.0 units.  The default is 3.4, suitable for an
  IBM 6091 19" color monitor.
  

-background, bg: name = #d9d9d9

  
  modometer's background color.
  

-foreground, fg: name = Black

  
  modometer's foreground color.
  

-fontname, fn: string = "fixed"

  
  modometer's odometer font.  An extremely small font
  is "rom6" while a rather large font is "helvr30".
  

-geometry, g: string = "<width>x<height>{+-}<xoffset>{+-}<yoffset>"

  
  Specifies the X geometry in the standard notation.
  The width and height are not normally specified since
  modometer calculates them based on the fontname.  If
  an "offset" value is positive it is measured from the
  top or left edge of the display, and if negative it is
  measured from the bottom or right edge of the screen.
  So, to start modometer in the bottom-right corner a
  geometry string of "-0-0" would be specified.
  

-iconic, i: switch

  
  If specified modometer starts up already iconified.
  

-microsecond_interval_time, mit: integer = 100000

  
  The number of microseconds between odometer updates.  The
  default value of 100,000 means that the pointer position
  is sampled 10 times per second, which seems to provide
  accurate distance measurements without consuming
  excessive amounts of your machine's resources.
  

-odometer, o: key cursor, pointer, both, keyend = both

  
  A keyword that specifies whether to display both
  odometers, or just one of them, and if just one,
  which one.
  

-odometer_file, of: file = $HOME/.modo

  
  The path name of the file to record total mouse distance
  (in millimeters) and other application information.  This
  file is read during modometer startup to initialize the
  distance totals and establish the distance units.  When
  you "Quit" modometer the updated distance/unit data is
  written to this file.
  

-odometer_autosave_time, oat: integer = 2

  
  Specifies the time interval in minutes between odometer
  file updates.  This is just for good luck, as modometer
  updates the odometer file when these events are received:
  
    - control/c
    - window close
  

-title, t: string = "Modo"

  
  The modometer window title line.
  

.SH AUTHOR
Stephen O. Lidie, lusol@Lehigh.EDU

Copyright (C) 1995 - 1996 by Stephen O. Lidie.  All rights reserved.

.SH SEE ALSO
.nf
.BR xodo
.BR tkodo
