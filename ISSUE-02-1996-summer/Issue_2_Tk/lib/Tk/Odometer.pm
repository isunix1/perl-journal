
package Tk::Odometer;

# Class "Odometer": constructor, methods, destructor, global class data,
# etcetera.  SOL, LUCC.  95/08/17.  lusol@Lehigh.EDU
#
# Stephen O. Lidie, Lehigh University Computing Center, lusol@Lehigh.EDU. 
# 96/03/02

require 5.002;
use English;
use Tk;
use strict;
use Tk::ErrorDialog;
use Tk::Frame;
@Tk::Odometer::ISA = qw(Tk::Frame);
Tk::Widget->Construct('Odometer');

=head1 NAME

 Odometer() - a widget that implements a typical automobile style odometer.

=head1 SYNOPSIS

 use Tk::Odometer;

 $odo = $MW->Odometer(
     -odometerlabel => 'Cursor',
     -font          => $fn,
     -foreground    => $fg,
     -background    => $bg,
     -cursor        => $cu,
 );

=head1 DESCRIPTION

 This class module manipulates instances of Odometer widgets.  An Odometer
 object consists of two odometers, arranged in a column, with the top one
 displaying total distance traveled and the bottom displaying trip distance.
 There is a "reset" button located next to the trip odometer that sets the
 trip distance to zero.

=head1 METHODS

=head2 $odo->add($distance, $units);

 $distance is always maintained in millimeters, $units is the conversion factor
 for displaying in other units.  Thus, if $units = 1 the distance is displayed
 in millimeters, if $units = 0.1 the displayed distance is in centimeters,
 etcetera.

=head2 $odo->bind('<Event>' => $callback);

 Bind a callback to the odometer's trip reset button.  A default <Button-1>
 binding sets the trip odometer to zero.

=head2 $odo->get_total_distance;

 Get the odometer's total distance, in millimeters.

=head2 $odo->reset_trip;

 Set the odometer's trip count to zero millimeters.

=head1 AUTHOR

 Stephen O. Lidie <lusol@Lehigh.EDU>

=head1 HISTORY

 lusol@Lehigh.EDU, LUCC, 95/08/17
  . Original version 1.0 release.

 lusol@Lehigh.EDU, LUCC, 96/03/19
  . Version 1.1:  update for Perl 5.002 and production Tk 4.0.

=head1 COPYRIGHT

 Copyright (C) 1995 - 1996 Stephen O. Lidie. All rights reserved.

 This program is free software; you can redistribute it and/or modify it under
 the same terms as Perl itself.

=cut

# Global data.

my (@PACK) = (-fill => 'both', -expand => 1); # often used pack attributes
my $Z = '00000';                              # zeros 

sub Populate {

    # Odometer composite widget constructor.
    #
    # We have been called to at last populate the composite widget $cw; call
    # superclass Popluate to pre-massage the widget arguments, then construct
    # the widget, initialize instance data and specify configure() options.

    my($cw, $args) = @ARG;

    $cw->SUPER::Populate($args);

    # Odometer label.

    my $l = $cw->Label->pack(@PACK);

    # Odometer total distance, left and right labels.

    $cw->make_odo('total')->pack(@PACK);

    # Odometer trip reset button.  It's placed inside a container frame so
    # there is a background to color, since trying to configure the composite
    # containing frame results in nasty recursion problems.  The button is
    # anchored southwest so it stays "attached to" the trip odometer.

    my $rbf = $cw->Frame(-relief => 'flat')->pack(@PACK);
    my $rb = $rbf->Button(
        -height             => 2,
        -width              => 5,
        -bitmap             => 'gray50',
        -relief             => 'flat',
        -command            => [$cw => 'reset_trip'],
        -highlightthickness => 0,
    )->pack(-anchor => 'sw', -expand => 1);

    # Odometer trip distance, left and right labels.

    $cw->make_odo('trip')->pack(@PACK);

    # Maintain instance variables in the composite widget hash.  Instance
    # variables hold data particular to one instance of an Odometer object.
    #
    # reset             = widget reference to trip reset button for bind()
    # total_mm          = total distance in millimeters
    # total_left        = total distance left  label -textvariable for add()
    # total_right       = total distance right label -textvariable for add()
    # total_right_label = widget reference to right label for colorizing
    # trip_mm           =
    # trip_left         =      (ditto for trip distance)
    # trip_right        =
    # trip_right_label  =

    $cw->{'reset'} = $rb;
    $cw->{'total_mm'} = 0;
    ($cw->{'total_left'}, $cw->{'total_right'}) = ($Z, $Z);
    $cw->reset_trip;

    # Now establish configuration specifications so that the composite behaves
    # like a standard Perl/Tk widget.  Each configuration specification entry
    # is a list of 4 items describing the option:  how to process a configure
    # request, its name in the resource database, its class name, and its
    # default value.
    #
    # The Tk::Configure->new() specification renames -odometerlabel to -text,
    # which is what Labels want, because -odometerlabel IS a Label.
    # 
    # The DESCENDANTS specification applies configure() recursively to all
    # descendant widgets.
    #
    # The METHOD specification invokes a method by the same name as the option
    # (without the dash), e.g.:
    #
    #     $cw->background($bg);
    #
    # Normally you don't need special configurators for background and
    # foreground attributes, but an Odometer is special because these colors
    # are reversed for the right half of the odometers.
    #
    # The -cursor specification says to configure only the indicated list of
    # widgets (in this case there is but one, $rb, the trip reset button.)
        
    $cw->ConfigSpecs(
        -odometerlabel  => [[Tk::Configure->new($l => '-text')], 
                            'odometerlabel', 'odometerLabel', 'Odometer'],
        -font           => ['DESCENDANTS', 'font', 'Font', 'fixed'],
        -background     => ['METHOD', 'background', 'Background', '#d9d9d9'],
        -foreground     => ['METHOD', 'foreground', 'Foreground', 'black'],
        -cursor         => [[$rb], 'cursor', 'Cursor', ['left_ptr']],
    );

    return $cw;

} # end Populate, Odometer constructor

sub add {

    # Accumulate millimeters and display, modulus 100,000, in user units.

    my($odo, $d, $u) = @ARG;

    $odo->BackTrace('Usage:  $odo->add($distance, $units)') if @ARG != 3;
    $odo->{'total_mm'} += $d;
    $odo->{'trip_mm' } += $d;

    my($n1, $f1, $n2, $f2, $s);
    $n1 = $odo->{'total_mm'} * $u;
    $f1 = $n1 - int($n1);
    $n2 = $odo->{'trip_mm' } * $u;
    $f2 = $n2 - int($n2);
    $s = sprintf("%011.5f%011.5f", ($n1 % 100000) + $f1, ($n2 % 100000) + $f2);
    $odo->{'total_left' } = substr($s,  0, 5);
    $odo->{'total_right'} = substr($s,  6, 5);
    $odo->{'trip_left'  } = substr($s, 11, 5);
    $odo->{'trip_right' } = substr($s, 17, 5);
    return $odo;

} # end add

sub bind {

    # Override bind() to select trip reset button, the only sensible widget.
    # Build an argument list to bind() so that the call behaves normally.

    my($odo, $event, $code) = @ARG;

    my @args = ();
    push @args, $event if defined $event;
    push @args, $code  if defined $code; 
    $odo->{'reset'}->bind(@args);
    return $odo;

} # end bind

sub get_total_distance {shift->{'total_mm'}} # end get_total_distance

sub reset_trip {

    # Zero trip millimeters and update -textvariable to update the display.

    my($odo) = @ARG;

    $odo->{'trip_mm'} = 0;
    ($odo->{'trip_left' }, $odo->{'trip_right' }) = ($Z, $Z);
    return $odo;
    
} # end reset_trip

# Odometer private methods.

sub make_odo {

    # Create a total or trip odometer and return its reference.  Store in the
    # composite widget keys of the form $kind_left and $kind_right which are
    # the Labels' -textvariable, and $kind_right_label which is a reference to
    # the right odometer Label (required for colorizing).  $kind is either
    # "total" or "trip".

    my($cw, $kind) = @ARG;

    my $f = $cw->Frame;
    foreach (qw(left right)) {
        my $textvariable = "${kind}_${ARG}";
        my $widget = "${kind}_${ARG}_label";
        my $l = $f->Label(
            -borderwidth  => 1,
            -relief       => 'sunken',
            -width        => 5,
            -textvariable => \$cw->{$textvariable},
        )->pack(-side => 'left', @PACK);
        $cw->{$widget} = $l if /^right$/;
    }; # forend

    return $f;

} # end make_odo

# Odometer background/foreground color configurator subroutines.

sub background {shift->bf(shift, '-foreground', '-background')};
sub foreground {shift->bf(shift, '-background', '-foreground')};

sub bf {

    # Reverse background/foreground colors on right odometer labels.

    my($odo, $color, $bf1, $bf2) = @ARG;

    my $total_right = $odo->{'total_right_label'};
    my $trip_right  = $odo->{'trip_right_label'};

    $odo->Walk(
        sub {
            my($widget) = @ARG;
            if ($widget == $total_right or $widget == $trip_right) {
                $widget->configure($bf1 => $color);
            } else {
                $widget->configure($bf2 => $color);
            }
        }
    );

} # end bf

1;
